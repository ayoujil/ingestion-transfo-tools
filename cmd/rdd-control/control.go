package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/blob"
	rddblob "rdd-pipeline/pkg/blob"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/Azure/azure-storage-queue-go/azqueue"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

var controlCmd = &cobra.Command{
	Use:   "control",
	Short: "Use control",
	Run:   runControl,
	Example: `/rdd-control control \
		--target-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \,
		--config-storage-account-queue-endpoint https://myConfigStorageAccount.queue.core.windows.net \,
		--config-input-queue-name queuemanagementinput
		--allowed-verbs list,unlease,restore,delete,generate_sas_token`,
}

func init() {
	rootCmd.AddCommand(controlCmd)
	common.AddTargetStorageAccountFlags(controlCmd)
	common.AddConfigStorageAccountQueueFlag(controlCmd)
	common.AddConfigInputQueueNameFlag(controlCmd)
	common.AddAllowedVerbsFlag(controlCmd)

}

var errExit *exec.ExitError

func runControl(cmd *cobra.Command, args []string) {
	idleTime := 0
	autoShutdownTime := 1800
	ctx := cmd.Context()
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Control")

	log.Info("Start")

	targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, err := common.GetQueueTargetStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetQueueTargetStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	allowedVerbs, err := common.GetAllowedVerbs(cmd)
	if err != nil {
		log.Error(err, "GetAllowedVerbs", "cmd", cmd)
		os.Exit(1)
	}

	targetStorageAccountURL = strings.TrimSuffix(targetStorageAccountURL, "/")
	configStorageAccountQueueURL = strings.TrimSuffix(configStorageAccountQueueURL, "/")

	log.Info("Allowed verbs", "allowedVerbs", allowedVerbs)

	// Creating the input queue client
	inputQueueClient, err := blob.NewQueueClientFromEnvironment(log, configStorageAccountQueueURL, inputQueueName)

	if err != nil && inputQueueClient != nil {
		fmt.Println("[ERROR] Creating the client for the input queue (NewQueueClientFromEnvironment)")
		fmt.Println(err)
		os.Exit(0)
	}

	msgUrl := inputQueueClient.NewMessagesURL()

	const concurrentMsgProcessing = 1 // 1 for now, parallel treatment should be working with this design (not tested)
	msgCh := make(chan *azqueue.DequeuedMessage, concurrentMsgProcessing)
	const poisonMessageDequeueThreshold = 4 // Indicates how many times a message is attempted to be processed before considering it a poison message	// The code below shows the service's infinite loop that dequeues messages and dispatches them in batches for processsing:

	// Process messages in parallel
	for n := 0; n < concurrentMsgProcessing; n++ {
		go func(msgCh <-chan *azqueue.DequeuedMessage) {
			for {
				msg := <-msgCh
				msgIDURL := msgUrl.NewMessageIDURL(msg.ID)
				popReceipt := msg.PopReceipt // This message's most-recent pop receipt

				// Check if we attemped to dequeue this message already.
				// If the number of dequeuing for this message exceeds "poisonMessageDequeueThreshold", it is considered poisonned and therefore should be dropped
				if msg.DequeueCount > poisonMessageDequeueThreshold {
					fmt.Println("Deleting message :", msg.Text)
					_, err = msgIDURL.Delete(ctx, popReceipt)
					if err != nil {
						log.Error(err, "Error deleting the message")
						os.Exit(1)
					}
					continue // Process a different message
				}

				err = ProcessMessage(ctx, log, msg, targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, allowedVerbs)
				if err != nil {
					log.Error(err, "Error processing the message")
					os.Exit(1)
				}

				_, _, _ = msg.InsertionTime, msg.ExpirationTime, msg.NextVisibleTime
				update, err := msgIDURL.Update(ctx, popReceipt, time.Second*20, msg.Text)
				if err != nil {
					log.Error(err, "Error updating message visibility on the queue")
				}
				popReceipt = update.PopReceipt

				// After processing the message, delete it from the queue
				_, err = msgIDURL.Delete(ctx, popReceipt)
				if err != nil {
					log.Error(err, "Error deleting message from the queue")
				}
			}
		}(msgCh)
	}
	for {
		// Try to dequeue a batch of messages from the queue
		dequeue, err := msgUrl.Dequeue(ctx, azqueue.QueueMaxMessagesDequeue, 10*time.Second)
		if err != nil {
			log.Error(err, "Error dequeuing message")
		}
		if dequeue.NumMessages() == 0 {
			// The queue was empty; sleep a bit and try again
			time.Sleep(time.Second * 10)
			idleTime = idleTime + 10
		} else {
			idleTime = 0
			log.Info("Message found, resetting the autoshutdown countdown")
			// We got some messages, put them in the channel so that many can be processed in parallel:
			for m := int32(0); m < dequeue.NumMessages(); m++ {
				msgCh <- dequeue.Message(m)
			}
		}

		if idleTime == autoShutdownTime {
			log.Info("Autoshutdown time reached, this ACI will stop")
			os.Exit(0)
		}

	}
}

type ControlMessage struct {
	Command        string
	Content        string
	Origin         string
	Results_amount int
}

var ErrVerbNotAllowed string = "[ERROR] This verb is not allowed by this ACI"

// Dispatch function depending on the command we ask the ACI to execute
func ProcessMessage(ctx context.Context, log logr.Logger, messageContent *azqueue.DequeuedMessage, targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, allowedVerbs string) error {
	var message ControlMessage
	var err error
	var result string

	err = json.Unmarshal([]byte(messageContent.Text), &message)
	if err != nil {
		fmt.Println("Error occured in unmarshal of the message:", err)
		return err
	}

	switch message.Command {
	case "list_files":
		if !strings.Contains(allowedVerbs, "list") {
			result = ErrVerbNotAllowed
		} else {
			result, err = ListFiles(ctx, log, message.Content, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	case "unlease_files":
		if !strings.Contains(allowedVerbs, "unlease") {
			result = ErrVerbNotAllowed
		} else {
			result, err = UnleaseFiles(ctx, log, message.Content, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	case "generate_sas_token":
		if !strings.Contains(allowedVerbs, "generate_sas_token") {
			result = ErrVerbNotAllowed
		} else {
			result, err = GenerateSASToken(ctx, log, message.Content)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}

	default:
		return fmt.Errorf("unsupported command '%s' in this message", message.Command)
	}
	fmt.Println("Message processed, sending response to the output queue")
	fmt.Printf("MessageID: %s, Result: %s \n", string(messageContent.ID), result)
	if strings.Contains(result, "ERROR") {
		message.Results_amount = 400
	}
	err = AnswerMessage(ctx, log, result, configStorageAccountQueueURL, string(messageContent.ID), inputQueueName, message.Results_amount)
	if err != nil {
		fmt.Println("Error occured while answering the message:", err)
		return err
	}
	return nil
}

// Send the answer in the managementoutput queue of the storage account with the following data:
// request_id: the id of the message that was sent into the managementinput queue, used to link a request to an answer
// content: the actual content of the message, ex: files list...
func AnswerMessage(ctx context.Context, log logr.Logger, messageContent, configStorageAccountQueueURL, messageId, inputQueueName string, results_amount int) error {
	resultSplitChar := " "
	finalMessage := ""
	outputQueueName := strings.Replace(inputQueueName, "managementinput", "managementoutput", 1)
	targetStorageQueueClient, err := blob.NewQueueClientFromEnvironment(log, configStorageAccountQueueURL, outputQueueName)
	if err != nil && targetStorageQueueClient != nil {
		return err
	}

	messageContent = strings.TrimPrefix(messageContent, " ")
	// Keep only "results_amount" lines in the return content if "results_amount" is defined (!= 0)
	if results_amount != 0 {
		if strings.Count(messageContent, resultSplitChar) >= results_amount {
			splitMessage := strings.SplitAfterN(messageContent, resultSplitChar, results_amount+1)
			for i := 0; i < results_amount; i++ {
				finalMessage = finalMessage + splitMessage[i]
			}
			messageContent = finalMessage
		}
	}

	msgUrl := targetStorageQueueClient.NewMessagesURL()
	messageValue := "{\"request_id\":\"" + messageId + "\",\"content\":\"" + messageContent + "\"}"

	_, err = msgUrl.Enqueue(ctx, messageValue, time.Second*0, 20*time.Minute)
	if err != nil {
		return err
	}
	return nil
}

func ListFiles(ctx context.Context, log logr.Logger, messageContent, targetStorageAccountURL string) (string, error) {
	result := ""
	// targetStorageAccountURL is the queue url, not the blob url
	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	// Get access to the storage account container
	listContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, targetStorageAccountBlobURL+"/", messageContent)
	if err != nil {
		return "", fmt.Errorf("Create container client: %w", err)
	}

	options := azblob.ListBlobsSegmentOptions{
		MaxResults: 0, // No max result
		Details:    azblob.BlobListingDetails{Metadata: true, Versions: false},
	}

	marker := azblob.Marker{}
	for marker.NotDone() {
		segment, err := listContainerClient.ListBlobsFlatSegment(ctx, marker, options)
		if err != nil {
			if strings.Contains(err.Error(), "ContainerNotFound") {
				// Subcase to make the error message easier to read
				return "", fmt.Errorf("Container not found")
			}
			return "", fmt.Errorf("list segment: %w", err)
		}

		log.V(2).Info("Found blobs", "count", len(segment.Segment.BlobItems))
		for _, blob := range segment.Segment.BlobItems {
			result = result + " " + "name:" + blob.Name +
				",size:" + fmt.Sprintf("%v", *blob.Properties.ContentLength) +
				",creationtime:" + strings.Replace(blob.Properties.CreationTime.String(), " ", "_", -1) +
				",leaseStatus:" + fmt.Sprintf("%v", blob.Properties.LeaseStatus)
		}

		marker = segment.NextMarker
	}

	return result, nil
}

func UnleaseFiles(ctx context.Context, log logr.Logger, messageContent, targetStorageAccountURL string) (string, error) {
	result := ""
	// targetStorageAccountURL is the queue url, not the blob url
	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	fmt.Println("unleasing in the storage account: ", targetStorageAccountBlobURL, "some blobs, messageContent: ", messageContent)
	accountName := strings.TrimPrefix(targetStorageAccountBlobURL, "https://")
	accountName = strings.Split(accountName, ".")[0]
	// At this point, messageContent will look like: "container:containername:filename:file.csv"
	// container:containername means that the container which is targetted is called "containername"
	// filename:file.csv says that we are looking for a specific file, called "file.csv"
	s := strings.Split(messageContent, ":")
	containerName := s[1]
	if strings.Contains(messageContent, ":filename:") {
		fileName := strings.Split(messageContent, ":filename:")[1]
		log.V(2).Info("looking for a specific file name", "filename", fileName)

		result, _ = executeUnlease(ctx, log, fileName, containerName, accountName, targetStorageAccountBlobURL, false)
	} else {
		regex := strings.Split(messageContent, ":regex:")[1]
		log.V(2).Info("looking for a regex", "regex", regex)
		result, _ = executeUnlease(ctx, log, regex, containerName, accountName, targetStorageAccountBlobURL, true)
	}

	return result, nil
}

func executeUnlease(ctx context.Context, log logr.Logger, fileName, containerName, accountName, targetStorageAccountBlobURL string, regex bool) (string, error) {
	result := ""
	// Get the container client
	containerClient, pipeline, err := rddblob.NewContainerClientFromEnvironment(log, targetStorageAccountBlobURL+"/", containerName)
	if err != nil {
		return "", fmt.Errorf("Create container client: %w", err)
	}

	options := azblob.ListBlobsSegmentOptions{
		MaxResults: 0, // No max result
		Details:    azblob.BlobListingDetails{Metadata: true, Versions: false},
	}

	// Iterate through the container blobs
	marker := azblob.Marker{}
	for marker.NotDone() {
		segment, err := containerClient.ListBlobsFlatSegment(ctx, marker, options)
		if err != nil {
			return "", fmt.Errorf("list segment: %w", err)
		}
		var blobUrl azblob.BlobURL

		for _, blob := range segment.Segment.BlobItems {
			if !regex {
				if blob.Name == fileName {
					if fmt.Sprintf("%v", blob.Properties.LeaseStatus) == "locked" {
						log.Info("Unleasing blob", "blob", blob.Name)
						bUrl, _ := url.Parse(targetStorageAccountBlobURL + "/" + containerName + "/" + blob.Name)
						blobUrl = azblob.NewBlobURL(*bUrl, pipeline)
						_, err = blobUrl.BreakLease(ctx, -1, azblob.ModifiedAccessConditions{})
						if err != nil {
							log.Error(err, "Error Breaking lease")
							os.Exit(1)
						}
						result = result + blob.Name
					}
				}
			} else {
				// fileName here is actually a regex
				matched, err := regexp.MatchString(fileName, blob.Name)
				if err != nil {
					return "", fmt.Errorf("failed to use regex %s: %w", blob.Name, err)
				}

				if matched {
					log.Info("Unleasing", "blob", blob.Name)
					if fmt.Sprintf("%v", blob.Properties.LeaseStatus) == "locked" {
						result = result + " " + blob.Name

						bUrl, _ := url.Parse(targetStorageAccountBlobURL + "/" + containerName + "/" + blob.Name)
						blobUrl = azblob.NewBlobURL(*bUrl, pipeline)
						_, err = blobUrl.BreakLease(ctx, -1, azblob.ModifiedAccessConditions{})
						if err != nil {
							log.Error(err, "Error Breaking lease")
							os.Exit(1)
						}
					}
				}
			}
		}

		marker = segment.NextMarker
	}
	return result, nil
}
