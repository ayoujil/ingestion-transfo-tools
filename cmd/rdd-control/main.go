package main

import (
	"context"
	"os"
	"os/signal"

	"github.com/go-logr/logr"
	"github.com/go-logr/zerologr"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:               "rdd-control",
	Short:             "Execute commands on the storage accounts",
	PersistentPreRunE: rootPreRun,
	SilenceUsage:      true,
}

var logLvl int8

func init() {
	rootCmd.PersistentFlags().Int8VarP(&logLvl, "log-level", "V", 1, "log level")
}

// main adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	ctxCancel, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		select {
		case <-c:
			cancel()
		case <-ctxCancel.Done():
		}
	}()

	// log.Error() >
	// 0 - Error	; .Error()
	// 1 - Warning 	; .Error()
	// log.Info() >
	// 2 - Info		; .V(0).Info() ; .Info()
	// 3 - Debug 	; .V(1).Info()
	// 4 - Trace 	; .V(2).Info()

	zl := zerolog.New(os.Stdout).With().Caller().Timestamp().Logger()
	log := zerologr.New(&zl)

	ctxLogr := logr.NewContext(ctxCancel, log)

	if err := rootCmd.ExecuteContext(ctxLogr); err != nil {
		os.Exit(0)
	}
}

func rootPreRun(cmd *cobra.Command, args []string) error {
	if err := cmd.ParseFlags(args); err != nil {
		return err
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMicro
	zerolog.SetGlobalLevel(zerolog.ErrorLevel - zerolog.Level(logLvl))

	return nil
}
