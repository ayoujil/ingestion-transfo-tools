package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/keyvault/azsecrets"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/services/storage/mgmt/2021-04-01/storage"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	"github.com/Azure/go-autorest/autorest/to"
	"github.com/go-logr/logr"
)

func newAzContainerClient(
	storageAccountName string,
	storageAccountKey string,
	containerName string) (azblob.ContainerClient, azblob.SharedKeyCredential, error) {
	cred, err := azblob.NewSharedKeyCredential(storageAccountName, storageAccountKey)
	if err != nil {
		return azblob.ContainerClient{}, azblob.SharedKeyCredential{}, err
	}
	containerURL := fmt.Sprintf("https://%s.blob.core.windows.net/%s", storageAccountName, containerName)
	containerClient, err := azblob.NewContainerClientWithSharedKey(containerURL, cred, nil)
	if err != nil {
		return azblob.ContainerClient{}, azblob.SharedKeyCredential{}, err
	}

	return containerClient, *cred, nil
}

// getSASURL is a convenience method for generating a SAS token for the currently pointed at container.
// It can only be used if the credential supplied during creation was a SharedKeyCredential.
func getSASURL(
	containerClient azblob.ContainerClient,
	credential azblob.SharedKeyCredential,
	permissions azblob.ContainerSASPermissions,
	start time.Time,
	expiry time.Time) (string, error) {

	urlParts := azblob.NewBlobURLParts(containerClient.URL())

	// Containers do not have snapshots, nor versions.
	var err error = nil
	urlParts.SAS, err = azblob.BlobSASSignatureValues{
		ContainerName: urlParts.ContainerName,
		Permissions:   permissions.String(),
		StartTime:     start.UTC(),
		ExpiryTime:    expiry.UTC(),
	}.NewSASQueryParameters(&credential)

	return urlParts.URL(), err
}

func saveTokenToKeyVault(
	ctx context.Context,
	keyVaultName string,
	key string,
	value string,
	cred *azidentity.DefaultAzureCredential) (string, error) {
	keyVaultURL := fmt.Sprintf("https://%s.vault.azure.net/", keyVaultName)

	client, err := azsecrets.NewClient(keyVaultURL, cred, nil)
	if err != nil {
		return "", err
	}

	_, err = client.SetSecret(ctx, key, value, nil)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/secrets/%s", keyVaultURL, key), nil
}

func getAccessKey(code string) (key string, err error) {
	ctx := context.Background()
	client := storage.NewAccountsClient(os.Getenv("AZURE_SUBSCRIPTION_ID"))
	authorizer, _ := auth.NewAuthorizerFromEnvironment()
	client.Authorizer = authorizer

	var keyToRegenerate = to.StringPtr("key2")
	keyList, err := client.RegenerateKey(
		ctx,
		fmt.Sprintf("%s-components", code),
		fmt.Sprintf("%simport", code),
		storage.AccountRegenerateKeyParameters{
			KeyName: keyToRegenerate,
		})
	if err != nil {
		return "", err
	}
	key2 := *(((*keyList.Keys)[1]).Value)
	return key2, nil
}

func doGenerateSASToken(
	ctx context.Context,
	code string,
	mode string,
	startAsString string) (string, error) {

	// credential
	cred, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		return "", err
	}

	// dates
	start, err := time.Parse("2006-01-02", startAsString)
	if err != nil {
		return "", err
	}
	expiry := start.AddDate(0, 0, 1)

	// storage account name
	storageAccountName := fmt.Sprintf("%simport", code)
	if strings.HasPrefix(storageAccountName, "s") {
		storageAccountName = fmt.Sprintf("%sfrontal", code)
	}

	// storage account access key
	storageAccountKey, err := getAccessKey(code) //nolint
	if err != nil {
		return "", err
	}

	// permission and container name
	var permission azblob.ContainerSASPermissions
	var containerName string
	switch {
	case "import" == strings.ToLower(mode):
		containerName = "input"
		permission = azblob.ContainerSASPermissions{
			Write: true,
			List:  true,
		}
	case "export" == strings.ToLower(mode):
		containerName = "output"
		permission = azblob.ContainerSASPermissions{
			Read: true,
			List: true,
		}
	case "rd" == strings.ToLower(mode):
		containerName = "data"
		permission = azblob.ContainerSASPermissions{
			Write: true,
			List:  true,
		}
	default:
		return "", errors.New("Mode not available. 'rd', 'import' or 'export' expected")
	}

	// client
	containerClient, sa_cred, err := newAzContainerClient(storageAccountName, storageAccountKey, containerName)
	if err != nil {
		return "", err
	}

	// SAS URL
	sasURL, err := getSASURL(containerClient, sa_cred, permission, start, expiry)
	if err != nil {
		return "", err
	}

	// Save secret
	keyVaultSuffix := "sas"
	keyVaultName := fmt.Sprintf("%s-%s", code, keyVaultSuffix)
	keyName := "import-sa-sas-token"
	secretIdentifier, err := saveTokenToKeyVault(ctx, keyVaultName, keyName, sasURL, cred)
	if err != nil {
		return "", err
	}

	return secretIdentifier, nil
}

type Request struct {
	Code string
	Mode string
	Date string
}

func GenerateSASToken(ctx context.Context, log logr.Logger, messageContent string) (string, error) {
	log.Info(fmt.Sprintf("New request: %s", messageContent))

	var request Request
	err := json.Unmarshal([]byte(messageContent), &request)
	if err != nil {
		return "", err
	}

	secretIdentifier, err := doGenerateSASToken(ctx, request.Code, request.Mode, request.Date)
	if err != nil {
		return "", err
	}

	log.Info(fmt.Sprintf("Token saved into %s", secretIdentifier))

	return secretIdentifier, nil
}
