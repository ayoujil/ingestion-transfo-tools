package cmd

import (
	"fmt"
	"os"

	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/spf13/cobra"
)

const (
	stepNameEnvironmentVariable = "HDH_STEP_NAME"
	areaNameEnvironmentVariable = "HDH_AREA_NAME"

	OutputStorageAccountFlag = "output-storage-account-endpoint"
	OutputContainerFlag      = "output-container"

	CatalogStorageAccountFlag = "catalog-storage-account-endpoint"
	CatalogContainerFlag      = "catalog-container"
	CatalogBlobNameFlag       = "catalog-blob-name"
	InputStorageAccountFlag   = "input-storage-account-endpoint"
	InputContainerFlag        = "input-container"
	CTAccountFlag             = "correspondence-table-storage-account-endpoint"
	CTContainerFlag           = "correspondence-table-container-name"
	QuarantineAccountFlag     = "quarantine-storage-account-endpoint"
	QuarantineContainerFlag   = "quarantine-container"
	ConfigAccountFlag         = "config-storage-account-endpoint"
	ConfigContainerFlag       = "config-container"
	ConfigBlobNameFlag        = "config-blob-name"
	KeyVaultFlag              = "keyvault-endpoint"
	NoCPKForInputFlag         = "no-cpk-for-input"
	NoCPKForOutputFlag        = "no-cpk-for-output"
	FilterMetadataFlag        = "filter-metadata"
	OutputBlobPrefixFlag      = "output-blob-prefix"
	WrapedKeyName             = "wraped-key-name"
	KeyVaultKeyName           = "keyvault-wrapping-key-name"

	StepNameMetadataKey = "hdhstep"
	AreaNameMetadataKey = "hdharea"

	MaskPrefixFlag    = "mask-prefix"
	KeySecretNameFlag = "key-secret-name"

	TargetStorageAccountFlag      = "target-storage-account-endpoint"
	ConfigStorageAccountQueueFlag = "config-storage-account-queue-endpoint"
	ConfigInputQueueNameFlag      = "config-input-queue-name"
	AllowedVerbsFlag              = "allowed-verbs"
)

func AddTargetStorageAccountFlags(cmd *cobra.Command) {
	cmd.Flags().String(TargetStorageAccountFlag, "", "Target storage account")
}

func AddConfigStorageAccountQueueFlag(cmd *cobra.Command) {
	cmd.Flags().String(ConfigStorageAccountQueueFlag, "", "Config storage account queue url")
}

func AddConfigInputQueueNameFlag(cmd *cobra.Command) {
	cmd.Flags().String(ConfigInputQueueNameFlag, "", "Config storage account input queue name")
}

func AddAllowedVerbsFlag(cmd *cobra.Command) {
	cmd.Flags().String(AllowedVerbsFlag, "", "Allowed verbs for the ACI")
}

func AddMetadataFilterFlag(cmd *cobra.Command) {
	cmd.Flags().Bool(FilterMetadataFlag, false, "True to filter blob to copy based on metadata, otherwise False (default to false)")
}

func AddMetadataKeyFlags(cmd *cobra.Command) {
	cmd.Flags().String(WrapedKeyName, v1alpha1.MetadataWrapedKey, "Metadata name to store wraped key")
	cmd.Flags().String(KeyVaultKeyName, v1alpha1.MetadataKeyvaultKey, "Metadata name to store keyvault key")
}

func AddNoCPKFlags(cmd *cobra.Command, addInput bool, addOutput bool) {
	cmd.Flags().Bool(NoCPKForInputFlag, false, "true if no CPK is used for input blob; otherwise false (default to flase)")
	cmd.Flags().Bool(NoCPKForOutputFlag, false, "true if no CPK shall be used for output blob; otherwise false (default to flase)")
}

func AddInputBlobFlags(cmd *cobra.Command) {
	cmd.Flags().String(InputContainerFlag, "data", "Input container to output the processed blob")
	(cobra.MarkFlagRequired(cmd.Flags(), InputContainerFlag))

	cmd.Flags().String(InputStorageAccountFlag, "", "Input Blob storage account")
	(cobra.MarkFlagRequired(cmd.Flags(), InputStorageAccountFlag))
}

func AddOutputBlobFlags(cmd *cobra.Command) {
	cmd.Flags().String(OutputContainerFlag, "data", "Output container to output the processed blob")
	(cobra.MarkFlagRequired(cmd.Flags(), OutputContainerFlag))

	cmd.Flags().String(OutputStorageAccountFlag, "", "Output Blob storage account")
	(cobra.MarkFlagRequired(cmd.Flags(), OutputStorageAccountFlag))
}

func AddCatalogBlobFlags(cmd *cobra.Command, required bool) {
	cmd.Flags().String(CatalogContainerFlag, "catalog", "Catalog container to output the processed blob")
	cmd.Flags().String(CatalogStorageAccountFlag, "", "Catalog Blob storage account")
	cmd.Flags().String(CatalogBlobNameFlag, "", "Catalog file name")

	if required {
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogContainerFlag))
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogStorageAccountFlag))
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogBlobNameFlag))
	}
}

func AddKeyVaultFlags(cmd *cobra.Command) {
	cmd.Flags().String("keyvault-endpoint", "", "Key Vault endpoint")
	(cobra.MarkFlagRequired(cmd.Flags(), "keyvault-endpoint"))
}

func AddPrefixFlags(cmd *cobra.Command) {
	cmd.Flags().String(MaskPrefixFlag, "", "prefix string for each masked value")
	(cobra.MarkFlagRequired(cmd.Flags(), MaskPrefixFlag))

}

func AddCtFlags(cmd *cobra.Command) {
	cmd.Flags().String(CTAccountFlag, "", "correspondence table storage account endpoint")
	cmd.Flags().String(CTContainerFlag, "", "container name where correspondence tables are stored")
}

func AddKeySecretNameFlags(cmd *cobra.Command) {
	cmd.Flags().String(KeySecretNameFlag, "", "keyvault key secret name")
	(cobra.MarkFlagRequired(cmd.Flags(), KeySecretNameFlag))
}

func AddOutputBlobPrefixFlags(cmd *cobra.Command) {
	cmd.Flags().String(OutputBlobPrefixFlag, "", "prefix output blob")
}

func AddConfigFlags(cmd *cobra.Command) {
	cmd.Flags().String(ConfigAccountFlag, "", "Config container for the blob to process")
	(cobra.MarkFlagRequired(cmd.Flags(), ConfigAccountFlag))

	cmd.Flags().String(ConfigContainerFlag, "config", "Config container for the blob to process")

	cmd.Flags().String(ConfigBlobNameFlag, "", "Config blob name for the blob to process")
	(cobra.MarkFlagRequired(cmd.Flags(), ConfigBlobNameFlag))
}

func GetMetadataKeyFlags(cmd *cobra.Command) (string, string, error) {
	keyVaultKeyName, err := cmd.Flags().GetString(KeyVaultKeyName)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag KeyVaultKeyName: %w", err)
	}

	wrapedKeyName, err := cmd.Flags().GetString(WrapedKeyName)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag WrapedKeyName: %w", err)
	}

	return keyVaultKeyName, wrapedKeyName, nil
}

func GetPipelineConfigInfo(cmd *cobra.Command) (configAccountURL, containerName, blobName string, err error) {
	configAccountURL, containerName, err = getStorageInfo(cmd, ConfigAccountFlag, ConfigContainerFlag)
	if err != nil {
		return "", "", "", err
	}

	blobName, err = cmd.Flags().GetString(ConfigBlobNameFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("get flag '%s': %w", ConfigBlobNameFlag, err)
	}

	return
}

func GetInputStorageInfo(cmd *cobra.Command) (storageAccountURL, containerName string, err error) {
	return getStorageInfo(cmd, InputStorageAccountFlag, InputContainerFlag)
}

func GetQuarantineStorageInfo(cmd *cobra.Command) (storageAccountURL, containerName string, err error) {
	return getStorageInfo(cmd, QuarantineAccountFlag, QuarantineContainerFlag)
}

func GetOutputStorageInfo(cmd *cobra.Command) (outputStorageAccountEndpoint, outputContainerName string, err error) {
	return getStorageInfo(cmd, OutputStorageAccountFlag, OutputContainerFlag)
}

func GetQueueTargetStorageInfo(cmd *cobra.Command) (targetStorageAccountFlag, configStorageAccountQueueFlag, configInputQueueName string, err error) {
	return getStorageQueueInfo(cmd, TargetStorageAccountFlag, ConfigStorageAccountQueueFlag, ConfigInputQueueNameFlag)
}

func GetAllowedVerbs(cmd *cobra.Command) (allowedVerbs string, err error) {
	allowedVerbs, err = cmd.Flags().GetString(AllowedVerbsFlag)
	if err != nil {
		return "", fmt.Errorf("retrieve flag '%s': %w", AllowedVerbsFlag, err)
	}
	return
}

func GetCatalogStorageInfo(cmd *cobra.Command) (catalogStorageAccountEndpoint, catalogContainerName string, err error) {
	return getStorageInfo(cmd, CatalogStorageAccountFlag, CatalogContainerFlag)
}

func GetCtStorageInfo(cmd *cobra.Command) (ctStorageAccountEndpoint, ctContainerName string, err error) {
	return getStorageInfo(cmd, CTAccountFlag, CTContainerFlag)
}

func getStorageInfo(cmd *cobra.Command, accountFlag, containerFlag string) (storageAccountURL, containerName string, err error) {
	storageAccountURL, err = cmd.Flags().GetString(accountFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", accountFlag, err)
	}

	containerName, err = cmd.Flags().GetString(containerFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", containerFlag, err)
	}
	return
}

func getStorageQueueInfo(cmd *cobra.Command, accountFlag, queueUrlFlag, inputQueueNameFlag string) (storageAccountURL, queueUrl, inputQueueName string, err error) {
	storageAccountURL, err = cmd.Flags().GetString(accountFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", accountFlag, err)
	}
	queueUrl, err = cmd.Flags().GetString(queueUrlFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", queueUrlFlag, err)
	}
	inputQueueName, err = cmd.Flags().GetString(inputQueueNameFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", inputQueueNameFlag, err)
	}
	return
}

func GetMetadataFilter(cmd *cobra.Command) (bool, map[string]string, error) {
	filterMedata, _ := cmd.Flags().GetBool(FilterMetadataFlag)

	if filterMedata {
		stepName, stepNamePresent := os.LookupEnv(stepNameEnvironmentVariable)
		areaName, areaNamePresent := os.LookupEnv(areaNameEnvironmentVariable)

		if !stepNamePresent || !areaNamePresent {
			return true,
				map[string]string{},
				fmt.Errorf("when flag '%s' is set, env variables '%s' and '%s' must be set", FilterMetadataFlag, stepNameEnvironmentVariable, areaNameEnvironmentVariable)
		}

		metadata := map[string]string{
			AreaNameMetadataKey: areaName,
			StepNameMetadataKey: stepName,
		}

		return true, metadata, nil
	}

	return false, map[string]string{}, nil
}
