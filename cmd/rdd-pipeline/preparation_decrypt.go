package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

var preparationDecryptCmd = &cobra.Command{
	Use:   "decrypt",
	Short: "Decrypt a blob",
	RunE:  runHdhDecrypt,
	Example: `rdd-pipeline preparation decrypt \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name decrypt.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--wraped-key-name wrapedkey \
	--keyvault-wrapping-key-name keyvaultkey \
	--keyvault-endpoint https://myVault.vault.azure.net`,
}

func init() {
	preparationCmd.AddCommand(preparationDecryptCmd)

	common.AddInputBlobFlags(preparationDecryptCmd)
	common.AddOutputBlobFlags(preparationDecryptCmd)

	common.AddKeyVaultFlags(preparationDecryptCmd)
	common.AddConfigFlags(preparationDecryptCmd)

	common.AddMetadataKeyFlags(preparationDecryptCmd)
}

func runHdhDecrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Preparation Decrypt")

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, err := cmd.Flags().GetString(common.KeyVaultFlag)
	if err != nil {
		log.Error(err, "retrieve flag", "KeyVaultFlag", common.KeyVaultFlag)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyVaultKeyName, wrapedKeyName, err := common.GetMetadataKeyFlags(cmd)
	if err != nil {
		log.Error(err, "retrieve flags")
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(inputStorageAccountURL, inputContainerName, pipelineConfig.Decrypt, nil, nil, 0, 0)

	// start executing on blob sent from watcher
	task := NewPreparationDecryptTask(
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrapedKeyName,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationDecryptTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	keyVaultKeyName         string
	wrapedKeyName           string
}

func NewPreparationDecryptTask(
	config *v1alpha1.PipelineConfig,
	vaultURL,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName,
	keyVaultKeyName,
	wrapedKeyName string,
) *PreparationDecryptTask {
	return &PreparationDecryptTask{
		config,
		vaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrapedKeyName,
	}
}

func (t *PreparationDecryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Preparation Decrypt")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("failed to get Client Provided Key: %w", err)
	}

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	inputCPK := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, inputCPK)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &inputCPK, prop.NewMetadata())

	blobVersion := inBlob.Metadata()["version"]
	if blobVersion == "v1" {
		return fmt.Errorf("Wrong version: v1 is not allowed (blob: '%s')", blobName)
	}

	// Get wraped key from encrypt
	wrapedKey, ok := inBlob.Metadata()[t.wrapedKeyName]
	if !ok {
		return fmt.Errorf("find key '%s' from blob's '%s' metadata", t.wrapedKeyName, blobName)
	}

	wk, err := base64.StdEncoding.DecodeString(wrapedKey)
	if err != nil {
		return fmt.Errorf("b64 decode wraped-key: %w", err)
	}

	// Get vault key used to encrypt
	keyvaultKey, ok := inBlob.Metadata()[t.keyVaultKeyName]
	if !ok {
		return fmt.Errorf("find key '%s' from blob's metadata", t.keyVaultKeyName)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		return fmt.Errorf("get Key Vault client: %w", err)
	}

	key, err := kvClient.UnWrap(ctx, keyvaultKey, wk)
	if err != nil {
		return fmt.Errorf("unwrap key: %w", err)
	}

	content, _, _, err := inBlob.DownloadBlobStream(ctx)
	if err != nil {
		return fmt.Errorf("can't download inblob '%s': %w", blobName, err)
	}

	stream, err := hdhcrypt.GetStreamReader(key, content)
	if err != nil {
		return fmt.Errorf("get stream reader from blob '%s': %w", blobName, err)
	}

	delete(inBlob.Metadata(), t.keyVaultKeyName)
	delete(inBlob.Metadata(), t.wrapedKeyName)

	uploadBlob := inBlob.Copy(outputContainerClient, &cpk, "")
	if err := uploadBlob.UploadBlobStream(ctx, stream); err != nil {
		return fmt.Errorf("can't upload uploadBlob: %w", err)
	}

	log.Info("successfully processed")
	return nil
}
