package main

import (
	"context"
	"errors"
	"fmt"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

// moveCmd represents the copy command.
var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "Move a blob",
	RunE:  runMove,
	Example: `rdd-pipeline move \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--warn-size 9999`,
}

func init() {
	initMoveOrCopy(moveCmd)
}

func runMove(cmd *cobra.Command, args []string) error {
	return runMoveOrCopy(cmd, true)
}

func initMoveOrCopy(cmd *cobra.Command) {
	rootCmd.AddCommand(cmd)

	common.AddInputBlobFlags(cmd)
	common.AddOutputBlobFlags(cmd)

	cmd.Flags().Int64("warn-size", 0, "warn if file size is over")

	cmd.Flags().Bool("watch", false, "true to run the command as a watcher; otherwise false (default to flase)")

	cmd.Flags().Bool("passthrough-only", false, "true to move or copy ONLY blobs flaged as 'passthrough'; otherwise all blobs will be processed (default to flase)")

	common.AddNoCPKFlags(cmd, true, true)

	common.AddMetadataFilterFlag(cmd)
}

func runMoveOrCopy(cmd *cobra.Command, deleteInputBlob bool) error {
	ctx := cmd.Context()
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Move")

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)
	noCPKForOutput, _ := cmd.Flags().GetBool(common.NoCPKForOutputFlag)
	runAsWatcher, _ := cmd.Flags().GetBool("watch")
	passthroughOnly, _ := cmd.Flags().GetBool("passthrough-only")
	warnSize, _ := cmd.Flags().GetInt64("warn-size")

	setMetadataForInput, metadata, err := common.GetMetadataFilter(cmd)
	if err != nil {
		log.Error(err, "GetMetadataFilter", "cmd", cmd)
		os.Exit(1)
	}

	requiredMetadata := map[string]string{}

	if passthroughOnly {
		requiredMetadata[v1alpha1.MetadataBlobType] = string(v1alpha1.PassThrough)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, requiredMetadata, metadata)

	// start executing on blob send from watcher
	task := NewMoveTask(
		inputStorageAccountURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		noCPKForInput,
		noCPKForOutput,
		runAsWatcher,
		deleteInputBlob,
		setMetadataForInput,
		metadata,
		warnSize,
	)
	if err := watchWorker.Run(ctx, task.Work, !runAsWatcher); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type moveTask struct {
	metadata                map[string]string
	inputStorageAccountURL  string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	warnSize                int64
	noCPKForOutput          bool
	runAsWatcher            bool
	deleteInputBlob         bool
	setMetadataForInput     bool
	noCPKForInput           bool
}

func NewMoveTask(inputStorageAccountURL, inputContainerName, outputStorageAccountURL, outputContainerName string,
	noCPKForInput, noCPKForOutput, runAsWatcher, deleteInputBlob, setMetadataForInput bool,
	metadata map[string]string, warnSize int64) *moveTask {
	return &moveTask{
		inputStorageAccountURL:  inputStorageAccountURL,
		inputContainerName:      inputContainerName,
		outputStorageAccountURL: outputStorageAccountURL,
		outputContainerName:     outputContainerName,
		noCPKForInput:           noCPKForInput,
		noCPKForOutput:          noCPKForOutput,
		runAsWatcher:            runAsWatcher,
		deleteInputBlob:         deleteInputBlob,
		setMetadataForInput:     setMetadataForInput,
		metadata:                metadata,
		warnSize:                warnSize,
	}
}

func (t *moveTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Move")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("get Client Provided Key: %w", err)
	}

	inputCPK := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}
	if !t.noCPKForInput {
		inputCPK = cpk
	}

	outputCPK := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, inputCPK)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &inputCPK, prop.NewMetadata())

	if !t.noCPKForOutput {
		outputCPK = cpk
	}

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	// put lease on blob if metadata must be write or if moving
	if t.setMetadataForInput || t.deleteInputBlob {
		if err := inBlob.AcquireLease(ctx); err != nil {
			return fmt.Errorf("acquire lease for '%s': %w", inBlob.BlobName(), err)
		}
		defer func() {
			if err := inBlob.ReleaseLease(context.Background()); err != nil {
				if !errors.Is(err, packageblob.ErrBlobNotFound) {
					log.Error(err, "release lease failed")
				}
			}
		}()
	}

	size, err := inBlob.Size(ctx)
	if err != nil {
		return fmt.Errorf("download blob '%s': %w", inBlob.BlobName(), err)
	}

	if t.warnSize != 0 && size > t.warnSize {
		log.Info("size exceed limit", "warnSize", t.warnSize, "size", size)
	}

	content, _, _, err := inBlob.DownloadBlobStream(ctx)
	if err != nil {
		return fmt.Errorf("can't download inblob '%s': %w", blobName, err)
	}

	uploadBlob := inBlob.Copy(outputContainerClient, &outputCPK, "")
	if err := uploadBlob.UploadBlobStream(ctx, content); err != nil {
		return fmt.Errorf("upload hdh blob '%s': %w", uploadBlob.BlobName(), err)
	}

	if t.setMetadataForInput {
		for k, v := range t.metadata {
			inBlob.Metadata()[k] = v
		}
		if err := inBlob.SetMetadata(ctx); err != nil {
			if !errors.Is(err, packageblob.ErrBlobNotFound) {
				log.Error(err, "set metadata on input blob", "blobName", inBlob.BlobName())
			}
		}
	} else if t.deleteInputBlob {
		if err := inBlob.Delete(ctx); err != nil {
			log.Error(err, "deleting")
		} else {
			log.V(1).Info("deleted successfully infile")
		}
	}

	log.Info("successfully processed")

	return nil
}
