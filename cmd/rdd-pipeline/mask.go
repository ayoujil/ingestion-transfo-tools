package main

import (
	"bufio"
	"bytes"
	"context"
	"io/ioutil"
	"os/exec"

	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"os"
	"path/filepath"
	"strings"

	"rdd-pipeline/pkg/dicomMask"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/csv"
	"rdd-pipeline/pkg/hdhpackage"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/jsonMask"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

var maskCmd = &cobra.Command{
	Use:   "mask",
	Short: "Mask a blob",
	Example: `rdd-pipeline mask \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name hash.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myInputContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--correspondence-table-storage-account-endpoint https://myCTStorageAccount.core.windows.net \
	--correspondence-table-container-name myCTContainer \
	--output-blob-prefix HDH \
	--mask-prefix HDH \
	--keyvault-endpoint https://myVault.vault.azure.net`,
	RunE: runMask,
}

func init() {
	rootCmd.AddCommand(maskCmd)

	common.AddInputBlobFlags(maskCmd)
	common.AddOutputBlobFlags(maskCmd)
	common.AddKeyVaultFlags(maskCmd)
	common.AddConfigFlags(maskCmd)
	common.AddCtFlags(maskCmd)
	common.AddPrefixFlags(maskCmd)
	common.AddOutputBlobPrefixFlags(maskCmd)
	common.AddNoCPKFlags(maskCmd, false, true)
}

func runMask(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("mask")

	log.Info("Start")
	ctStorageAccountURL, _ := cmd.Flags().GetString(common.CTAccountFlag)
	ctContainerName, _ := cmd.Flags().GetString(common.CTContainerFlag)
	noCPKForOutput, _ := cmd.Flags().GetBool(common.NoCPKForOutputFlag)
	maskPrefix, _ := cmd.Flags().GetString(common.MaskPrefixFlag)
	outputBlobPrefix, _ := cmd.Flags().GetString(common.OutputBlobPrefixFlag)

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, err := cmd.Flags().GetString(common.KeyVaultFlag)
	if err != nil {
		log.Error(err, "retrieve flag", "KeyVaultFlag", common.KeyVaultFlag)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.Hash,
		nil, nil, 0, 0)

	// start masking blob send from watcher
	task := NewMaskTask(
		pipelineConfig,
		keyvaultURL,
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
		maskPrefix,
		noCPKForOutput,
		outputBlobPrefix,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type MaskTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	ctStorageAccountURL     string
	ctContainerName         string
	prefix                  string
	noCPKForOutput          bool
	outputBlobPrefix        string
}

func NewMaskTask(
	config *v1alpha1.PipelineConfig,
	vaultURL string,
	outputStorageAccountURL string,
	outputContainerName string,
	inputContainerName string,
	ctStorageAccountURL string,
	ctContainerName string,
	prefix string,
	noCPKForOutput bool,
	outputBlobPrefix string,
) *MaskTask {
	return &MaskTask{
		config:                  config,
		vaultURL:                vaultURL,
		outputStorageAccountURL: outputStorageAccountURL,
		inputContainerName:      inputContainerName,
		outputContainerName:     outputContainerName,
		ctStorageAccountURL:     ctStorageAccountURL,
		ctContainerName:         ctContainerName,
		prefix:                  prefix,
		noCPKForOutput:          noCPKForOutput,
		outputBlobPrefix:        outputBlobPrefix,
	}
}

func (t *MaskTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Mask")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("get Client Provided Key: %w", err)
	}

	outputCPK := cpk
	if t.noCPKForOutput {
		outputCPK = azblob.ClientProvidedKeyOptions{
			EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
		}
		log.V(1).Info("Output blob will NOT be encrypted")
	}

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		return fmt.Errorf("get Key Vault client: %w", err)
	}

	inFile, err := hdhpackage.NewPackageFromBlobURL(ctx, log, blobName, blobUrl, &cpk)
	if err != nil {
		return fmt.Errorf("can't build hdh package: %w", err)
	}

	if err := inFile.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", blobName, err)
	}
	defer func() {
		if err := inFile.ReleaseLease(context.Background()); err != nil {
			log.Error(err, "release lease failed")
		}
	}()

	hashConfig, err := t.config.Hash.GetConfig(inFile.BlobName())
	if err != nil {
		return fmt.Errorf("retrieve Hash Config for '%s': %w", inFile.BlobName(), err)
	}

	if hashConfig == nil {
		return fmt.Errorf("no Hash Config matching '%s' could be found", inFile.BlobName())
	}

	var reader io.Reader

	ct := masquage.CorrespondenceTable{}

	blobVersion := inFile.Metadata()["version"]
	if blobVersion == "v1" {
		return fmt.Errorf("Wrong version: v1 is not allowed (blob: '%s')", inFile.BlobName())
	}

	blobType := inFile.Type()
	if blobType == "" {
		blobType = string(hashConfig.BlobType)
	}

	// File name masking part, applied only if the path part is present in the config file or in the file metadata config, and if there is a separator and depthlevel part into it.
	var pathConfig *v1alpha1.PathConfig
	if config := inFile.Config(); config != nil && config.PATH != nil {
		pathConfig = config.PATH
		log.V(1).Info("Read config from hdhpackage", "config", config)
	} else {
		pathConfig = hashConfig.Default.PATH
		log.V(1).Info("Read config from config file", "config", hashConfig.Default)
	}

	bKey, err := kvClient.GetSecret(ctx, hashConfig.Key.Secret)
	if err != nil && (blobType != string(v1alpha1.PassThrough) || pathConfig != nil) {
		return fmt.Errorf("cannot retrieve hash key: %w", err)
	}

	ctFileName := ""

	switch blobType {
	case string(v1alpha1.CSV):
		createCT := true
		log.Info("Run Mask CSV")
		var csvConfig *v1alpha1.CSVConfig

		if config := inFile.Config(); config != nil && config.CSV != nil {
			csvConfig = config.CSV
			log.V(1).Info("Read config from hdhpackage", "config", config)
		} else {
			csvConfig = hashConfig.Default.CSV
			log.V(1).Info("Read config from config file", "config", hashConfig.Default)
		}

		log.Info("Use config for CSV Hash", "config", csvConfig)

		if csvConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask:
- blob: "<Your regex here>"
  type: "CSV"
  key:
    secret: <secret name here>
  default:
    csv:
      separator: ","
      columnNames: ["patient_id","patient_name"]
      skipLines: [1]
      localCt: true
`)
			return fmt.Errorf("Hashconfig is empty (null)")
		}

		if csvConfig.Separator == "" {
			return fmt.Errorf("no separator found in config")
		}
		inFile.Config().CSV = csvConfig
		if t.ctStorageAccountURL == "" && t.ctContainerName == "" {
			createCT = false
		} else {
			if csvConfig.LocalCt {
				ctFile, err := ioutil.TempFile("", "ctfile")
				if err != nil {
					return fmt.Errorf("create temp file: %w", err)
				}

				_, err = ctFile.Write([]byte("{"))
				if err != nil {
					return fmt.Errorf("appending first '{' to the ct file: %w", err)
				}
				if err := ctFile.Close(); err != nil {
					log.Error(err, "close tmp file %w")
				}

				defer func() {
					if err := os.Remove(ctFile.Name()); err != nil {
						log.Error(err, "delete tmp file: %w")
					}
				}()

				ctFileName = ctFile.Name()
			}
		}

		if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
			reader = csv.NewHashReader(log, inFile, csvConfig.Separator, string(bKey), t.prefix, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, ct, createCT, ctFileName)
		} else {
			reader = inFile
		}
	case string(v1alpha1.Image):
		log.Info("Run Mask Image")
		reader = NewMaskImage(log, inFile)
	case string(v1alpha1.PassThrough):
		log.Info("Run Move Task (file type is PassThrough)")
		reader = inFile
	case string(v1alpha1.Json):
		log.Info("Run Mask Json")
		var jsonConfig *v1alpha1.JsonConfig

		if config := inFile.Config(); config != nil && config.Json != nil {
			jsonConfig = config.Json
			log.V(1).Info("Read config from hdhpackage", "config", config)
		} else {
			jsonConfig = hashConfig.Default.Json
			log.V(1).Info("Read config from config file", "config", hashConfig.Default)
		}

		if jsonConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask:
- blob: "<Your regex here>"
  type: "Json"
  key:
    secret: <secret name here>
  default:
    json:
	  tags: ["$..userId", "$..birthdate"]
`)
			return fmt.Errorf("no hashconfig found, please check your mask config file")
		}

		log.Info("Use config for Json Hash", "config", jsonConfig)
		inFile.Config().Json = jsonConfig

		if len(jsonConfig.Tags) != 0 {
			reader = jsonMask.NewHashReader(log, inFile, string(bKey), t.prefix, jsonConfig.Tags, ct, t.ctContainerName)
		} else {
			reader = inFile
		}
	case string(v1alpha1.Dicom):
		log.Info("Run Mask Dicom")
		var dicomConfig *v1alpha1.DicomConfig

		if config := inFile.Config(); config != nil && config.Dicom != nil {
			dicomConfig = config.Dicom
			log.V(1).Info("Read config from hdhpackage", "config", config)
		} else {
			dicomConfig = hashConfig.Default.Dicom
			log.V(1).Info("Read config from config file", "config", hashConfig.Default)
		}

		if dicomConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask:
- blob: "<Your regex here>"
  type: "Dicom"
  key:
    secret: <secret name here>
  default:
    dicom:
	  tags: ["0010,0020", "0010,0010"]
`)
			return fmt.Errorf("no hashconfig found, please check your mask config file")
		}

		log.Info("Use config for Dicom Hash", "config", dicomConfig)
		inFile.Config().Dicom = dicomConfig

		maskScriptPath := "/dicomMaskScripts/maskDicomFile.rb"
		if len(dicomConfig.Tags) != 0 {
			reader = dicomMask.NewHashReader(log, inFile, string(bKey), t.prefix, dicomConfig.Tags, ct, maskScriptPath)
		} else {
			reader = inFile
		}
	default:
		return fmt.Errorf("unsupported blob type '%s' for blob '%s'", blobType, blobName)
	}

	// File name Masking
	var currentBlobName string
	currentBlobName = inFile.BlobName()
	if pathConfig != nil && pathConfig.DepthLevel != nil {
		log.Info("Path masking config found, proceeding.")

		if pathConfig.Separator == "" {
			return fmt.Errorf("no separator found in config")
		}

		currentBlobName = maskPath(log, currentBlobName, pathConfig.Separator, pathConfig.DepthLevel, string(bKey), t.prefix, t, ct, ctFileName)
		inFile.Config().PATH = pathConfig
	} else {
		log.Info("No path masking config found, skipping this step.")
	}
	var outBlobName string
	if t.outputBlobPrefix != "" {
		outBlobName = t.outputBlobPrefix + "_" + currentBlobName
	} else {
		outBlobName = currentBlobName
	}

	outFile := inFile.Copy(reader, outputContainerClient, &outputCPK, outBlobName).(hdhpackage.HDHPackage)
	outFile.Metadata()[v1alpha1.MetadataBlobType] = blobType

	log = log.WithValues("blobName", outFile.BlobName())
	log.V(2).Info("outFile copy", "config", outFile.Config())

	if err := outFile.Upload(ctx); err != nil {
		return fmt.Errorf("upload hdh file: %w", err)
	}

	if t.ctStorageAccountURL != "" && t.ctContainerName != "" {
		ctContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.ctStorageAccountURL, t.ctContainerName)
		if err != nil {
			return fmt.Errorf("build CT container client from storage account: %w", err)
		}

		// ctFileName != "" means that we use a local file as ct. It needs to be modified to fit our needs (no duplicated lines, json format)
		if ctFileName != "" {
			ctFile, err := os.OpenFile(ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
			if err != nil {
				return fmt.Errorf("opening tmp ct file : %w", err)
			}

			_, err = ctFile.Write([]byte("\n}"))
			if err != nil {
				return fmt.Errorf("appending last '}' to the ct file: %w", err)
			}

			if err := ctFile.Close(); err != nil {
				log.Error(err, "close tmp file %w")
			}

			// Apply Uniq to the file
			cmd := exec.Command( //nolint:gosec
				"uniq",
				ctFileName)

			ctOutfile, err := os.Create(ctFileName + "_uniq")
			if err != nil {
				panic(err)
			}

			defer func() {
				if err := os.Remove(ctOutfile.Name()); err != nil {
					log.Error(err, "delete tmp ct out file: %w")
				}
			}()

			var stderr bytes.Buffer
			cmd.Stdout = ctOutfile
			cmd.Stderr = &stderr
			err = cmd.Run()
			ctOutfile.Close()

			if err != nil {
				return fmt.Errorf("during uniq call for the ct file: %w", err)
			}

			file, err := os.Open(ctOutfile.Name())
			if err != nil {
				return fmt.Errorf("opening the ct file for transfert: %w", err)
			}

			read := bufio.NewReader(file)
			ctBlob := packageblob.NewBlobFromContainerClient(outBlobName, ctContainerClient, &cpk, nil)
			if err := ctBlob.UploadBlobStream(ctx, read); err != nil {
				return fmt.Errorf("upload ct data to '%s': %w", outBlobName, err)
			}

		} else {
			var ctEncoder bytes.Buffer
			err = json.NewEncoder(&ctEncoder).Encode(ct)
			if err != nil {
				return fmt.Errorf("create ct json file: %w", err)
			}
			ctBlob := packageblob.NewBlobFromContainerClient(outBlobName, ctContainerClient, &cpk, nil)
			if err := ctBlob.UploadBlobStream(ctx, &ctEncoder); err != nil {
				return fmt.Errorf("upload ct data to '%s': %w", outBlobName, err)
			}
			log.V(1).Info("ct processed successfully", "entries", len(ct))
		}
	}

	if err := inFile.Delete(ctx); err != nil {
		log.Error(err, "Could not delete")
	} else {
		log.V(1).Info("deleted successfully infile")
	}

	log.Info("successfully processed")
	return nil
}

// Remove the file metadata by decoding/encoding the blob
func NewMaskImage(log logr.Logger, inFile hdhpackage.HDHPackage) io.Reader {
	// emulate a reader
	rOut, wOut := io.Pipe()

	go (func() {
		img, _, err := image.Decode(inFile)
		if err != nil {
			if err := wOut.CloseWithError(err); err != nil {
				log.Error(err, "error decoding image")
				return
			}
		}

		err = jpeg.Encode(wOut, img, nil)
		if err != nil {
			if err := wOut.CloseWithError(err); err != nil {
				log.Error(err, "error encoding image")
				return
			}
		}

		if err := wOut.Close(); err != nil {
			log.Error(err, "error closing io.pipe : out")
			return
		}
	})()

	return rOut
}

func maskPath(log logr.Logger, BlobNameBeforeMasking string, separator string, depthLevels []int, bKey string, maskPrefix string, t *MaskTask, ct masquage.CorrespondenceTable, ctFileName string) string {
	// Description: Mask the string given as "BlobNameBeforeMasking", using "separator" as separator, and "depthLevels" as a list of subsets of the "BlobNameBeforeMasking" to mask.
	// Returns the newly masked value, as follow: <maskPrefix:maskedValue> and also add the hdh-mask <-> prod-mask if we need to
	// bKey: the key used to mask the data
	// maskPrefix: will be added before the newly masked value, with the following pattern: <maskPrefix:maskedValue>
	// ct is the same ct than for the file content --> not optimal from a performance perspective when we have to reverse it
	var BlobNameOutput string = ""
	var fileExtension string = filepath.Ext(BlobNameBeforeMasking)
	var fileExtensionAppend string = ""

	// If the last extension is .gpg, we have to ignore the second extension which will be the file's extension
	if fileExtension == ".gpg" {
		BlobNameBeforeMasking = BlobNameBeforeMasking[:strings.LastIndex(BlobNameBeforeMasking, ".")]
		fileExtensionAppend = fileExtension
	}

	// Second level of file extension: compression. This also have to be ignored
	fileExtension = filepath.Ext(BlobNameBeforeMasking)
	if fileExtension == ".gzip" || fileExtension == ".gz" {
		BlobNameBeforeMasking = BlobNameBeforeMasking[:strings.LastIndex(BlobNameBeforeMasking, ".")]
		fileExtensionAppend = fileExtension + fileExtensionAppend
	}

	// The extension is kept apart until the end of the path masking process
	fileExtension = filepath.Ext(BlobNameBeforeMasking)
	BlobNameBeforeMasking = BlobNameBeforeMasking[:strings.LastIndex(BlobNameBeforeMasking, ".")]
	fileExtensionAppend = fileExtension + fileExtensionAppend
	split := strings.SplitAfter(BlobNameBeforeMasking, separator)

	for _, v := range depthLevels {
		if (v-1 < len(split)) && (v-1 >= 0) {
			var oldName = split[v-1]
			split[v-1], _ = masquage.Hash(split[v-1], bKey)
			split[v-1] = maskPrefix + ":" + split[v-1]
			// Applied only when there is a CT (currently in maskProd): the file name masking association (hdh mask-prod mask) is in the same CT as the file content masking association
			if t.ctStorageAccountURL != "" && t.ctContainerName != "" {
				if ctFileName == "" {
					ct[split[v-1]] = strings.TrimSuffix(oldName, separator)
				} else {
					ctFile, err := os.OpenFile(ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
					if err != nil {
						log.Error(err, "opening tmp ct file : %w")
					}

					_, err = ctFile.Write([]byte("\n" + "\"" + split[v-1] + "\"" + ":" + "\"" + strings.TrimSuffix(oldName, separator) + "\","))
					if err != nil {
						log.Error(err, "adding the path name part to the local ct: %w")
					}

					if err := ctFile.Close(); err != nil {
						log.Error(err, "close tmp file %w")
					}

				}
			}
			if v != len(split) {
				split[v-1] += separator
			}
		} else {
			log.Info("Path index not existing, skipping this index (info: depthlevel starts at 1)")
		}
	}

	for i := range split {
		BlobNameOutput += split[i]
	}
	BlobNameOutput = strings.TrimSuffix(BlobNameOutput, separator)
	BlobNameOutput += fileExtensionAppend
	return BlobNameOutput
}
