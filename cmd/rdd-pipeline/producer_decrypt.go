package main

import (
	"bytes"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/crypt/gpg"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	v2 "rdd-pipeline/pkg/hdhpackage/v2"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"

	//nolint
	"golang.org/x/crypto/openpgp/armor"
)

var producerDecryptCmd = &cobra.Command{
	Use:   "decrypt",
	Short: "Decrypt data from the SFTP import",
	RunE:  runDecryptKeyvault,
	Example: `rdd-pipeline producer decrypt \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name decryption.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--keyvault-endpoint https://myVault.vault.azure.net`,
}

func init() {
	producerCmd.AddCommand(producerDecryptCmd)

	common.AddInputBlobFlags(producerDecryptCmd)
	common.AddOutputBlobFlags(producerDecryptCmd)

	common.AddKeyVaultFlags(producerDecryptCmd)
	common.AddConfigFlags(producerDecryptCmd)
}

func runDecryptKeyvault(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("producer decrypt")

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, err := cmd.Flags().GetString(common.KeyVaultFlag)
	if err != nil {
		log.Error(err, "retrieve flag", "KeyVaultFlag", common.KeyVaultFlag)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.Decrypt,
		nil, nil, 0, 0)

	// start executing on blob sent from watcher
	task := NewProducerDecryptTask(
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	return nil
}

type ProducerDecryptTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
}

func NewProducerDecryptTask(
	pipelineConfig *v1alpha1.PipelineConfig,
	keyvaultURL,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName string,
) *ProducerDecryptTask {
	return &ProducerDecryptTask{
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
	}
}

func (t *ProducerDecryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl
	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Producer Decrypt")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("failed to get Client Provided Key: %w", err)
	}

	inputCPK := cpk
	outputCPK := cpk

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, inputCPK)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &inputCPK, prop.NewMetadata())

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	if err := inBlob.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", blobName, err)
	}
	defer func() {
		if err := inBlob.ReleaseLease(context.Background()); err != nil {
			if !errors.Is(err, packageblob.ErrBlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}
	}()

	decryptConfig, err := t.config.Pipeline.Decrypt.GetConfig(blobName)
	if err != nil {
		return fmt.Errorf("retrieve Decrypt Config for '%s': %w", blobName, err)
	}

	if decryptConfig == nil {
		return fmt.Errorf("no Decrypt Config matching '%s' could be found", blobName)
	}

	content, _, _, err := inBlob.DownloadBlobStream(ctx)
	if err != nil {
		return fmt.Errorf("can't download inblob '%s': %w", blobName, err)
	}

	// Subcase: The blob section does not have a specific "gpg" part, so we apply the default one
	if decryptConfig.GPG.SignatureKey.Secret == "" && decryptConfig.GPG.DecryptKey.Secret == "" {
		log.Info("No specific GPG configuration found for the blob, applying DefaultGPG")
		decryptConfig.GPG = t.config.Pipeline.DefaultGPG
	} else {
		// Subcase required when the blob regex has been assigned with defaultgpg decryption configuration
		// Without this subcase, we can't detect that the decryptConfig has previously been assigned with the defaultgpg value
		// caveat: if a custom config matches the default config, we will have the "applying DefaultGPG" message
		if decryptConfig.GPG == t.config.Pipeline.DefaultGPG {
			log.Info("No specific GPG configuration found for the blob, applying DefaultGPG")
		}
	}

	log.Info("current GPG configuration for the blob", "decryptConfig.GPG", decryptConfig.GPG)
	var compressedData io.Reader
	if decryptConfig.GPG.SignatureKey.Secret != "" && decryptConfig.GPG.DecryptKey.Secret != "" && decryptConfig.GPG.DecryptKey.PassphraseSecret != "" {
		compressedData, err = decryptGPG(ctx, log, content, t.vaultURL, decryptConfig.GPG)
		if err != nil {
			return fmt.Errorf("decryptGPG failed for blob '%s': %w", blobName, err)
		}
	} else {
		return fmt.Errorf("missing correct GPG configuration for blob '%s', please check your decryption.yaml file", blobName)
	}

	var decompressedData io.Reader
	// Decompress
	switch decryptConfig.Compression.Format {
	case v1alpha1.Gzip:
		log.V(2).Info("gunzip data")
		decompressedData, err = gzip.NewReader(compressedData)
		if err != nil {
			return fmt.Errorf("gunzip for blob '%s': %w", blobName, err)
		}
	default:
		decompressedData = compressedData
	}

	outBlobUrl := outputContainerClient.NewBlockBlobURL(inBlob.Name)
	outFile, err := v2.NewFromFile(inBlob.Name, decompressedData, &outBlobUrl, &outputCPK, inBlob.Metadata())
	if err != nil {
		return fmt.Errorf("can't create HDHPackage V2 from file: %w", err)
	}

	if err := outFile.Upload(ctx); err != nil {
		return fmt.Errorf("upload blob '%s' as stream: %w", blobName, err)
	}

	if err := inBlob.Delete(ctx); err != nil {
		log.Error(err, "Warning: Could not delete blob")
	} else {
		log.V(1).Info("Successfully blob deletion")
	}

	log.Info("successfully processed")
	return nil
}

func decryptGPG(ctx context.Context, log logr.Logger, content io.Reader, vaultURL string, config v1alpha1.GPGConfig) (io.Reader, error) {
	log.V(1).Info("Decrypt wih GPG")

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(vaultURL)
	if err != nil {
		return nil, fmt.Errorf("get Key Vault client: %w", err)
	}

	bSignatureKey, err := kvClient.GetSecret(ctx, config.SignatureKey.Secret)
	if err != nil {
		return nil, fmt.Errorf("open public key '%s': %w", config.SignatureKey.Secret, err)
	}

	bPrivateKey, err := kvClient.GetSecret(ctx, config.DecryptKey.Secret)
	if err != nil {
		return nil, fmt.Errorf("open private key '%s': %w", config.DecryptKey.Secret, err)
	}

	var passphrase []byte
	if config.DecryptKey.Password != "" {
		passphrase = []byte(config.DecryptKey.Password)
	}

	if config.DecryptKey.PassphraseSecret != "" {
		passphrase, err = kvClient.GetSecret(ctx, config.DecryptKey.PassphraseSecret)
		if err != nil {
			return nil, fmt.Errorf("open passphrase secret '%s': %w", config.DecryptKey.PassphraseSecret, err)
		}
	}

	signatureKey := bytes.NewReader(bSignatureKey)
	privateKey := bytes.NewReader(bPrivateKey)

	messageReader := content

	if config.Content.Armored {
		block, err := armor.Decode(content)
		if err != nil {
			return nil, err
		}
		messageReader = block.Body
	}

	data, err := gpg.Decrypt(log, messageReader, signatureKey, privateKey, passphrase)
	if err != nil {
		return nil, fmt.Errorf("decrypt data: %w", err)
	}

	return data, nil
}
