package main

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
	"golang.org/x/sync/errgroup"
)

var producerEncryptCmd = &cobra.Command{
	Use:   "encrypt",
	Short: "Encrypt a blob",
	RunE:  runHdhEncrypt,
	Example: `rdd-pipeline producer encrypt \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-container myOutputContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-container myOutputContainer \
	--catalog-blob-name catalog.csv
	--keyvault-endpoint https://myVault.vault.azure.net \
	--wraped-key-name wrapedkey \
	--keyvault-wrapping-key-name keyvaultkey \
	--key-secret-name encrypt`,
}

func init() {
	producerCmd.AddCommand(producerEncryptCmd)

	common.AddInputBlobFlags(producerEncryptCmd)
	common.AddOutputBlobFlags(producerEncryptCmd)
	common.AddCatalogBlobFlags(producerEncryptCmd, false)

	common.AddKeyVaultFlags(producerEncryptCmd)

	common.AddKeySecretNameFlags(producerEncryptCmd)

	common.AddNoCPKFlags(producerEncryptCmd, true, false)

	common.AddMetadataKeyFlags(producerEncryptCmd)
}

func runHdhEncrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("producer encrypt")

	log.Info("Start")

	keyVaultKey, _ := cmd.Flags().GetString(common.KeySecretNameFlag)
	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, err := cmd.Flags().GetString(common.KeyVaultFlag)
	if err != nil {
		log.Error(err, "KeyVaultFlag", common.KeyVaultFlag) //nolint
		os.Exit(1)
	}

	keyVaultKeyName, wrapedKeyName, err := common.GetMetadataKeyFlags(cmd)
	if err != nil {
		log.Error(err, "retrieve flags")
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil)

	// start executing on blob send from watcher
	task := NewProducerEncryptTask(
		keyvaultURL,
		keyVaultKey,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrapedKeyName,
		noCPKForInput,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	// execute listing if needed
	if catalogBlobName, err := cmd.Flags().GetString(common.CatalogBlobNameFlag); err == nil && catalogBlobName != "" {
		if err := runCatalog(cmd, outputStorageAccountURL, outputContainerName); err != nil {
			return fmt.Errorf("build catalog failed: %w", err)
		}
	}

	return nil
}

type ProducerEncryptTask struct {
	vaultURL                string
	keyVaultKey             string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	keyVaultKeyName         string
	wrapedKeyName           string
	noCPKForInput           bool
}

func NewProducerEncryptTask(
	keyVaultURL,
	keyVaultKey,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName,
	keyVaultKeyName,
	wrapedKeyName string,
	noCPKForInput bool,
) *ProducerEncryptTask {
	return &ProducerEncryptTask{
		keyVaultURL,
		keyVaultKey,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrapedKeyName,
		noCPKForInput,
	}
}

func (t *ProducerEncryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("process producer encrypt")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		return fmt.Errorf("get Key Vault client: %w", err)
	}

	inputCPK := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	if !t.noCPKForInput {
		inputCPK, err = rddblob.NewClientProvidedKeyOptions()
		if err != nil {
			return fmt.Errorf("get Client Provided Key: %w", err)
		}
	}

	outputCpk := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, inputCPK)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &inputCPK, prop.NewMetadata())

	if err := inBlob.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", inBlob.BlobName(), err)
	} else {
		log.Info("Lease acquired", "blobName", inBlob.BlobName())
	}
	defer func() {
		if err := inBlob.ReleaseLease(context.Background()); err != nil {
			if !errors.Is(err, packageblob.ErrBlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}
	}()

	// Generate Key
	key, err := hdhcrypt.GenerateKey()
	if err != nil {
		return fmt.Errorf("generate key: %w", err)
	}

	// Wrap key to be stored in metadata
	wrapedKey, err := kvClient.Wrap(ctx, t.keyVaultKey, key)
	if err != nil {
		return fmt.Errorf("wrap key: %w", err)
	}

	pr, pw := io.Pipe()
	defer pr.Close()

	stream, err := hdhcrypt.GetStreamWriter(key, pw)
	if err != nil {
		return fmt.Errorf("getStreamWriter: %w", err)
	}

	inBlob.Metadata()[t.wrapedKeyName] = base64.StdEncoding.EncodeToString(wrapedKey)
	inBlob.Metadata()[t.keyVaultKeyName] = t.keyVaultKey

	g, gctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		defer stream.Close()

		content, _, _, err := inBlob.DownloadBlobStream(gctx)
		if err != nil {
			return fmt.Errorf("can't download inblob '%s': %w", blobName, err)
		}
		log.Info("download ok", "content", content)

		if _, err := io.Copy(stream, content); err != nil {
			return fmt.Errorf("can't copy data: %w", err)
		}
		return nil
	})

	g.Go(func() error {
		defer pr.Close()

		uploadBlob := inBlob.Copy(outputContainerClient, &outputCpk, "")
		if err := uploadBlob.UploadBlobStream(gctx, pr); err != nil {
			return fmt.Errorf("upload blob '%s' as stream: %w", inBlob.BlobName(), err)
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	if err := inBlob.Delete(ctx); err != nil {
		log.Error(err, "DeleteBlobWithLease")
	} else {
		log.V(1).Info("Blob deleted successfully")
	}

	log.Info("successfully processed")
	return nil
}
