package main

import (
	"context"
	"errors"
	"fmt"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	v2 "rdd-pipeline/pkg/hdhpackage/v2"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

// prepConvertCmd represents the convert command in preparation env.
var prepConvertCmd = &cobra.Command{
	Use:   "convert",
	Short: "Convert a blob to passthrough hdhpackage v2",
	RunE:  runPrepConvert,
	Example: `rdd-pipeline preparation convert \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer`,
}

func init() {
	preparationCmd.AddCommand(prepConvertCmd)

	common.AddInputBlobFlags(prepConvertCmd)
	common.AddOutputBlobFlags(prepConvertCmd)
}

func runPrepConvert(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Preparation Convert")

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, nil, nil)

	// start executing on blob sent from watcher
	task := NewPreparationConvertTask(outputStorageAccountURL, outputContainerName, inputContainerName)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationConvertTask struct {
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
}

func NewPreparationConvertTask(outputStorageAccountURL, outputContainerName, inputContainerName string) *PreparationConvertTask {
	return &PreparationConvertTask{
		inputContainerName:      inputContainerName,
		outputStorageAccountURL: outputStorageAccountURL,
		outputContainerName:     outputContainerName,
	}
}

func (t *PreparationConvertTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Convert")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("get Client Provided Key: %w", err)
	}

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, cpk)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &cpk, prop.NewMetadata())

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	if err := inBlob.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", blobName, err)
	}
	defer func() {
		if err := inBlob.ReleaseLease(context.Background()); err != nil {
			if !errors.Is(err, packageblob.ErrBlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}
	}()

	bContent, _, _, err := inBlob.DownloadBlobStream(ctx)
	if err != nil {
		return fmt.Errorf("download blob '%s': %w", inBlob.Name, err)
	}
	defer bContent.Close()

	outBlobUrl := outputContainerClient.NewBlockBlobURL(inBlob.Name)
	outFile, err := v2.NewFromFile(inBlob.Name, bContent, &outBlobUrl, &cpk, inBlob.Metadata())
	if err != nil {
		return fmt.Errorf("can't create HDHPackage V2 from file: %w", err)
	}

	outFile.Metadata()[v1alpha1.MetadataVersion] = v2.Version

	if err := outFile.Upload(ctx); err != nil {
		return fmt.Errorf("upload blob '%s' as stream: %w", blobName, err)
	}

	log.Info("successfully processed")
	return nil
}
