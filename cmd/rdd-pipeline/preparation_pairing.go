package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/csv"
	"rdd-pipeline/pkg/dicomMask"
	"rdd-pipeline/pkg/hdhpackage"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/jsonMask"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

var pairingCmd = &cobra.Command{
	Use:   "pairing",
	Short: "Pair data with a producer",
	RunE:  runPairing,
	Example: `rdd-pipeline preparation pairing \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--correspondence-table-storage-account-endpoint https://myCTStorageAccount.core.windows.net \
	--correspondence-table-container-name ctContainer`,
}

func init() {
	preparationCmd.AddCommand(pairingCmd)

	common.AddInputBlobFlags(pairingCmd)
	common.AddOutputBlobFlags(pairingCmd)

	pairingCmd.Flags().String(common.CTAccountFlag, "", "Blob storage account Name")
	(cobra.MarkFlagRequired(pairingCmd.Flags(), common.CTAccountFlag))

	pairingCmd.Flags().String(common.CTContainerFlag, "", "container name where files are stored")
	(cobra.MarkFlagRequired(pairingCmd.Flags(), common.CTContainerFlag))
}

func runPairing(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Pairing")

	log.Info("Start")

	ctStorageAccountURL, ctContainerName, err := common.GetCtStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetCtStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, nil, nil)

	task := NewPreparationPairingTask(
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
	)

	// start executing on blob sent from watcher
	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationPairingTask struct {
	outputStorageAccountURL string
	outputContainerName     string
	inputContainerName      string
	ctStorageAccountURL     string
	ctContainerName         string
}

func NewPreparationPairingTask(
	outputStorageAccountURL,
	outputContainerName,
	inputContainerName,
	ctStorageAccountURL,
	ctContainerName string,
) *PreparationPairingTask {
	return &PreparationPairingTask{
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
	}
}

func (t *PreparationPairingTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Preparation Pairing")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("failed to get Client Provided Key: %w", err)
	}

	inputCPK := cpk
	outputCPK := cpk

	inFile, err := hdhpackage.NewPackageFromBlobURL(ctx, log, blobName, blobUrl, &inputCPK)
	if err != nil {
		return fmt.Errorf("can't build hdh package: %w", err)
	}

	outputContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		return fmt.Errorf("create Output container client: %w", err)
	}

	ctContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.ctStorageAccountURL, t.ctContainerName)
	if err != nil {
		return fmt.Errorf("create CT container client: %w", err)
	}

	ctBlob := packageblob.NewBlobFromContainerClient(inFile.BlobName(), ctContainerClient, &cpk, nil)
	ctContent, _, err := ctBlob.DownloadBlob(ctx)
	if err != nil {
		return fmt.Errorf("download CT blob '%s': %w", blobName, err)
	}

	ct := new(masquage.CorrespondenceTable)
	if err := json.Unmarshal(ctContent, ct); err != nil {
		return fmt.Errorf("failed to Unmarshall ct from '%s': %w", blobName, err)
	}

	if err := inFile.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", blobName, err)
	}
	defer func() {
		err := inFile.ReleaseLease(context.Background())
		if err != nil {
			log.Error(err, "release lease failed")
		}
	}()

	var reader io.Reader
	blobVersion := inFile.Metadata()["version"]
	if blobVersion == "v1" {
		return fmt.Errorf("Wrong version: v1 is not allowed (blob: '%s')", blobName)
	}

	blobType := inFile.Metadata()["type"]

	switch blobType {
	case string(v1alpha1.CSV):
		log.Info("Run pairing CSV")

		var csvConfig *v1alpha1.CSVConfig

		if config := inFile.Config(); config != nil && config.CSV != nil {
			csvConfig = config.CSV
			log.V(1).Info("Read config from hdhpackage", "config", config)
		}

		log.Info("Use config for CSV pairing", "config", csvConfig)
		if csvConfig.Separator == "" {
			return fmt.Errorf("no separator found in config")
		}

		if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
			reader = csv.NewPairingReader(log, inFile, csvConfig.Separator, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, *ct)
		} else {
			reader = inFile
		}
	case string(v1alpha1.PassThrough):
		log.Info("Run Move Task (file type is PassThrough)")
		reader = inFile

	case string(v1alpha1.Json):
		var jsonConfig *v1alpha1.JsonConfig

		if config := inFile.Config(); config != nil && config.Json != nil {
			jsonConfig = config.Json
			log.V(1).Info("Read config from hdhpackage", "config", config)
		}

		if len(jsonConfig.Tags) != 0 {
			reader = jsonMask.NewPairingReader(log, inFile, jsonConfig.Tags, *ct)
		} else {
			reader = inFile
		}
	case string(v1alpha1.Dicom):
		log.Info("Run pairing Dicom")

		var dicomConfig *v1alpha1.DicomConfig

		if config := inFile.Config(); config != nil && config.Dicom != nil {
			dicomConfig = config.Dicom
			log.V(1).Info("Read config from hdhpackage", "config", config)
		}
		pairingScriptPath := "/dicomMaskScripts/pairingDicom.rb"
		if len(dicomConfig.Tags) != 0 {
			reader = dicomMask.NewPairingReader(log, inFile, dicomConfig.Tags, *ct, pairingScriptPath)
		} else {
			reader = inFile
		}
	default:
		return fmt.Errorf("unsupported blob type '%s' for blob '%s'", blobType, blobName)
	}

	var currentBlobName string
	currentBlobName = inFile.BlobName()
	currentBlobName = pairingPath(log, currentBlobName, *ct)

	outFile := inFile.Copy(reader, outputContainerClient, &outputCPK, currentBlobName).(hdhpackage.HDHPackage)
	if err := outFile.Upload(ctx); err != nil {
		return fmt.Errorf("upload hdh blob '%s': %w", outFile.BlobName(), err)
	}

	if err := ctBlob.Delete(ctx); err != nil {
		log.Error(err, "correspondence table deletion error")
	} else {
		log.V(1).Info("Correspondence table deleted successfully")
	}

	if err := inFile.Delete(ctx); err != nil {
		log.Error(err, "Warning: Could not delete blob")
	} else {
		log.V(1).Info("Successfully blob deletion")
	}

	log.Info("successfully processed")

	return nil
}

func pairingPath(log logr.Logger, BlobName string, ct masquage.CorrespondenceTable) string {
	// For each CT element: replaceAll in the file name. To be optimized
	for key := range ct {
		BlobName = strings.ReplaceAll(BlobName, key, ct[key])
	}

	return BlobName
}
