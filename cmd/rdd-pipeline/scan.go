package main

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	common "rdd-pipeline/cmd"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/clamav"
	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/utils"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
)

var producerScanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Scan a file with ClamAV",
	RunE:  runScan,
	Example: `rdd-pipeline scan \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container inputContainer \
	--output-storage-account-endpoint https://myRetentionStorageAccount.core.windows.net \
	--output-container retentionContainer \
	--quarantine-storage-account-endpoint https://myQuarantineStorageAccount.core.windows.net \
	--quarantine-container quarantineContainer`,
}

func init() {
	rootCmd.AddCommand(producerScanCmd)

	common.AddInputBlobFlags(producerScanCmd)
	common.AddOutputBlobFlags(producerScanCmd)

	producerScanCmd.Flags().String(common.QuarantineAccountFlag, "", "Quarantine storage account endpoint")
	(cobra.MarkFlagRequired(producerScanCmd.Flags(), common.QuarantineAccountFlag))

	producerScanCmd.Flags().String(common.QuarantineContainerFlag, "", "Quarantine container to output the errored blob")
	(cobra.MarkFlagRequired(producerScanCmd.Flags(), common.QuarantineContainerFlag))

	common.AddNoCPKFlags(producerScanCmd, true, false)
}

func runScan(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("scan")

	log.Info("Start")

	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	retentionStorageAccountURL, retentionContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	quarantineStorageAccountURL, quarantineContainerName, err := common.GetQuarantineStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetQuarantineStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	scanner, err := clamav.New(log)
	if err != nil {
		log.Error(err, "clam")
		os.Exit(1)
	}

	exitCode, err := scanner.Update()
	if err != nil {
		log.Error(err, "first clam update", "exitCode", exitCode)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil)

	// start executing on blob sent from watcher
	task := NewScanTask(
		scanner,
		retentionStorageAccountURL,
		retentionContainerName,
		inputContainerName,
		quarantineStorageAccountURL,
		quarantineContainerName,
		noCPKForInput,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type ScanTask struct {
	scanner                     *clamav.Scanner
	retentionStorageAccountURL  string
	retentionContainerName      string
	inputContainerName          string
	quarantineStorageAccountURL string
	quarantineContainerName     string
	noCPKForInput               bool
}

func NewScanTask(
	scanner *clamav.Scanner,
	retentionStorageAccountURL,
	retentionContainerName,
	inputContainerName,
	quarantineStorageAccountURL,
	quarantineContainerName string,
	noCPKForInput bool,
) *ScanTask {
	return &ScanTask{
		retentionStorageAccountURL:  retentionStorageAccountURL,
		retentionContainerName:      retentionContainerName,
		inputContainerName:          inputContainerName,
		quarantineStorageAccountURL: quarantineStorageAccountURL,
		quarantineContainerName:     quarantineContainerName,
		scanner:                     scanner,
		noCPKForInput:               noCPKForInput,
	}
}

func (t *ScanTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	blobUrl := in.(worker.Job).BlobUrl

	blobName := utils.BlobNameFull(&blobUrl.BlobURL, t.inputContainerName)

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Scan")
	log = log.WithValues("blobName", blobName)

	log.Info("Start")

	cpk, err := rddblob.NewClientProvidedKeyOptions()
	if err != nil {
		return fmt.Errorf("get Client Provided Key: %w", err)
	}

	retentionContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.retentionStorageAccountURL, t.retentionContainerName)
	if err != nil {
		return fmt.Errorf("create Retention container client: %w", err)
	}

	quarantineContainerClient, _, err := rddblob.NewContainerClientFromEnvironment(log, t.quarantineStorageAccountURL, t.quarantineContainerName)
	if err != nil {
		return fmt.Errorf("create Quarantine container client: %w", err)
	}

	inputCPK := cpk
	if t.noCPKForInput {
		inputCPK = azblob.ClientProvidedKeyOptions{
			EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
		}
	}

	outputCPK := cpk

	prop, err := blobUrl.GetProperties(ctx, azblob.BlobAccessConditions{}, inputCPK)
	if err != nil {
		return fmt.Errorf("can't getProperties: %w", err)
	}
	inBlob := packageblob.NewBlob(blobName, blobUrl, &inputCPK, prop.NewMetadata())

	if err := inBlob.AcquireLease(ctx); err != nil {
		return fmt.Errorf("acquire lease for '%s': %w", inBlob.BlobName(), err)
	}
	defer func() {
		if err := inBlob.ReleaseLease(context.Background()); err != nil {
			if !errors.Is(err, packageblob.ErrBlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}
	}()

	file, err := ioutil.TempFile("", "scanfile")
	if err != nil {
		return fmt.Errorf("create temp file: %w", err)
	}

	log.V(2).Info("create file for scan", "filename", file.Name())

	defer func() {
		if err := file.Close(); err != nil {
			log.Error(err, "close tmp file", "file", file.Name())
		}

		if err := os.Remove(file.Name()); err != nil {
			log.Error(err, "delete tmp file", "file", file.Name())
		}
	}()

	if err := file.Chmod(0o644); err != nil {
		return fmt.Errorf("chmod file: %w", err)
	}

	content, _, _, err := inBlob.DownloadBlobStream(ctx)
	if err != nil {
		return fmt.Errorf("can't download inblob '%s': %w", blobName, err)
	}

	if _, err := file.ReadFrom(content); err != nil {
		return fmt.Errorf("read from stream file: %w", err)
	}

	if err := file.Sync(); err != nil {
		return fmt.Errorf("sync file: %w", err)
	}

	// reset pointer to BOF
	_, err = file.Seek(0, 0)
	if err != nil {
		return fmt.Errorf("file seek failed : %w", err)
	}

	if exitCode, err := t.scanner.Scan(file.Name()); err != nil {
		switch exitCode {
		case 1:
			log.Error(err, "some virus founds")
			quarantineBlob := inBlob.Copy(quarantineContainerClient, &outputCPK, "")
			if err := quarantineBlob.UploadBlobStream(ctx, file); err != nil {
				return fmt.Errorf("upload blob '%s' to quarantine: %w", quarantineBlob.BlobName(), err)
			}
		default:
			return fmt.Errorf("some errors occurred on blob '%s' (exitCode: %v): %w", inBlob.BlobName(), exitCode, err)
		}
	} else {
		log.V(1).Info("Scan succeeded")

		retentionBlob := inBlob.Copy(retentionContainerClient, &outputCPK, "")
		if err := retentionBlob.UploadBlobStream(ctx, file); err != nil {
			return fmt.Errorf("upload blob '%s' to retention: %w", retentionBlob.BlobName(), err)
		}
	}

	if err := inBlob.Delete(ctx); err != nil {
		log.Error(err, "Could not delete blob")
	} else {
		log.V(1).Info("Successfully blob deletion")
	}

	log.Info("successfully processed")
	return nil
}
