module rdd-pipeline

go 1.16

require (
	github.com/Azure/azure-pipeline-go v0.2.3
	github.com/Azure/azure-sdk-for-go v58.2.0+incompatible
	github.com/Azure/azure-sdk-for-go/sdk/azidentity v0.13.0
	github.com/Azure/azure-sdk-for-go/sdk/keyvault/azsecrets v0.6.0
	github.com/Azure/azure-sdk-for-go/sdk/storage/azblob v0.3.0
	github.com/Azure/azure-storage-blob-go v0.14.0
	github.com/Azure/azure-storage-queue-go v0.0.0-20191125232315-636801874cdd
	github.com/Azure/go-autorest/autorest/adal v0.9.16
	github.com/Azure/go-autorest/autorest/azure/auth v0.5.8
	github.com/Azure/go-autorest/autorest/to v0.4.0
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/ProtonMail/gopenpgp/v2 v2.2.4
	github.com/blang/semver v3.5.1+incompatible
	github.com/go-logr/logr v1.1.0
	github.com/go-logr/zerologr v1.1.4
	github.com/google/uuid v1.3.0
	github.com/gruntwork-io/terratest v0.38.2
	github.com/guilhem/chunkreaderat v0.1.0
	github.com/guilhem/gorkers v0.0.1
	github.com/klauspost/compress v1.13.6
	github.com/libp2p/go-openssl v0.0.7
	github.com/mattn/go-pointer v0.0.1 // indirect
	github.com/pkg/sftp v1.13.4
	github.com/rs/zerolog v1.25.0
	github.com/spacemonkeygo/openssl v0.0.0-20181017203307-c2dcc5cca94a // indirect
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/yaml.v2 v2.4.0
)

replace github.com/ProtonMail/gopenpgp/v2 => github.com/guilhem/gopenpgp/v2 v2.2.5-0.20211003175636-4a57375c7e92
