package dicomMask

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type PairingReader struct {
	in                io.Reader
	Tags              []string
	currentline       int
	dicomWriter       *io.PipeWriter
	out               *io.PipeReader
	ct                masquage.CorrespondenceTable
	log               logr.Logger
	pairingScriptPath string
}

func NewPairingReader(log logr.Logger, in io.Reader, tags []string, ct masquage.CorrespondenceTable, pairingScriptPath string) io.Reader {
	dicomReader := in
	rOut, wOut := io.Pipe()

	dicomWriter := wOut

	log = log.WithName("Pairing Dicom")
	log = log.WithValues("tags", tags)

	pairingReader := &PairingReader{
		in:                dicomReader,
		dicomWriter:       dicomWriter,
		out:               rOut,
		Tags:              tags,
		currentline:       0,
		ct:                ct,
		log:               log,
		pairingScriptPath: pairingScriptPath,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {
	if r.pairingScriptPath == "skipPairing" {
		return nil
	}

	r.log.Info("Start processing file pairing")

	file, err := ioutil.TempFile("", "pairingfile")
	if err != nil {
		return fmt.Errorf("create temp pairing file: %w", err)
	}

	r.log.Info("create tmp file for pairing")

	defer func() {
		if err := file.Close(); err != nil {
			r.log.Error(err, "close tmp file")
		}

		if err := os.Remove(file.Name()); err != nil {
			r.log.Error(err, "delete tmp file")
		}
	}()

	if err := file.Chmod(0o644); err != nil {
		return fmt.Errorf("chmod file: %w", err)
	}

	// 1. Download the file locally on the ACI temporary storage (required by the used library)
	content, _ := io.ReadAll(r.in)
	_, err = file.Write(content)
	if err != nil {
		return fmt.Errorf("copying locally the file: %w", err)
	}

	content = nil

	// 2. Create a temporary file containing the CT (it is small for Dicom files)
	ctFile, err := ioutil.TempFile("", "ctFile")
	if err != nil {
		return fmt.Errorf("create temp file: %w", err)
	}

	defer func() {
		if err := ctFile.Close(); err != nil {
			r.log.Error(err, "close tmp file")
		}

		if err := os.Remove(ctFile.Name()); err != nil {
			r.log.Error(err, "delete tmp ct file")
		}
	}()

	if err := ctFile.Chmod(0o644); err != nil {
		return fmt.Errorf("chmod file: %w", err)
	}

	for key, element := range r.ct {
		line := fmt.Sprintf("%s::%s\n", key, element)
		_, err = ctFile.WriteString(line)
		if err != nil {
			r.log.Error(err, "writing ct")
		}
	}

	// 3. Call a ruby script to do the pairing
	tagsList := ""
	for _, path := range r.Tags {
		if tagsList != "" {
			tagsList = tagsList + "::" + path
		} else {
			tagsList = path
		}
	}

	//"../pkg/dicomMask/scripts/pairingDicom.rb",
	//"/dicomMaskScripts/pairingDicom.rb",
	fileName := file.Name()
	ctFileName := ctFile.Name()
	cmd := exec.Command( //nolint:gosec
		"ruby",
		r.pairingScriptPath,
		fileName,
		ctFileName,
		tagsList)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		r.log.Info(fmt.Sprint(err) + ": " + stderr.String())
		log.Fatal(err)
		return nil
	}

	// Get the output of the ruby script
	dicomPair := out.String()

	// Parse the return value to check if it contains an error
	if strings.Contains(dicomPair, "[ERROR]") {
		return fmt.Errorf("pairing file: %s", dicomPair)
	}

	contentOutput, _ := os.ReadFile(file.Name())
	if _, err := r.dicomWriter.Write(contentOutput); err != nil {
		log.Fatal(err)
	}
	return nil
}
