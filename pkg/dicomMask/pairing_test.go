package dicomMask

import (
	"fmt"
	"io"
	"os"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(tags []string, ct masquage.CorrespondenceTable) pairingReaderTest {
	file, err := os.Open("./files/mr_small_maskprdc.dcm")

	pairingScriptPath := "../../pkg/dicomMask/scripts/pairingDicom.rb"

	if err != nil {
		_ = fmt.Errorf("Couldn't open file for the test: %w", err)
	}

	rOut, wOut := io.Pipe()

	return pairingReaderTest{
		Reader: &PairingReader{
			log:               logr.Discard(),
			in:                file,
			Tags:              tags,
			dicomWriter:       wOut,
			ct:                ct,
			out:               rOut,
			pairingScriptPath: pairingScriptPath,
		},
		Writer: wOut,
	}
}

func TestNewPairingReader(t *testing.T) {
	Tags := []string{"0100,0010"}

	type args struct {
		log               logr.Logger
		in                io.Reader
		tags              []string
		ct                masquage.CorrespondenceTable
		ct_name           string
		pairingScriptPath string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "DICOM pairing create pairingReader",
			args: args{
				log:               logr.Discard(),
				in:                strings.NewReader(""),
				tags:              Tags,
				ct:                masquage.CorrespondenceTable{},
				ct_name:           "ct",
				pairingScriptPath: "skipPairing",
			},
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewPairingReader(tt.args.log, tt.args.in, tt.args.tags, tt.args.ct, tt.args.pairingScriptPath)

			switch v := got.(type) {
			case *PairingReader, io.Reader:

			default:
				t.Errorf("NewPairingReader() = %v", v)
			}
		})
	}
}

func TestPairingReader_process(t *testing.T) {
	fileOut := "./files/mr_small_pairing.dcm"

	tests := []struct {
		name    string
		fields  pairingReaderTest
		wantErr bool
		want    string
		ctIn    masquage.CorrespondenceTable
	}{
		{
			name: "DICOM pairing Reader simple",
			fields: newPairingReader([]string{"0010,0020"}, masquage.CorrespondenceTable{
				"pdlr:ef844a8d162f2d1e1dc5e1753fd2923524863926826d15d66f847eef4b0eb9dc": "FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844",
			}),
			wantErr: false,
			want:    fileOut,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			r := tt.fields

			go func() {

				errP := r.Reader.process()

				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("PairingReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}

			}()

			got, err := io.ReadAll(r.Reader)
			if err != nil {
				t.Errorf("Couldn't read the output: %w", err) //nolint
			}

			wantRead, err := os.Open(tt.want)

			if err != nil {
				t.Errorf("Couldn't read the wanted result file: %w", err) //nolint
			}
			wantBytes, _ := io.ReadAll(wantRead)

			if !testEq(got, wantBytes) {
				t.Errorf("Dicom output does not match expected file (%s)\n", tt.want)
			}
		})
	}
}
