package dicomMask

import (
	"fmt"
	"io"
	"os"
	"rdd-pipeline/pkg/masquage"
	"reflect"
	"strings"
	"testing"

	"github.com/go-logr/logr"
)

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func newHashReader(tags []string) hashReaderTest {
	file, err := os.Open("./files/mr_small.dcm")
	maskerScriptPath := "./scripts/maskDicomFile.rb"

	if err != nil {
		_ = fmt.Errorf("Couldn't open file for the test: %w", err)
	}

	rOut, wOut := io.Pipe()
	return hashReaderTest{
		Reader: &HashReader{
			log:              logr.Discard(),
			in:               file,
			Key:              "48656c6c6f",
			Prefix:           "FHDH",
			Tags:             tags,
			dicomWriter:      wOut,
			ct:               masquage.CorrespondenceTable{},
			out:              rOut,
			maskerScriptPath: maskerScriptPath,
		},
		Writer: wOut,
	}

}

func TestNewHashReader(t *testing.T) {
	Tags := []string{"0010,0020"}

	type args struct {
		log              logr.Logger
		in               io.Reader
		key              string
		prefix           string
		tags             []string
		ct               masquage.CorrespondenceTable
		maskerScriptPath string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "create",
			args: args{
				log:              logr.Discard(),
				in:               strings.NewReader(""),
				key:              "skipItKey101",
				prefix:           "FHDH",
				tags:             Tags,
				ct:               masquage.CorrespondenceTable{},
				maskerScriptPath: "./scripts/maskDicomFile.rb",
			},
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewHashReader(tt.args.log, tt.args.in, tt.args.key, tt.args.prefix, tt.args.tags, tt.args.ct, tt.args.maskerScriptPath)

			switch v := got.(type) {
			case *HashReader, io.Reader:

			default:
				t.Errorf("NewHashReader() = %v", v)
			}
		})
	}
}

func TestHashReader_process(t *testing.T) {
	fileOut := "./files/mr_small_out_1.dcm"
	fileOut2 := "./files/file_out_2.dcm"

	tests := []struct {
		name    string
		fields  hashReaderTest
		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name:    "Simple with existing pattern (0010,0020)",
			fields:  newHashReader([]string{"0010,0020"}),
			wantErr: false,
			want:    fileOut,
			wantCt: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
			},
		}, {
			name:    "Simple with nothing to mask (unknown)",
			fields:  newHashReader([]string{"unknown"}),
			wantErr: false,
			want:    fileOut2,
			wantCt:  masquage.CorrespondenceTable{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			r := tt.fields

			go func() {

				errP := r.Reader.process()

				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}

			}()

			got, err := io.ReadAll(r.Reader)
			if err != nil {
				t.Errorf("Couldn't read the output: %w", err) //nolint
			}

			wantRead, err := os.Open(tt.want)

			if err != nil {
				t.Errorf("Couldn't read the wanted result file: %w", err) //nolint
			}
			wantBytes, _ := io.ReadAll(wantRead)

			if !testEq(got, wantBytes) {
				t.Errorf("Dicom output does not match expected file (%s)\n", tt.want)
			}

			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("CT: process() = \n%s\n -- want CT \n%v\n", r.Reader.ct, tt.wantCt)
			}

		})
	}
}

func testEq(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
