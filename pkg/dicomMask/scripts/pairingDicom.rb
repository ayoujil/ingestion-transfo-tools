require 'dicom'
require 'openssl'

include DICOM
DICOM.logger.level = Logger::ERROR

# Read file:
dcm = DObject.read("#{ARGV[0]}")

# Mask the tag value with the content of the ct file passed in parameter
tags = ARGV[2].split("::")

# File containing the CT
file="#{ARGV[1]}"

for i in 0 ... tags.length
    begin
    # Get the current value of the tag
        currentVal = dcm["#{tags[i]}"].value
        # Get the corresponding value in the ct
        File.readlines(file).each do |line|
            if line.include? currentVal
                val = line.split("::")
                # Modify it in the loaded dicom file
                dcm["#{tags[i]}"].value = val[1]
            end
        end
    rescue => e
        #puts "[ERROR] Masking Dicom file with tag: #{tags[i]} (check that this tag really exists), error: " + e.message
        next
    end

end

# Write the new values to the file
dcm.write("#{ARGV[0]}")