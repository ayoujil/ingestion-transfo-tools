require 'dicom'
require 'openssl'

include DICOM
DICOM.logger.level = Logger::ERROR

# Read file:
dcm = DObject.read("#{ARGV[0]}")

prefix = "#{ARGV[3]}"
tags = ARGV[2].split("::")
outStr = ""
key = [ARGV[1]].pack('H*')
# Modify the element:
for i in 0 ... tags.length
    begin
        oldValue = dcm["#{tags[i]}"].value
        dcm["#{tags[i]}"].value = prefix + ":" + OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), key, dcm["#{tags[i]}"].value)
        newValue = dcm["#{tags[i]}"].value
        if outStr != ""
            outStr = outStr + ",,#{oldValue}::#{newValue}"
        else
            outStr = "#{oldValue}::#{newValue}"
        end
    rescue => e
        #puts "[ERROR] Masking Dicom file with tag: #{tags[i]} (check that this tag really exists), error: " + e.message
        next
    end
end

# Write to file:
dcm.write("#{ARGV[0]}")

# Print CT
puts outStr