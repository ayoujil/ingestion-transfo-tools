package dicomMask

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type HashReader struct {
	in               io.Reader
	Prefix           string
	Tags             []string
	Key              string
	dicomWriter      *io.PipeWriter
	out              *io.PipeReader
	ct               masquage.CorrespondenceTable
	log              logr.Logger
	maskerScriptPath string
}

func NewHashReader(log logr.Logger, in io.Reader, key string, prefix string, tags []string, ct masquage.CorrespondenceTable, maskerScriptPath string) io.Reader {

	dicomReader := in
	rOut, wOut := io.Pipe()

	dicomWriter := wOut

	log = log.WithName("Hash Dicom")
	log = log.WithValues("prefix", prefix)
	log = log.WithValues("tags", tags)

	hashReader := &HashReader{
		in:               dicomReader,
		dicomWriter:      dicomWriter,
		out:              rOut,
		Tags:             tags,
		Prefix:           prefix,
		Key:              key,
		ct:               ct,
		log:              log,
		maskerScriptPath: maskerScriptPath,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return hashReader
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *HashReader) process() error {
	if r.Key == "skipItKey101" {
		return nil
	}

	file, err := ioutil.TempFile("", "maskfile")
	if err != nil {
		return fmt.Errorf("create temp file: %w", err)
	}

	r.log.Info("create tmp file for mask")

	defer func() {
		if err := file.Close(); err != nil {
			r.log.Error(err, "close tmp file %w")
		}

		if err := os.Remove(file.Name()); err != nil {
			r.log.Error(err, "delete tmp file: %w")
		}
	}()

	if err := file.Chmod(0o644); err != nil {
		return fmt.Errorf("chmod file: %w", err)
	}

	content, _ := io.ReadAll(r.in)
	_, err = file.Write(content)
	if err != nil {
		return fmt.Errorf("copying locally the file: %w", err)
	}
	content = nil

	// Call masking tool in ruby
	tagsList := ""
	for _, path := range r.Tags {
		if tagsList != "" {
			tagsList = tagsList + "::" + path
		} else {
			tagsList = path
		}
	}
	fileName := file.Name()
	cmd := exec.Command( //nolint:gosec
		"ruby",
		r.maskerScriptPath,
		fileName,
		r.Key,
		tagsList,
		r.Prefix)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		r.log.Info(fmt.Sprint(err) + ": " + stderr.String())
		log.Fatal(err)
		return nil
	}
	// Get the output of the ruby script
	dicomMask := out.String()
	dicomMask = strings.TrimSuffix(dicomMask, "\n")

	// Parse the return value to check if it contains an error
	if strings.Contains(dicomMask, "ERROR") {
		return fmt.Errorf("masking file: %s", dicomMask)
	}

	// The return needs to be parsed to get the old value and the newly masked value. Format is : <old1::masked1,,old2::masked2>
	// The used separator is "::" to avoid errors in the split in case we have a string already containing ":"
	if dicomMask != "" {
		split := strings.Split(dicomMask, ",,")
		for _, ctLine := range split {
			splitLine := strings.Split(ctLine, "::")
			oldVal := splitLine[1]
			// The used library appends a newline, it needs to be removed
			oldVal = strings.TrimSuffix(oldVal, "\n")
			maskedVal := splitLine[0]
			maskedVal = strings.TrimSuffix(maskedVal, "\n")
			r.ct[oldVal] = maskedVal
		}
	} else {
		r.log.Info("[WARNING] Nothing was masked")
	}

	contentOutput, _ := os.ReadFile(file.Name())
	if _, err := r.dicomWriter.Write(contentOutput); err != nil {
		log.Fatal(err)
	}

	return nil
}
