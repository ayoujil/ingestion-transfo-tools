package catalog

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"

	packageblob "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
)

type Catalog struct {
	Reader  *io.PipeReader
	wr      *io.PipeWriter
	catalog *csv.Writer
}

func NewCatalog() *Catalog {
	c := new(Catalog)

	re, wr := io.Pipe()
	catalog := csv.NewWriter(wr)

	c.Reader = re
	c.wr = wr
	c.catalog = catalog

	return c
}

func (c *Catalog) WriteAll(lines [][]string) error {
	defer c.wr.Close()
	return c.catalog.WriteAll(lines)
}

func (c *Catalog) CreateStreamCatalog(ctx context.Context, containerURL *azblob.ContainerURL) error {
	options := azblob.ListBlobsSegmentOptions{
		MaxResults: 0, // No max result
		Details:    azblob.BlobListingDetails{Metadata: true, Versions: false},
	}

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("CreateCatalog")
	log = log.WithValues("container", containerURL.String())

	defer log.V(2).Info("Stop listing container")
	defer c.wr.Close()
	defer c.catalog.Flush()

	log.V(2).Info("Start listing container")

	line := []string{
		"BlobName",
		"BlobType",
		"OriginalPath",
		"PackageVersion",
		"SizeInBytes",
		"CreationDate",
	}

	if err := c.catalog.Write(line); err != nil {
		return fmt.Errorf("write listing header: %w", err)
	}

	marker := azblob.Marker{}
	for marker.NotDone() {
		segment, err := containerURL.ListBlobsFlatSegment(ctx, marker, options)
		if err != nil {
			return fmt.Errorf("list segment: %w", err)
		}

		log.V(2).Info("Found blobs", "count", len(segment.Segment.BlobItems))

		for _, blob := range segment.Segment.BlobItems {
			line := []string{
				blob.Name,
				blob.Metadata[v1alpha1.MetadataBlobType],
				blob.Metadata[v1alpha1.MetadataOriginalPath],
				blob.Metadata[v1alpha1.MetadataVersion],
				fmt.Sprintf("%v", *blob.Properties.ContentLength),
				blob.Properties.CreationTime.String(),
			}

			log.V(2).Info("Add line to listing", "line", line)

			if err := c.catalog.Write(line); err != nil {
				return fmt.Errorf("write listing line: %w", err)
			}
		}

		marker = segment.NextMarker
	}

	return nil
}

func (c *Catalog) UploadCatalog(ctx context.Context, name string, containerURL *azblob.ContainerURL) error {
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("UploadCatalog")
	log = log.WithValues("container", containerURL.String())
	log = log.WithValues("catalogName", name) //nolint

	cpk := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	outputBlob := packageblob.NewBlobFromContainerClient(name, containerURL, &cpk, nil)
	if err := outputBlob.UploadBlobStream(ctx, c.Reader); err != nil {
		return fmt.Errorf("upload listing data to '%s': %w", name, err)
	}
	return nil
}
