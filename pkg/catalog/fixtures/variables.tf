variable "prefix" {
  type        = string
  description = "(Required) The Prefix of the storage account to be created."
}

variable "num_files" {
  type = number
}

variable "metadata" {
  type    = map(string)
  default = {}
}
