resource "azurerm_storage_account" "input" {
  name                      = "${var.prefix}sa"
  resource_group_name       = azurerm_resource_group.test.name
  location                  = azurerm_resource_group.test.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  account_kind              = "StorageV2"
  enable_https_traffic_only = true
  allow_blob_public_access  = false

  identity {
    type = "SystemAssigned"
  }
}

output "storage_connection_string" {
  value     = azurerm_storage_account.input.primary_connection_string
  sensitive = true
}

output "storage_blob_endpoint" {
  value = azurerm_storage_account.input.primary_blob_endpoint
}

resource "azurerm_storage_container" "data" {
  name                  = "data"
  storage_account_name  = azurerm_storage_account.input.name
  container_access_type = "private"
}

output "storage_container_name" {
  value = azurerm_storage_container.data.name
}

resource "azurerm_storage_container" "catalog" {
  name                  = "catalog"
  storage_account_name  = azurerm_storage_account.input.name
  container_access_type = "private"
}

output "catalog_container_name" {
  value = azurerm_storage_container.catalog.name
}

resource "azurerm_storage_blob" "test_file" {
  count = var.num_files

  name                   = "test-file-${count.index}.csv"
  storage_account_name   = azurerm_storage_account.input.name
  storage_container_name = azurerm_storage_container.data.name
  type                   = "Block"
  source                 = "test-file.csv"
  metadata               = var.metadata
}
