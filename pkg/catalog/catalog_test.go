// +build integration

package catalog_test

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/require"
	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/catalog"
	"rdd-pipeline/pkg/schema/v1alpha1"
)

func TestCatalog_CreateStreamCatalog(t *testing.T) {
	uniqueId := random.UniqueId()
	tfPrefix := fmt.Sprintf("testcatalog%s", strings.ToLower(uniqueId))
	numFiles := 5
	fileTypes := "CSV"
	fileVersion := "v2"
	filePath := "test-file.csv"

	// Configure Terraform setting up a path to Terraform code.
	terraformOptions := &terraform.Options{
		// The path to our Terraform code
		TerraformDir: "./fixtures/",
		Vars: map[string]interface{}{
			"prefix":    tfPrefix,
			"num_files": numFiles,
			"metadata": map[string]string{
				v1alpha1.MetadataBlobType:     fileTypes,
				v1alpha1.MetadataVersion:      fileVersion,
				v1alpha1.MetadataOriginalPath: filePath,
			},
		},
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	storageAccountURL := terraform.Output(t, terraformOptions, "storage_blob_endpoint")
	storageContainerName := terraform.Output(t, terraformOptions, "storage_container_name")

	log := logr.Discard()
	storageContainerURL, _, err := rddblob.NewContainerClientFromEnvironment(log, storageAccountURL, storageContainerName)
	if err != nil {
		t.Errorf("NewContainerClientFromEnvironment():Storage error = %v", err)
	}

	type args struct {
		catalogName         string
		ctx                 context.Context
		storageContainerURL *azblob.ContainerURL
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "checkNumLines",
			args: args{
				catalogName:         "catalog.csv",
				ctx:                 context.TODO(),
				storageContainerURL: storageContainerURL,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			c := catalog.NewCatalog()
			go func() {
				if err := c.CreateStreamCatalog(tt.args.ctx, tt.args.storageContainerURL); (err != nil) != tt.wantErr {
					t.Errorf("Catalog.CreateCatalog() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()

			csv := csv.NewReader(c.Reader)
			rec, _ := csv.ReadAll()
			// strip header
			rec = rec[1:]

			t.Logf("Catalog.ReadAll() content = %v", rec)

			// check num lines + header
			require.Equal(t, len(rec), numFiles, "num lines.")

			for _, l := range rec {
				require.Equal(t, len(l), 6, "6 cols.")
				require.Equal(t, l[1], fileTypes, "type is not the same.")
				require.Equal(t, l[2], filePath, "original path is not the same.")
				require.Equal(t, l[3], fileVersion, "version is not the same.")
			}
		})
	}
}

func TestCatalog_UploadCatalog(t *testing.T) {
	uniqueId := random.UniqueId()
	tfPrefix := fmt.Sprintf("testcatalog%s", strings.ToLower(uniqueId))

	// Configure Terraform setting up a path to Terraform code.
	terraformOptions := &terraform.Options{
		// The path to our Terraform code
		TerraformDir: "./fixtures/",
		Vars: map[string]interface{}{
			"prefix":    tfPrefix,
			"num_files": 0,
		},
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	storageAccountURL := terraform.Output(t, terraformOptions, "storage_blob_endpoint")
	catalogContainerName := terraform.Output(t, terraformOptions, "catalog_container_name")

	log := logr.Discard()
	containerURL, _, err := rddblob.NewContainerClientFromEnvironment(log, storageAccountURL, catalogContainerName)
	if err != nil {
		t.Errorf("NewContainerClientFromEnvironment():Catalog error = %v", err)
	}

	type args struct {
		ctx          context.Context
		name         string
		containerURL *azblob.ContainerURL
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "upload",
			args: args{
				ctx:          context.TODO(),
				name:         "catalog.csv",
				containerURL: containerURL,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			c := catalog.NewCatalog()
			lines := [][]string{
				{"header", "col1", "col2"},
				{"line 1", "col1", "col2"},
				{"line 2", "col1", "col2"},
			}
			go func(c *catalog.Catalog) {
				if err := c.WriteAll(lines); err != nil {
					t.Errorf("Catalog.UploadCatalog() error = %v, wantErr %v", err, tt.wantErr)
				}
			}(c)

			if err := c.UploadCatalog(tt.args.ctx, tt.args.name, tt.args.containerURL); (err != nil) != tt.wantErr {
				t.Errorf("Catalog.UploadCatalog() error = %v, wantErr %v", err, tt.wantErr)
			}

			blobURL := tt.args.containerURL.NewBlockBlobURL(tt.args.name)
			content, _, err := rddblob.DownloadBlob(tt.args.ctx, &blobURL, azblob.ClientProvidedKeyOptions{})
			if (err != nil) != tt.wantErr {
				t.Errorf("Catalog.UploadCatalog() error = %v, wantErr %v", err, tt.wantErr)
			}

			catalog := csv.NewReader(bytes.NewReader(content))
			catalogLines, _ := catalog.ReadAll()
			if !reflect.DeepEqual(lines, catalogLines) {
				t.Errorf("Catalog.UploadCatalog() = %v, want %v", lines, catalogLines)
			}
		})
	}
}
