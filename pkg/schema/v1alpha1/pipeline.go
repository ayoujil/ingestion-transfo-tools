package v1alpha1

import (
	"fmt"
	"regexp"

	"rdd-pipeline/pkg/schema/meta"
	"rdd-pipeline/pkg/util"
)

// !!! WARNING !!! This config version is already released, please DO NOT MODIFY the structs in this file.
const (
	Version      string = "rdd-pipeline/v1alpha1"
	PipelineKind string = "Pipeline"
	Gzip         string = "gzip"
)

// NewSkaffoldConfig creates a SkaffoldConfig.
func NewPipelineConfig() util.VersionedConfig {
	return new(PipelineConfig)
}

type PipelineConfig struct {
	meta.Config `yaml:",inline"`
	// Pipeline defines the Build/Test/Deploy phases.
	Pipeline `yaml:",inline"`
}

func (config *PipelineConfig) GetVersion() string {
	return config.APIVersion
}

func (config *PipelineConfig) GetKind() string {
	return config.Kind
}

// Pipeline describes a Skaffold pipeline.
type Pipeline struct {
	// Decrypt describes how files are decrypted.
	Decrypt DecryptConfigs `yaml:"decrypt,omitempty"`

	// Hash describes how files are hashed.
	Hash HashConfigs `yaml:"mask,omitempty"`

	// Import describes the SFTP config from where to download files.
	Import Import `yaml:"import,omitempty"`

	// DefaultGPG is used when no GPG specific conf is detailed for a blob type
	DefaultGPG GPGConfig `yaml:"defaultgpg,omitempty"`
}

type (
	HashConfigs    []*HashConfig
	DecryptConfigs []*DecryptConfig
)

func (d HashConfigs) GetConfig(blob string) (*HashConfig, error) {
	for _, c := range d {
		matched, err := match(c.Blob, blob)
		if err != nil {
			return nil, fmt.Errorf("failed to use regex %s: %w", c.Blob, err)
		}

		if matched {
			return c, nil
		}
	}

	return nil, nil
}

func (d DecryptConfigs) Match(blob string) bool {
	c, err := d.GetConfig(blob)
	return (c != nil) && (err == nil)
}

func (d DecryptConfigs) GetConfig(blob string) (*DecryptConfig, error) {
	for _, c := range d {
		matched, err := match(c.Blob, blob)
		if err != nil {
			return nil, fmt.Errorf("failed to use regex %s: %w", c.Blob, err)
		}

		if matched {
			return c, nil
		}
	}

	return nil, nil
}

func (d HashConfigs) Match(blob string) bool {
	c, err := d.GetConfig(blob)
	return (c != nil) && (err == nil)
}

func match(c, blob string) (bool, error) {
	return regexp.MatchString(c, blob)
}

type DecryptConfig struct {
	BlobConfig  `yaml:",inline"`
	Compression CompressionConfig `yaml:"compression,omitempty"`
	GPG         GPGConfig         `yaml:"gpg,omitempty"`
}

type CompressionConfig struct {
	Format string `yaml:"format" yamltags:"required"`
}

type GPGConfig struct {
	SignatureKey SecretReference `yaml:"signatureKey" yamltags:"required"`
	DecryptKey   SecretReference `yaml:"decryptKey" yamltags:"required"`
	Content      GPGContent      `yaml:"content,omitempty"`
}

type GPGContent struct {
	Armored bool `yaml:"armored,omitempty"`
}

type HashConfig struct {
	BlobConfig `yaml:",inline"`
	Key        SecretReference `yaml:"key" yamltags:"required"`
	Default    HashFileConfig  `yaml:"default" yamltags:"required"`
}

type SecretReference struct {
	Secret           string `yaml:"secret,omitempty"`
	PassphraseSecret string `yaml:"passphraseSecret,omitempty"`
	Password         string `yaml:"password,omitempty"`
}

type Import struct {
	Sftp SftpConfig `yaml:"sftp,omitempty"`
}

type SftpConfig struct {
	Server    string   `yaml:"server"`
	User      string   `yaml:"user"`
	Delete    *bool    `yaml:"delete,omitempty"`
	Files     []string `yaml:"files"`
	KeepAlive bool     `yaml:"keepalive,omitempty"`
}
