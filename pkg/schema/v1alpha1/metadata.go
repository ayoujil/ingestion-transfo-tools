package v1alpha1

// Metadata keys added to the blos
const (
	MetadataOriginalPath = "originalpath"
	MetadataMatchPattern = "match"
	MetadataBlobType     = "type"
	MetadataVersion      = "version"
	MetadataWrapedKey    = "wrapedkey"
	MetadataKeyvaultKey  = "keyvaultkey"
	MetadataHashConfig   = "hashconfig"
)
