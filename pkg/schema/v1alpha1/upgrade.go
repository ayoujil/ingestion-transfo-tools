package v1alpha1

import (
	"fmt"

	"rdd-pipeline/pkg/util"
)

// Upgrade upgrades a configuration to the next version.
func (c *PipelineConfig) Upgrade() (util.VersionedConfig, error) {
	return nil, fmt.Errorf("there's no version to upgrade from \"%s\"", c.GetVersion())
}
