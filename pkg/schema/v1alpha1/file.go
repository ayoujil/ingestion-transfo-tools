package v1alpha1

import (
	"rdd-pipeline/pkg/schema/meta"
)

type FileConfig struct {
	meta.Config `yaml:",inline"`
	// Pipeline defines the Build/Test/Deploy phases.
	HashFileConfig `yaml:",inline"`
	Origin         *OriginConfig `yaml:"origin,omitempty"`
}

type HashFileConfig struct {
	CSV   *CSVConfig   `json:"csv,omitempty" yaml:"csv,omitempty"`
	PATH  *PathConfig  `json:"path,omitempty" yaml:"path,omitempty"`
	Json  *JsonConfig  `json:"json,omitempty" yaml:"json,omitempty"`
	Dicom *DicomConfig `json:"dicom,omitempty" yaml:"dicom,omitempty"`
}

type DicomConfig struct {
	Tags []string `json:"tags,omitempty" yaml:"tags,omitempty"`
}

type JsonConfig struct {
	Tags []string `json:"tags,omitempty" yaml:"tags,omitempty"`
}

type CSVConfig struct {
	Columns     []int    `json:"columns" yaml:"columns" yamltags:"required"`
	ColumnNames []string `json:"columnNames,omitempty" yaml:"columnNames,omitempty"`
	Separator   string   `json:"separator" yaml:"separator" yamltags:"required"`
	SkipLines   []int    `json:"skipLines,omitempty" yaml:"skipLines,omitempty"`
	LocalCt     bool     `json:"localCt,omitempty" yaml:"localCt,omitempty"`
}

type PathConfig struct {
	DepthLevel []int  `json:"depthLevel" yaml:"depthLevel" yamltags:"required"`
	Separator  string `json:"separator" yaml:"separator" yamltags:"required"`
}

type OriginConfig struct {
	File    string `yaml:"file,omitempty"`
	Archive string `yaml:"archive,omitempty"`
}
