package v1alpha1

type BlobType string

const (
	PassThrough BlobType = "PassThrough"
	CSV         BlobType = "CSV"
	Image       BlobType = "Image"
	Json        BlobType = "Json"
	Dicom       BlobType = "Dicom"
)

type BlobConfig struct {
	Blob     string   `yaml:"blob" yamltags:"required"`
	BlobType BlobType `yaml:"type" yamltags:"required"`
}
