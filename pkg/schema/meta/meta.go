package meta

// MetaConfig holds the fields parsed from the rdd-pipeline configuration file (config.yaml).
type Config struct {
	// APIVersion is the version of the configuration.
	APIVersion string `yaml:"apiVersion,omitempty" yamltags:"required"`

	// Kind is always `Config`. Defaults to `Config`.
	Kind string `yaml:"kind,omitempty" yamltags:"required"`

	// Metadata holds additional information about the config.
	Metadata map[string]string `yaml:"metadata,omitempty"`
}
