package worker

import (
	"context"
	"fmt"

	rddblob "rdd-pipeline/pkg/blob"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"gopkg.in/yaml.v2"
)

type ConfigContainer struct {
	StorageAccountURL string
	ContainerName     string
	BlobName          string
}

type BlobConfig interface {
	Match(string) bool
}

func (c *ConfigContainer) Download(ctx context.Context) (*v1alpha1.PipelineConfig, error) {
	log := logr.FromContextOrDiscard(ctx)

	configContainer, _, err := rddblob.NewContainerClientFromEnvironment(log, c.StorageAccountURL, c.ContainerName)
	if err != nil {
		return nil, fmt.Errorf("create Config container client: %w", err)
	}

	blobURL := configContainer.NewBlockBlobURL(c.BlobName)
	content, _, err := rddblob.DownloadBlob(ctx, &blobURL, azblob.ClientProvidedKeyOptions{EncryptionAlgorithm: azblob.EncryptionAlgorithmNone})
	if err != nil {
		return nil, fmt.Errorf("download config blob '%s': %w", c.BlobName, err)
	}

	pipelineConfig := new(v1alpha1.PipelineConfig)
	if err := yaml.Unmarshal(content, pipelineConfig); err != nil {
		return nil, fmt.Errorf("unmarshall config from blob '%s': %w", c.BlobName, err)
	}

	return pipelineConfig, nil
}
