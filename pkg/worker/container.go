package worker

import (
	"context"
	"fmt"
	"os"
	"runtime"
	"sync/atomic"
	"time"

	rddblob "rdd-pipeline/pkg/blob"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/guilhem/gorkers"
)

var gMaxWorkers = int64(runtime.NumCPU())

// time interval between two blob checks in seconds
var timeTicker = 30

// autoShutdownDelay is in seconds, 30 min by default if the flag is not set
var autoShutdownDelay = int64(1800)

func SetMaxWorkers(n int) {
	atomic.StoreInt64(&gMaxWorkers, int64(n))
}

func SetAutoShutdownDelay(n int) {
	atomic.StoreInt64(&autoShutdownDelay, int64(n))
}

type Job struct {
	BlobUrl *azblob.BlockBlobURL
}

func NewJob(blobUrl azblob.BlockBlobURL) Job {
	return Job{BlobUrl: &blobUrl}
}

type ProcessContainer struct {
	storageAccountURL string
	containerName     string
	blobConfig        BlobConfig
	requiredMetadata  map[string]string
	metadataToSkip    map[string]string
	currentTasks      int
	idleTime          int
}

func NewProcessContainer(storageAccountURL, containerName string, requiredMetadata, metadataToSkip map[string]string) *ProcessContainer {
	return NewProcessContainerWithConfig(storageAccountURL, containerName, nil, requiredMetadata, metadataToSkip, 0, 0)
}

func NewProcessContainerWithConfig(
	storageAccountURL,
	containerName string,
	blobConfig BlobConfig,
	requiredMetadata,
	metadataToSkip map[string]string,
	currentTasks int,
	idleTime int,
) *ProcessContainer {
	return &ProcessContainer{
		storageAccountURL: storageAccountURL,
		containerName:     containerName,
		blobConfig:        blobConfig,
		requiredMetadata:  requiredMetadata,
		metadataToSkip:    metadataToSkip,
		currentTasks:      0,
		idleTime:          0,
	}
}

func (pc *ProcessContainer) Run(ctx context.Context, task gorkers.WorkFunc, runOnce bool) error {
	pc.idleTime = 0
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithValues("maxWorkers", gMaxWorkers)
	log = log.WithValues("autoShutdown (secs)", autoShutdownDelay)
	ctx, cancel := context.WithCancel(ctx)

	// start watching in container for blob
	watchGroup := gorkers.NewRunner(ctx, pc.Work, 1, 1)
	watchGroup.AfterFunc(func(_ context.Context, _ interface{}, err error) error {
		log.Info("watch finished")
		if err != nil {
			log.Error(err, "watch worker failed")
			cancel()
		}
		return err
	})

	// start executing on blob sent from watcher
	taskGroup := gorkers.NewRunner(ctx, task, gMaxWorkers, 1).InFrom(watchGroup)
	taskGroup.AfterFunc(func(_ context.Context, _ interface{}, err error) error {
		log.Info("task finished")
		pc.currentTasks--
		if err != nil {
			log.Error(err, "task worker failed")
		} else {
			pc.idleTime = 0
		}
		return nil
	})

	if err := watchGroup.Start(); err != nil {
		return fmt.Errorf("starting watch worker: %w", err)
	}
	if err := taskGroup.Start(); err != nil {
		return fmt.Errorf("starting task worker: %w", err)
	}

	defer func() {
		watchGroup.Wait().Stop()
		taskGroup.Wait().Stop()
		log.Info("defer finished")
	}()

	if err := watchGroup.Send(nil); err != nil {
		return fmt.Errorf("sending first tick to watcher: %w", err)
	}

	if !runOnce {
		ticker := time.NewTicker(time.Duration(timeTicker) * time.Second)
		defer ticker.Stop()
		for {
			if pc.currentTasks != 0 {
				log.Info("A file is still being processed")
			}
			// Autoshutdown check
			if pc.currentTasks == 0 && autoShutdownDelay != 0 {
				pc.idleTime += timeTicker
				if int64(pc.idleTime) == int64(autoShutdownDelay)+int64(timeTicker) {
					log.Info("auto shutdown timer reached, shutting down the ACI")
					os.Exit(0)
				}
			}
			select {
			case <-ctx.Done():
				return nil
			case <-ticker.C:
				if err := watchGroup.Send(nil); err != nil {
					return fmt.Errorf("sending tick to watcher : %w", err)
				}
			}
		}
	}

	return nil
}

func (pc *ProcessContainer) Work(ctx context.Context, _ interface{}, out chan<- interface{}) error {
	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("process container")
	log = log.WithValues("containerName", pc.containerName, "storageAccount", pc.storageAccountURL)

	containerURL, _, err := rddblob.NewContainerClientFromEnvironment(log, pc.storageAccountURL, pc.containerName)
	if err != nil {
		return fmt.Errorf("create Input container client: %w", err)
	}

	if err := pc.Process(ctx, out, containerURL); err != nil {
		return fmt.Errorf("process container list with config: %w", err)
	}

	return nil
}

func (pc *ProcessContainer) Process(ctx context.Context, out chan<- interface{}, containerURL *azblob.ContainerURL) error {
	options := azblob.ListBlobsSegmentOptions{
		MaxResults: 0, // No max result
		Details:    azblob.BlobListingDetails{Metadata: true},
	}

	log := logr.FromContextOrDiscard(ctx)
	log = log.WithName("Process Container")
	log = log.WithValues("container", containerURL)

	defer log.V(2).Info("Stop processing container")

	log.V(2).Info("Start processing container")
	marker := azblob.Marker{}
	for marker.NotDone() {
		segment, err := containerURL.ListBlobsFlatSegment(ctx, marker, options)
		if err != nil {
			return fmt.Errorf("list segment: %w", err)
		}

		log.V(2).Info("Found blobs", "count", len(segment.Segment.BlobItems))
		for _, blob := range segment.Segment.BlobItems {
			if blob.Properties.LeaseStatus == azblob.LeaseStatusLocked {
				log.V(1).Info("blob is locked and will be ignored", "blobName", blob.Name)
				continue
			}

			// Skip ignored blobs
			if pc.blobConfig != nil && !pc.blobConfig.Match(blob.Name) {
				log.V(1).Info("blob does not match config file pattern and will be ignored", "blobName", blob.Name)
				continue
			}

			skipBlob := false

			// Skip blobs matching metadata
			for key, element := range pc.metadataToSkip {
				if blob.Metadata[key] == element {
					log.V(1).Info("blob has matching metadata and will be ignored", "blobName", blob.Name, "metadata_key", key)
					skipBlob = true
					break
				}
			}

			if skipBlob {
				continue
			}

			// Skip blobs NOT matching metadata
			for key, element := range pc.requiredMetadata {
				value, ok := blob.Metadata[key]

				if !ok || value != element {
					log.V(1).Info("blob does not have the required metadata and will be ignored", "blobName", blob.Name, "metadata_key", key)
					skipBlob = true
					break
				}
			}

			if skipBlob {
				continue
			}
			pc.currentTasks++
			blobUrl := containerURL.NewBlockBlobURL(blob.Name)

			out <- NewJob(blobUrl)
		}

		marker = segment.NextMarker
	}

	return nil
}
