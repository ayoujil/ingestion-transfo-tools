package csv

import (
	"encoding/csv"
	"io"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(skipLines, columns []int, columnNames []string, ct masquage.CorrespondenceTable) pairingReaderTest {
	csvIn := `id;name;pseudo` + "\n" +
		`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
		`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n"
	csvReader := csv.NewReader(
		strings.NewReader(csvIn),
	)
	csvReader.Comma = rune(";"[0])

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(";"[0])

	// zl := zerolog.New(os.Stdout).With().Caller().Timestamp().Logger()
	// log := zerologr.New(&zl)

	return pairingReaderTest{
		Reader: &PairingReader{
			log:         logr.Discard(),
			in:          csvReader,
			SkipLines:   skipLines,
			ColumnNames: columnNames,
			Columns:     columns,
			csvWriter:   csvWriter,
			ct:          ct,
			out:         rOut,
		},
		Writer: wOut,
	}
}

func TestNewPairingReader(t *testing.T) {
	skipLines := []int{1}
	columns := []int{}
	columnNames := []string{"id"}

	type args struct {
		log         logr.Logger
		in          io.Reader
		separator   string
		skipLines   []int
		columns     []int
		columnNames []string
		ct          masquage.CorrespondenceTable
	}
	tests := []struct {
		name string
		args args
		want io.Reader
	}{
		{
			name: "create",
			args: args{
				log:         logr.Discard(),
				in:          strings.NewReader(""),
				separator:   ";",
				skipLines:   skipLines,
				columnNames: columnNames,
				columns:     columns,
				ct:          masquage.CorrespondenceTable{},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewPairingReader(tt.args.log, tt.args.in, tt.args.separator, tt.args.skipLines, tt.args.columns, tt.args.columnNames, tt.args.ct)

			switch v := got.(type) {
			case *PairingReader, io.Reader:

			default:
				t.Errorf("NewPairingReader() = %v", v)
			}
		})
	}
}

func TestPairingReader_process(t *testing.T) {
	csvOut := `id;name;pseudo` + "\n" +
		`1;health-data-hub;hdh` + "\n" +
		`2;france-television;ftv` + "\n"
	ctOut := masquage.CorrespondenceTable{
		"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
		"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
	}

	tests := []struct {
		name    string
		fields  pairingReaderTest
		wantErr bool
		want    string
	}{
		{
			name:    "with_col_names",
			fields:  newPairingReader([]int{1}, []int{}, []string{"id"}, ctOut),
			wantErr: false,
			want:    csvOut,
		},
		{
			name:    "with_col_rank",
			fields:  newPairingReader([]int{1}, []int{1}, []string{}, ctOut),
			wantErr: false,
			want:    csvOut,
		},
		{
			name:    "with_col_rank_too_small",
			fields:  newPairingReader([]int{}, []int{-1}, []string{}, ctOut),
			wantErr: true,
			want:    "",
		},
		{
			name:    "with_col_rank_out_of_bound",
			fields:  newPairingReader([]int{}, []int{99}, []string{}, ctOut),
			wantErr: true,
			want:    "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := tt.fields
			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("Hash() = %s, want %v", got, tt.want)
			}
		})
	}
}
