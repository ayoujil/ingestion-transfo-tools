package csv

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"sort"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type PairingReader struct {
	in          *csv.Reader
	SkipLines   []int
	ColumnNames []string
	Columns     []int
	currentline int
	csvWriter   *csv.Writer
	out         io.Reader
	ct          masquage.CorrespondenceTable
	log         logr.Logger
}

func NewPairingReader(log logr.Logger, in io.Reader, separator string, skipLines []int, columns []int, columnNames []string, ct masquage.CorrespondenceTable) io.Reader {
	csvReader := csv.NewReader(in)
	csvReader.Comma = rune(separator[0])

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(separator[0])

	sort.Ints(skipLines)

	log = log.WithName("Pairing CSV")
	log = log.WithValues("separator", separator)
	log = log.WithValues("skipLines", skipLines)
	log = log.WithValues("columns", columns)
	log = log.WithValues("columnNames", columnNames)

	pairingReader := &PairingReader{
		in:          csvReader,
		csvWriter:   csvWriter,
		out:         rOut,
		SkipLines:   skipLines,
		ColumnNames: columnNames,
		Columns:     columns,
		currentline: 0,
		ct:          ct,
		log:         log,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error pairing")
		}
	})()

	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {
	defer r.csvWriter.Flush()

	maskCols := r.Columns

	for {
		// 1. Iterate over columns from the input blob
		// 2. Find Correspondence Table Key (K) within each line
		// 3. Replace the key (K) in input CSV by the value (V) from Correspondence Table
		r.currentline++

		line, err := r.in.Read()
		if err != nil {
			// end of file
			if errors.Is(err, io.EOF) {
				return nil
			}
			return fmt.Errorf("can't read CSV: %w", err)
		}

		// convert colnames to colnums
		if r.currentline == 1 {
			if len(r.ColumnNames) > 0 {
				r.log.V(1).Info("masking columns", "names", r.ColumnNames)
				maskCols, err = convertColNameToColNum(line, r.ColumnNames)
				if err != nil {
					return fmt.Errorf("converting colname to colnum : %w", err)
				}
			}
			r.log.V(1).Info("masking columns", "positions", maskCols, "skiplines", r.SkipLines)
		}

		// Skip line
		if n := sort.SearchInts(r.SkipLines, r.currentline); !(n < len(r.SkipLines) && r.SkipLines[n] == r.currentline) {
			for _, col := range maskCols {
				i := col - 1

				if col > len(line) {
					return fmt.Errorf("column-position too big: %d > %d", col, len(line))
				} else if i < 0 {
					return fmt.Errorf("column-position too small: %d", col)
				}

				id := line[i]

				idCT, ok := r.ct[id]
				if !ok {
					return fmt.Errorf("CT key at line %d/%d could not be found", col, r.currentline)
				}

				line[i] = idCT
			}
		}

		if err := r.csvWriter.Write(line); err != nil {
			return fmt.Errorf("can't write CSV: %w", err)
		}
	}
}
