package csv

import (
	"encoding/csv"
	"io"
	"reflect"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func newHashReader(skipLines, columns []int, columnNames []string) hashReaderTest {
	csvReader := csv.NewReader(
		strings.NewReader(`id;name;pseudo` + "\n" +
			`1;health-data-hub;hdh` + "\n" +
			`2;france-television;ftv` + "\n"),
	)
	csvReader.Comma = rune(";"[0])

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(";"[0])

	createCT := true
	ctFileName := ""
	return hashReaderTest{
		Reader: &HashReader{
			log:         logr.Discard(),
			in:          csvReader,
			Key:         "0123456789abcdef",
			Prefix:      "HDH",
			SkipLines:   skipLines,
			ColumnNames: columnNames,
			Columns:     columns,
			csvWriter:   csvWriter,
			createCT:    createCT,
			ctFileName:  ctFileName,
			ct:          masquage.CorrespondenceTable{},
			out:         rOut,
		},
		Writer: wOut,
	}
}

func TestNewHashReader(t *testing.T) {
	skipLines := []int{1}
	columns := []int{}
	columnNames := []string{"id"}

	type args struct {
		log         logr.Logger
		in          io.Reader
		separator   string
		key         string
		prefix      string
		skipLines   []int
		columns     []int
		columnNames []string
		ct          masquage.CorrespondenceTable
		createCT    bool
		ctFileName  string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "create",
			args: args{
				log:         logr.Discard(),
				in:          strings.NewReader(""),
				separator:   ";",
				key:         "0123456789abcdef",
				prefix:      "HDH",
				skipLines:   skipLines,
				columnNames: columnNames,
				columns:     columns,
				ct:          masquage.CorrespondenceTable{},
				createCT:    true,
				ctFileName:  "",
			},
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewHashReader(tt.args.log, tt.args.in, tt.args.separator, tt.args.key, tt.args.prefix, tt.args.skipLines, tt.args.columns, tt.args.columnNames, tt.args.ct, tt.args.createCT, tt.args.ctFileName)

			switch v := got.(type) {
			case *HashReader, io.Reader:

			default:
				t.Errorf("NewHashReader() = %v", v)
			}
		})
	}
}

func TestHashReader_process(t *testing.T) {
	csvOut := `id;name;pseudo` + "\n" +
		`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
		`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n"
	ctOut := masquage.CorrespondenceTable{
		"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
		"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
	}

	// zl := zerolog.New(os.Stdout).With().Caller().Timestamp().Logger()
	// log := zerologr.New(&zl)

	tests := []struct {
		name    string
		fields  hashReaderTest
		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name:    "with_col_names",
			fields:  newHashReader([]int{1}, []int{}, []string{"id"}),
			wantErr: false,
			want:    csvOut,
			wantCt:  ctOut,
		},
		{
			name:    "with_col_names_out_of_bound",
			fields:  newHashReader([]int{}, []int{}, []string{"dummy"}),
			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name:    "with_col_rank",
			fields:  newHashReader([]int{1}, []int{1}, []string{}),
			wantErr: false,
			want:    csvOut,
			wantCt:  ctOut,
		},
		{
			name:    "with_col_rank_out_of_bound",
			fields:  newHashReader([]int{}, []int{99}, []string{}),
			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:    "with_col_rank_too_small",
			fields:  newHashReader([]int{}, []int{-1}, []string{}),
			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:    "with_dbl_col_names",
			fields:  newHashReader([]int{1}, []int{}, []string{"id", "name"}),
			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;HDH:1c097515e0d8b3b520e145fa60bdeba3b6387f4add2272469087d7ac30642e9a;hdh` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;HDH:92139665a786179214c6039c32e2bb489ba597d432575dccfdefc5de70aa3299;ftv` + "\n",
			wantCt: masquage.CorrespondenceTable{
				"HDH:1c097515e0d8b3b520e145fa60bdeba3b6387f4add2272469087d7ac30642e9a": "health-data-hub",
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:92139665a786179214c6039c32e2bb489ba597d432575dccfdefc5de70aa3299": "france-television",
			},
		},
		{
			name:    "with_no_skip",
			fields:  newHashReader([]int{0}, []int{1}, []string{}),
			wantErr: false,
			want: `HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065;name;pseudo` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n",
			wantCt: masquage.CorrespondenceTable{
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065": "id",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := tt.fields
			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("process() = %s, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("process() = %s, want %v", r.Reader.ct, tt.want)
			}
		})
	}
}

func Test_convertColNameToColNum(t *testing.T) {
	type args struct {
		cols  []string
		names []string
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{
			name: "test colnum",
			args: args{
				cols:  []string{"name", "id", "desc"},
				names: []string{"id"},
			},
			want:    []int{2},
			wantErr: false,
		},
		{
			name: "test not found",
			args: args{
				cols:  []string{"name", "id", "desc"},
				names: []string{"my_id"},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			got, err := convertColNameToColNum(tt.args.cols, tt.args.names)
			if (err != nil) != tt.wantErr {
				t.Errorf("convertColNameToColNumCSV() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convertColNameToColNumCSV() = %v, want %v", got, tt.want)
			}
		})
	}
}
