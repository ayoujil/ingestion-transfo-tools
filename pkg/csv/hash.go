package csv

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"sort"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type HashReader struct {
	in          *csv.Reader
	SkipLines   []int
	Prefix      string
	ColumnNames []string
	Columns     []int
	Key         string
	currentline int
	csvWriter   *csv.Writer
	out         *io.PipeReader
	createCT    bool
	ctFileName  string
	ct          masquage.CorrespondenceTable
	log         logr.Logger
}

func NewHashReader(log logr.Logger, in io.Reader, separator string, key string, prefix string, skipLines []int, columns []int, columnNames []string, ct masquage.CorrespondenceTable, createCT bool, ctFileName string) io.Reader {
	csvReader := csv.NewReader(in)
	csvReader.Comma = rune(separator[0])

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(separator[0])

	sort.Ints(skipLines)

	log = log.WithName("Hash CSV")
	log = log.WithValues("prefix", prefix)
	log = log.WithValues("separator", separator)
	log = log.WithValues("skipLines", skipLines)
	log = log.WithValues("columns", columns)
	log = log.WithValues("columnNames", columnNames)
	log = log.WithValues("createCT", createCT)

	hashReader := &HashReader{
		in:          csvReader,
		csvWriter:   csvWriter,
		out:         rOut,
		SkipLines:   skipLines,
		ColumnNames: columnNames,
		Columns:     columns,
		Prefix:      prefix,
		Key:         key,
		currentline: 0,
		ct:          ct,
		createCT:    createCT,
		ctFileName:  ctFileName,
		log:         log,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()

	return hashReader
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *HashReader) process() error {
	var ctFile *os.File
	var err error
	start := ""
	hash := ""
	idin := ""
	var ok bool
	defer r.csvWriter.Flush()

	if r.createCT {
		// r.ctFileName != "" means that we use a local ct file instead of a map to avoir OOM errors (too much RAM usage)
		if r.ctFileName != "" {
			ctFile, err = os.OpenFile(r.ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
			if err != nil {
				return fmt.Errorf("opening tmp ct file : %w", err)
			}

			defer func() {
				if err := ctFile.Close(); err != nil {
					r.log.Error(err, "close tmp file %w")
				}
			}()
		}
	}

	for {
		r.currentline++
		line, err := r.in.Read()
		if err != nil {
			// end of file
			if errors.Is(err, io.EOF) {
				return nil
			}
			return fmt.Errorf("can't read CSV: %w", err)
		}

		// convert colnames to colnums
		if r.currentline == 1 {
			if len(r.ColumnNames) > 0 {
				r.log.V(1).Info("masking columns", "names", r.ColumnNames)
				r.Columns, err = convertColNameToColNum(line, r.ColumnNames)
				if err != nil {
					return fmt.Errorf("converting colname to colnum : %w", err)
				}
			}
			r.log.V(1).Info("masking columns", "positions", r.Columns, "skiplines", r.SkipLines)
		}

		// Skip lines
		if n := sort.SearchInts(r.SkipLines, r.currentline); !(n < len(r.SkipLines) && r.SkipLines[n] == r.currentline) {
			// mask colums
			for _, col := range r.Columns {
				iC := col - 1

				if col > len(line) {
					return fmt.Errorf("column-position too big: %d > %d", col, len(line))
				} else if iC < 0 {
					return fmt.Errorf("column-position too small: %d", col)
				}

				id := line[iC]

				hash, err = masquage.HashOpenssl(id, r.Key)

				if err != nil {
					return fmt.Errorf("hash CSV %w", err)
				}
				if r.Prefix != "" {
					hash = r.Prefix + ":" + hash
				}
				line[iC] = hash
				if r.createCT {
					if idin, ok = r.ct[hash]; ok {
						if idin != id {
							return fmt.Errorf("colision on hash: %s", hash)
						}
						idin = ""
					} else {

						if r.ctFileName != "" {
							_, err = ctFile.Write([]byte(start + "\n" + "\"" + hash + "\"" + ":" + "\"" + id + "\""))
							start = ","
							if err != nil {
								return fmt.Errorf("adding value in the ct file: %w", err)
							}
						} else {
							if idin, ok = r.ct[hash]; ok {
								if idin != id {
									return fmt.Errorf("collision on hash: %s", hash)
								}
								idin = ""
							} else {
								r.ct[hash] = id
							}
						}
					}

					hash = ""
					id = ""
				}
			}
		}
		if err := r.csvWriter.Write(line); err != nil {
			return fmt.Errorf("can't write CSV: %w", err)
		}

		line = nil

	}
}
func convertColNameToColNum(cols []string, names []string) ([]int, error) {
	colDict := make(map[string]int)
	for colRank, colName := range cols {
		colDict[colName] = colRank + 1
	}

	var colIds []int
	for _, name := range names {
		if colRank, exist := colDict[name]; exist {
			colIds = append(colIds, colRank)
		} else {
			fmt.Printf("[WARNING] col '%s' does not exist in col list but has been specified in your masking configuration\n", name)
		}
	}
	return colIds, nil
}
