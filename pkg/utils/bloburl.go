package utils

import (
	"strings"

	"github.com/Azure/azure-storage-blob-go/azblob"
)

func BlobNameFull(blobUrl *azblob.BlobURL, inputContainerName string) string {
	// remove the container name, surrounded by '/', from the blobName
	return strings.TrimPrefix(blobUrl.URL().Path, "/"+inputContainerName+"/")
}

func BlobName(blobUrl *azblob.BlobURL) string {
	return blobUrl.URL().Path
}
