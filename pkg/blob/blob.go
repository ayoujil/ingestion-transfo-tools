package blob

import (
	"context"
	"fmt"
	"io"

	"github.com/Azure/azure-storage-blob-go/azblob"
)

func DownloadBlob(ctx context.Context, blobURL *azblob.BlockBlobURL, cpk azblob.ClientProvidedKeyOptions) ([]byte, map[string]string, error) {
	blobProps, err := blobURL.GetProperties(ctx, azblob.BlobAccessConditions{}, cpk)
	if err != nil {
		return []byte{}, nil, fmt.Errorf("failed to get properties: %w", err)
	}

	data := make([]byte, blobProps.ContentLength())

	if err := azblob.DownloadBlobToBuffer(ctx, blobURL.BlobURL, 0, 0, data, azblob.DownloadFromBlobOptions{ClientProvidedKeyOptions: cpk}); err != nil {
		return data, nil, fmt.Errorf("failed to download: %w", err)
	}

	return data, blobProps.NewMetadata(), nil
}

func DownloadBlobStream(ctx context.Context, blobURL *azblob.BlockBlobURL, cpk azblob.ClientProvidedKeyOptions) (io.ReadCloser, int64, map[string]string, error) {
	down, err := blobURL.Download(ctx, 0, azblob.CountToEnd, azblob.BlobAccessConditions{}, false, cpk)
	if err != nil {
		return nil, 0, nil, err
	}
	return down.Body(azblob.RetryReaderOptions{}), down.ContentLength(), down.NewMetadata(), nil
}

func Size(ctx context.Context, blobURL *azblob.BlockBlobURL, cpk azblob.ClientProvidedKeyOptions) (int64, error) {
	properties, err := blobURL.GetProperties(ctx, azblob.BlobAccessConditions{}, cpk)
	if err != nil {
		return 0, fmt.Errorf("can't get properties: %w", err)
	}
	return properties.ContentLength(), nil
}

func UploadBlob(ctx context.Context, blobURL *azblob.BlockBlobURL, data []byte, metadata map[string]string, cpk azblob.ClientProvidedKeyOptions) error {
	if _, err := azblob.UploadBufferToBlockBlob(ctx, data, *blobURL, azblob.UploadToBlockBlobOptions{Metadata: metadata, ClientProvidedKeyOptions: cpk}); err != nil {
		return fmt.Errorf("failed upload Blob: %w", err)
	}
	return nil
}

func UploadBlobStream(ctx context.Context, blobURL *azblob.BlockBlobURL, data io.Reader, metadata map[string]string, cpk azblob.ClientProvidedKeyOptions) error {
	if _, err := azblob.UploadStreamToBlockBlob(ctx, data, *blobURL, azblob.UploadStreamToBlockBlobOptions{Metadata: metadata, ClientProvidedKeyOptions: cpk}); err != nil {
		return fmt.Errorf("failed upload Blob Stream: %w", err)
	}
	return nil
}

func DeleteBlob(ctx context.Context, blobURL *azblob.BlockBlobURL) error {
	return DeleteBlobWithLease(ctx, blobURL, "")
}

func SetMetadata(ctx context.Context, blobURL *azblob.BlockBlobURL, metadata map[string]string, cpk azblob.ClientProvidedKeyOptions) error {
	if _, err := blobURL.SetMetadata(ctx, metadata, azblob.BlobAccessConditions{}, cpk); err != nil {
		return fmt.Errorf("set Blob metadata: %w", err)
	}

	return nil
}

func SetMetadataWithLease(ctx context.Context, blobURL *azblob.BlockBlobURL, metadata map[string]string, cpk azblob.ClientProvidedKeyOptions, leaseID string) error {
	accessConditions := azblob.BlobAccessConditions{
		LeaseAccessConditions: azblob.LeaseAccessConditions{
			LeaseID: leaseID,
		},
	}

	if _, err := blobURL.SetMetadata(ctx, metadata, accessConditions, cpk); err != nil {
		return fmt.Errorf("set Blob metadata: %w", err)
	}

	return nil
}

func DeleteBlobWithLease(ctx context.Context, blobURL *azblob.BlockBlobURL, leaseID string) error {
	accessConditions := azblob.BlobAccessConditions{
		LeaseAccessConditions: azblob.LeaseAccessConditions{
			LeaseID: leaseID,
		},
	}

	if _, err := blobURL.Delete(ctx, azblob.DeleteSnapshotsOptionInclude, accessConditions); err != nil {
		return fmt.Errorf("failed delete Blob: %w", err)
	}

	return nil
}
