// +build integration

package blob_test

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/google/uuid"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	rddblob "rdd-pipeline/pkg/blob"
)

func Test_upload_download_delete(t *testing.T) {
	uniqueId := random.UniqueId()
	storageAccountName := fmt.Sprintf("terratest-storage-%s", uniqueId)

	// Configure Terraform setting up a path to Terraform code.
	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "./fixtures/",
		Vars: map[string]interface{}{
			"storage_account_name": storageAccountName,
		},
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	storageAccountConnectionString := terraform.Output(t, terraformOptions, "primary_connection_string")
	containerName := terraform.Output(t, terraformOptions, "storage_container_name")

	tests := []struct {
		name          string
		want          string
		containerName string
		blobName      string
		data          string
		wantHost      string
		wantErr       bool
	}{
		{
			name:          "should succeed",
			want:          "hello world",
			wantErr:       false,
			data:          "hello world",
			wantHost:      storageAccountName + ".blob.core.windows.net",
			containerName: containerName,
			blobName:      uuid.New().String(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client, err := rddblob.NewBlobClientFromConnectionString(storageAccountConnectionString)
			if err != nil {
				t.Errorf("NewBlobClientFromConnectionString() error = %v", err)
			}

			if client.AccountURL.Host != tt.wantHost {
				t.Errorf("NewBlobClientFromConnectionString() = %v, want %v", client.AccountURL.Host, tt.wantHost)
			}

			containerClient, err := client.NewContainerClient(tt.containerName)
			if err != nil {
				t.Errorf("NewContainerClient() error = %v", err)
			}

			err = containerClient.UploadBlob(tt.blobName, []byte(tt.data), nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("UploadBlob() error = %v, wantErr %v", err, tt.wantErr)
			}

			content, _, err := containerClient.DownloadBlob(tt.blobName)
			if (err != nil) != tt.wantErr {
				t.Errorf("DownloadBlob() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(string(content), tt.want) {
				t.Errorf("DownloadBlob() = %v, want %v", string(content), tt.want)
			}

			err = containerClient.DeleteBlob(tt.blobName)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteBlob() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
