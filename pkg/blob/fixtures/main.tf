terraform {
  required_version = "~>1.0"
  required_providers {
    source  = "hashicorp/azurerm"
    version = "~> 2.67.0"
  }
}

provider "azurerm" {
  features {}
}
