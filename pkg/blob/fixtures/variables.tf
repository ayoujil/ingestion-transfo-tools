variable "storage_account_name" {
  type        = string
  description = "(Required) The Name of the storage account to be created."
}
