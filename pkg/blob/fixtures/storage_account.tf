resource "azurerm_storage_account" "test" {
  name                      = var.storage_account_name
  resource_group_name       = azurerm_resource_group.test.name
  location                  = azurerm_resource_group.test.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  account_kind              = "StorageV2"
  enable_https_traffic_only = true
  allow_blob_public_access  = false

  identity {
    type = "SystemAssigned"
  }
}

output "primary_connection_string" {
  value = azurerm_storage_account.test.primary_connection_string
}

resource "azurerm_storage_container" "test" {
  name                  = "test"
  storage_account_name  = azurerm_storage_account.test.name
  container_access_type = "private"
}

output "storage_container_name" {
  value = azurerm_storage_container.test.name
}
