package blob

import (
	"fmt"
	"net/url"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/Azure/azure-pipeline-go/pipeline"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/Azure/azure-storage-queue-go/azqueue"
	"github.com/Azure/go-autorest/autorest/adal"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	"github.com/go-logr/logr"
)

const (
	connectionStringAccountName      = "accountname"
	connectionStringAccountKey       = "accountkey"
	connectionStringEndpointSuffix   = "endpointsuffix"
	connectionStringEndpointProtocol = "defaultendpointsprotocol"
	blobServiceName                  = "blob"
	tokenRefreshTolerance            = 300
)

func NewContainerClientFromConnectionString(connectionString, containerName string) (*azblob.ContainerURL, error) {
	parts := map[string]string{}
	for _, pair := range strings.Split(connectionString, ";") {
		if pair == "" {
			continue
		}

		equalDex := strings.IndexByte(pair, '=')
		if equalDex <= 0 {
			return nil, fmt.Errorf("invalid connection segment %q", pair)
		}

		value := strings.TrimSpace(pair[equalDex+1:])
		key := strings.TrimSpace(strings.ToLower(pair[:equalDex]))
		parts[key] = value
	}

	credential, err := azblob.NewSharedKeyCredential(parts[connectionStringAccountName], parts[connectionStringAccountKey])
	if err != nil {
		return nil, err
	}

	pipeline := azblob.NewPipeline(credential, azblob.PipelineOptions{})

	url, err := url.Parse(fmt.Sprintf("%s://%s.%s.%s", parts[connectionStringEndpointProtocol], parts[connectionStringAccountName], blobServiceName, parts[connectionStringEndpointSuffix]))
	if err != nil {
		return nil, err
	}

	url.Path = path.Join(url.Path, containerName)

	client := azblob.NewContainerURL(*url, pipeline)

	return &client, nil
}

func NewContainerClientFromEnvironment(log logr.Logger, accountURL, containerName string) (*azblob.ContainerURL, pipeline.Pipeline, error) {
	log = log.WithValues("accountURL", accountURL, "containerName", containerName)

	settings, err := auth.GetSettingsFromEnvironment()
	settings.Values[auth.Resource] = "https://storage.azure.com/"

	if err != nil {
		return nil, nil, fmt.Errorf("read environment: %w", err)
	}

	getClient := func() (*adal.ServicePrincipalToken, error) {
		// 1.Client Credentials
		if clientConfig, err := settings.GetClientCredentials(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 2. Client Certificate
		if clientConfig, err := settings.GetClientCertificate(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 3. Username Password
		if clientConfig, err := settings.GetUsernamePassword(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 4. MSI
		return settings.GetMSI().ServicePrincipalToken()
	}

	spt, err := getClient()
	if err != nil {
		return nil, nil, fmt.Errorf("create service principal token: %w", err)
	}

	return createContainerClientFromServicePrincipalToken(log, spt, accountURL, containerName)
}

func NewQueueClientFromEnvironment(log logr.Logger, accountURL, queueName string) (*azqueue.QueueURL, error) {
	log = log.WithValues("accountURL", accountURL, "queueName", queueName)

	settings, err := auth.GetSettingsFromEnvironment()
	settings.Values[auth.Resource] = "https://storage.azure.com/"

	if err != nil {
		return nil, fmt.Errorf("read environment: %w", err)
	}

	getClient := func() (*adal.ServicePrincipalToken, error) {
		// 1.Client Credentials
		if clientConfig, err := settings.GetClientCredentials(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 2. Client Certificate
		if clientConfig, err := settings.GetClientCertificate(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 3. Username Password
		if clientConfig, err := settings.GetUsernamePassword(); err == nil {
			return clientConfig.ServicePrincipalToken()
		}
		// 4. MSI
		return settings.GetMSI().ServicePrincipalToken()
	}

	spt, err := getClient()
	if err != nil {
		return nil, fmt.Errorf("create service principal token: %w", err)
	}

	return createQueueClientFromServicePrincipalToken(log, spt, accountURL, queueName)
}

func createContainerClientFromServicePrincipalToken(log logr.Logger, spt *adal.ServicePrincipalToken, accountURL string, containerName string) (*azblob.ContainerURL, pipeline.Pipeline, error) {
	var tokenRefresher azblob.TokenRefresher = func(credential azblob.TokenCredential) time.Duration {
		log.V(2).Info("Refresh Azure Storage auth token")

		err := spt.Refresh()
		if err != nil {
			log.Error(err, "refresh token")
			return 0
		}

		token := spt.Token()
		if token.IsZero() {
			log.Error(fmt.Errorf("empty token received"), "acquire token from the SP Token")
			return 0
		}

		expiresIn, err := strconv.ParseInt(string(token.ExpiresIn), 10, 64)
		if err != nil {
			log.Error(err, "new token expiresIn cannot be parsed", "token.ExpiresIn", token.ExpiresIn)
			return 0
		}

		credential.SetToken(token.AccessToken)

		return time.Duration(expiresIn-tokenRefreshTolerance) * time.Second
	}

	pipeline := azblob.NewPipeline(azblob.NewTokenCredential("", tokenRefresher), azblob.PipelineOptions{
		Retry: azblob.RetryOptions{
			// cf. https://github.com/Azure/azure-storage-blob-go/blob/master/azblob/zc_policy_retry.go#L30
			// TryTimeout indicates the maximum time allowed for any single try of an HTTP request.
			// A value of zero means that you accept our default timeout. NOTE: When transferring large amounts
			// of data, the default TryTimeout will probably not be sufficient. You should override this value
			// based on the bandwidth available to the host machine and proximity to the Storage service. A good
			// starting point may be something like (60 seconds per MB of anticipated-payload-size).
			TryTimeout: time.Minute * 500 * 1024,
		},
	})

	url, err := url.Parse(accountURL)
	if err != nil {
		return nil, nil, err
	}

	url.Path = path.Join(url.Path, containerName)

	client := azblob.NewContainerURL(*url, pipeline)

	return &client, pipeline, nil
}

func createQueueClientFromServicePrincipalToken(log logr.Logger, spt *adal.ServicePrincipalToken, accountURL string, queueName string) (*azqueue.QueueURL, error) {
	var tokenRefresher azqueue.TokenRefresher = func(credential azqueue.TokenCredential) time.Duration {
		log.V(2).Info("Refresh Azure Storage auth token")

		err := spt.Refresh()
		if err != nil {
			log.Error(err, "refresh token")
			return 0
		}

		token := spt.Token()
		if token.IsZero() {
			log.Error(fmt.Errorf("empty token received"), "acquire token from the SP Token")
			return 0
		}

		expiresIn, err := strconv.ParseInt(string(token.ExpiresIn), 10, 64)
		if err != nil {
			log.Error(err, "new token expiresIn cannot be parsed", "token.ExpiresIn", token.ExpiresIn)
			return 0
		}

		credential.SetToken(token.AccessToken)

		return time.Duration(expiresIn-tokenRefreshTolerance) * time.Second
	}

	pipeline := azqueue.NewPipeline(azqueue.NewTokenCredential("", tokenRefresher), azqueue.PipelineOptions{
		Retry: azqueue.RetryOptions{
			TryTimeout: time.Minute * 500 * 1024,
		},
	})

	url, err := url.Parse(fmt.Sprintf("%s/%s", accountURL, queueName))

	if err != nil {
		return nil, err
	}
	client := azqueue.NewQueueURL(*url, pipeline)

	return &client, nil
}

func NewContainerClientFromSharedKey(accountName, containerName, accountKey string) (*azblob.ContainerURL, error) {
	cred, err := azblob.NewSharedKeyCredential(accountName, accountKey)
	if err != nil {
		return nil, fmt.Errorf("failed to get sharedCredential: %w", err)
	}

	pipeline := azblob.NewPipeline(cred, azblob.PipelineOptions{})

	url, err := url.Parse(fmt.Sprintf("https://%s.blob.core.windows.net", accountName))
	if err != nil {
		return nil, fmt.Errorf("failed to parse url for %s: %w", accountName, err)
	}

	url.Path = path.Join(url.Path, containerName)

	client := azblob.NewContainerURL(*url, pipeline)

	return &client, nil
}
