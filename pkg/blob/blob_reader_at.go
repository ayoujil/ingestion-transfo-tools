package blob

import (
	"context"
	"fmt"
	"io"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
)

type BlobReaderAt struct {
	blobURL *azblob.BlobURL
	Context context.Context
	log     logr.Logger
	cpk     azblob.ClientProvidedKeyOptions
}

func NewBlobReaderAt(context context.Context, log logr.Logger, blobURL *azblob.BlobURL, cpk azblob.ClientProvidedKeyOptions) *BlobReaderAt {
	return &BlobReaderAt{
		blobURL: blobURL,
		Context: context,
		cpk:     cpk,
		log:     log,
	}
}

func (b *BlobReaderAt) ReadAt(p []byte, offset int64) (int, error) {
	down, err := b.blobURL.Download(b.Context, offset, int64(len(p)), azblob.BlobAccessConditions{}, false, b.cpk)
	if err != nil {
		return 0, fmt.Errorf("download failed with: %w", err)
	}

	r := down.Response().Body
	defer r.Close()

	n, err := io.ReadFull(r, p)
	if err != nil {
		return n, fmt.Errorf("readFull failed: %w", err)
	}

	b.log.V(2).Info("blobreader", "read", n, "len", len(p), "offset", offset)
	return n, nil
}

func (b *BlobReaderAt) SizeWithError() (int64, error) {
	resp, err := b.blobURL.GetProperties(b.Context, azblob.BlobAccessConditions{}, b.cpk)
	if err != nil {
		return 0, fmt.Errorf("get blob size failed: %w", err)
	}
	return resp.ContentLength(), nil
}

func (b *BlobReaderAt) Size() int64 {
	size, err := b.SizeWithError()
	if err != nil {
		return 0
	}
	return size
}
