package blob

import (
	"crypto/aes"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"os"

	"github.com/Azure/azure-storage-blob-go/azblob"
)

const cpkEnvironmentVariable = "HDH_CPK"

func NewClientProvidedKeyOptions() (azblob.ClientProvidedKeyOptions, error) {
	key, present := os.LookupEnv(cpkEnvironmentVariable)

	cpk := azblob.ClientProvidedKeyOptions{
		EncryptionAlgorithm: azblob.EncryptionAlgorithmNone,
	}

	if !present {
		return cpk, nil
	}

	bKey := []byte(key)

	if l := len(key); l != 32 {
		return cpk, fmt.Errorf("env variable %s is not an AES 256 key. Size %d, should be 32", cpkEnvironmentVariable, l)
	}

	if _, err := aes.NewCipher(bKey); err != nil {
		return cpk, fmt.Errorf("env variable %s is not an AES", cpkEnvironmentVariable)
	}

	b64Key := base64.StdEncoding.EncodeToString(bKey)
	sha := sha256.Sum256(bKey)
	b64Sha := base64.StdEncoding.EncodeToString(sha[:])
	scope := ""

	cpk = azblob.NewClientProvidedKeyOptions(&b64Key, &b64Sha, &scope)

	return cpk, nil
}
