package keyvault

import (
	"context"
	"encoding/base64"
	"fmt"

	kvauth "github.com/Azure/azure-sdk-for-go/services/keyvault/auth"
	azkeyvault "github.com/Azure/azure-sdk-for-go/services/keyvault/v7.1/keyvault"
)

type KeyVaultClient struct {
	Client *azkeyvault.BaseClient
	KvURL  string
}

func NewKeyVaultClientFromEnvironment(url string) (*KeyVaultClient, error) {
	authorizer, err := kvauth.NewAuthorizerFromEnvironment()
	if err != nil {
		return nil, fmt.Errorf("unable to create vault authorizer: %w", err)
	}

	basicClient := azkeyvault.New()
	basicClient.Authorizer = authorizer

	c := &KeyVaultClient{
		Client: &basicClient,
		KvURL:  url,
	}
	return c, nil
}

func NewKeyVaultClientFromCLI(url string) (*KeyVaultClient, error) {
	authorizer, err := kvauth.NewAuthorizerFromCLI()
	if err != nil {
		return nil, fmt.Errorf("unable to create vault authorizer: %w", err)
	}

	basicClient := azkeyvault.New()
	basicClient.Authorizer = authorizer

	c := &KeyVaultClient{
		Client: &basicClient,
		KvURL:  url,
	}
	return c, nil
}

func (c *KeyVaultClient) GetKey(keyName string) ([]byte, error) {
	return nil, fmt.Errorf("Not Supported: %s", "yet")
}

func (c *KeyVaultClient) GetSecret(ctx context.Context, secretName string) ([]byte, error) {
	secretValue, err := c.Client.GetSecret(ctx, c.KvURL, secretName, "")
	if err != nil {
		return nil, fmt.Errorf("unable to get secret value: %w", err)
	}

	return []byte(*secretValue.Value), nil
}

func (c *KeyVaultClient) Wrap(ctx context.Context, keyName string, value []byte) ([]byte, error) {
	v := base64.RawURLEncoding.EncodeToString(value)
	parameters := azkeyvault.KeyOperationsParameters{Value: &v, Algorithm: azkeyvault.RSAOAEP256}
	result, err := c.Client.WrapKey(ctx, c.KvURL, keyName, "", parameters)
	if err != nil {
		return nil, fmt.Errorf("unable to wrap key '%s': %w", keyName, err)
	}

	return base64.RawURLEncoding.DecodeString(*result.Result)
}

func (c *KeyVaultClient) UnWrap(ctx context.Context, keyName string, value []byte) ([]byte, error) {
	v := base64.RawURLEncoding.EncodeToString(value)
	parameters := azkeyvault.KeyOperationsParameters{Value: &v, Algorithm: azkeyvault.RSAOAEP256}
	result, err := c.Client.UnwrapKey(ctx, c.KvURL, keyName, "", parameters)
	if err != nil {
		return nil, fmt.Errorf("unable to unwrap key '%s': %w", keyName, err)
	}

	return base64.RawURLEncoding.DecodeString(*result.Result)
}
