package gpg

import (
	"errors"
	"fmt"
	"io"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/go-logr/logr"
)

func Decrypt(log logr.Logger, input, publicKey, secretKey io.Reader, passphrase []byte) (io.Reader, error) {
	// Open the private key file

	var publicKeyRing *crypto.KeyRing

	if publicKey != nil {
		log.V(1).Info("Read Public Key")
		publicKeyObj, err := crypto.NewKeyFromArmoredReader(publicKey)
		if err != nil {
			return nil, fmt.Errorf("fail reading armored public key: %w", err)
		}

		publicKeyRing, err = crypto.NewKeyRing(publicKeyObj)
		if err != nil {
			return nil, fmt.Errorf("fail creating public keyring: %w", err)
		}
		log.V(1).Info("Finished reading public key from the keyvault", "keyid", publicKeyObj.GetKeyID(), "fingerprint", publicKeyObj.GetFingerprint())
	}

	log.V(1).Info("Read Private Key")
	privateKeyObj, err := crypto.NewKeyFromArmoredReader(secretKey)
	if err != nil {
		return nil, fmt.Errorf("fail reading armored private key: %w", err)
	}

	locked, err := privateKeyObj.IsLocked()
	if err != nil {
		return nil, fmt.Errorf("can't check locked: %w", err)
	}

	if locked {
		if len(passphrase) != 0 {
			log.V(1).Info("Decrypting private key using passphrase")
			privateKeyObj, err = privateKeyObj.Unlock(passphrase)
			if err != nil {
				return nil, fmt.Errorf("fail unlocking privateKey with password: %w", err)
			}
		} else {
			return nil, errors.New("Required passphrase not provided")
		}
	} else if len(passphrase) != 0 {
		log.V(1).Info("key is unlocked. unrequired passphrase provided!")
	}

	privateKeyRing, err := crypto.NewKeyRing(privateKeyObj)
	if err != nil {
		return nil, fmt.Errorf("fail creating private keyring: %w", err)
	}

	log.V(1).Info("Finished decrypting private key from the keyvault", "keyid", privateKeyObj.GetKeyID(), "fingerprint", privateKeyObj.GetFingerprint())

	message, err := privateKeyRing.DecryptStream(input, publicKeyRing, crypto.GetUnixTime())
	if err != nil {
		return nil, fmt.Errorf("fail decrypt input: %w", err)
	}

	privateKeyRing.ClearPrivateParams()

	return message, nil
}
