package gpg_test

import (
	"io"
	"net/http"
	"os"
	"reflect"
	"testing"

	"rdd-pipeline/pkg/crypt/gpg"

	"github.com/go-logr/logr"
)

const (
	passphrase      = "testhdh"
	dummypassphrase = "dummy"
)

func TestDecrypt(t *testing.T) {
	encyptedFile, _ := os.Open("testdata/42encrypted.pgp")
	encyptedAndSignedFile, _ := os.Open("testdata/42encrypted&signed.pgp")
	privateKey, _ := os.Open("testdata/private-key.asc")
	publicKey, _ := os.Open("testdata/public-key.asc")
	publicGuilhem, _ := os.Open("testdata/public-guilhem.asc")
	keyErrored, _ := os.Open("testdata/public-errored.asc")
	publicDummy, _ := os.Open("testdata/public-dummy.asc")
	privateNoSign, _ := os.Open("testdata/private-nosign.asc")
	privateDummy, _ := os.Open("testdata/private-dummy.asc")

	p11encrypted, _ := os.Open("testdata/p11_fake_crypted.png")
	p11public, _ := os.Open("testdata/p11_fake_transfer_public_key.rsa")
	p11HDHprivate, _ := os.Open("testdata/p11_fake_hdh_priv.rsa")

	type args struct {
		input      io.Reader
		publicKey  io.Reader
		secretKey  io.Reader
		passphrase string
	}
	tests := []struct {
		name        string
		args        args
		want        []byte
		contentType string
		wantErr     bool
	}{
		{
			name: "KO private key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicGuilhem,
				secretKey:  keyErrored,
				passphrase: passphrase,
			},
			want:    []byte("42"),
			wantErr: true,
		},
		{
			name: "KO public key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  keyErrored,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:    []byte("42"),
			wantErr: true,
		},
		{
			name: "No sign KO private key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicGuilhem,
				secretKey:  privateNoSign,
				passphrase: passphrase,
			},
			want:    []byte("42"),
			wantErr: true,
		},
		{
			name: "OK encrypt and OK signed and no passphrase",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicGuilhem,
				secretKey:  privateKey,
				passphrase: "",
			},
			want:    []byte("42"),
			wantErr: true,
		},
		{
			name: "OK encrypt and OK signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicGuilhem,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			contentType: "text/plain; charset=utf-8",
			wantErr:     false,
		},
		{
			name: "OK encrypt and KO signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicDummy,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:    nil,
			wantErr: true, // Can't find signature
		},
		{
			name: "KO encrypt and OK signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicGuilhem,
				secretKey:  privateDummy,
				passphrase: dummypassphrase,
			},
			want:    nil,
			wantErr: true, // openpgp: incorrect key
		},
		{
			name: "OK encrypt",
			args: args{
				input:      encyptedFile,
				publicKey:  nil,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:    []byte("42"),
			wantErr: false,
		},
		{
			name: "KO encrypt",
			args: args{
				input:      encyptedFile,
				publicKey:  nil,
				secretKey:  privateDummy,
				passphrase: dummypassphrase,
			},
			want:    nil,
			wantErr: true, // openpgp: incorrect key
		},
		{
			name: "KO passphrase",
			args: args{
				input:      encyptedFile,
				publicKey:  publicKey,
				secretKey:  privateKey,
				passphrase: "bad-passphrase",
			},
			want:    nil,
			wantErr: true, // openpgp: invalid data: private key checksum failure
		},
		{
			name: "deepPiste",
			args: args{
				input:      p11encrypted,
				publicKey:  p11public,
				secretKey:  p11HDHprivate,
				passphrase: "123456",
			},
			want:        nil,
			contentType: "image/png",
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		(publicKey.Seek(0, 0))
		(privateKey.Seek(0, 0))
		(encyptedAndSignedFile.Seek(0, 0))
		(encyptedFile.Seek(0, 0))
		(publicGuilhem.Seek(0, 0))
		(privateDummy.Seek(0, 0))
		(publicDummy.Seek(0, 0))

		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint
			got, err := gpg.Decrypt(logr.Discard(), tt.args.input, tt.args.publicKey, tt.args.secretKey, []byte(tt.args.passphrase))
			var gotB []byte
			if err == nil {
				gotB, err = io.ReadAll(got)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Decrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				t.Logf("error is: %v", err)
				return
			}
			if tt.want != nil && !reflect.DeepEqual(gotB, tt.want) {
				t.Errorf("Decrypt() = %v, want %v", got, tt.want)
			}
			if tt.contentType != "" {
				ct := http.DetectContentType(gotB)
				if ct != tt.contentType {
					t.Errorf("Decrypt() ContentType = %v, want %v", ct, tt.contentType)
				}
			}
		})
	}
}
