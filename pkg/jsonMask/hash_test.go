package jsonMask

import (
	"io"
	"reflect"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func newHashReader(tags []string, testType string) hashReaderTest {
	var jsonReader io.Reader
	if testType == "standard" {
		jsonReader = io.Reader(
			strings.NewReader(`[ { "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } }, { "id": "4.5.7.9.6.969540965406", "userId": "PATIENT_2", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`),
		)
	} else if testType == "oneObject" {
		jsonReader = io.Reader(
			strings.NewReader(`{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "characteristics": { "birthdate": "1990-09-14", "age": 18 } } }`),
		)
	} else if testType == "multilineOneObject" {
		jsonReader = io.Reader(
			strings.NewReader(`{
				"id": "1.2.3.4.5.4732847239432",
				"userId": "PATIENT_1",
				"value": {
					"characteristics": {
						"birthdate": "1990-09-14",
						"age": 18
					}
				}
			}`),
		)
	}
	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	return hashReaderTest{
		Reader: &HashReader{
			log:        logr.Discard(),
			in:         jsonReader,
			Key:        "0123456789abcdef",
			Prefix:     "HDH",
			Tags:       tags,
			jsonWriter: jsonWriter,
			ct:         masquage.CorrespondenceTable{},
			out:        rOut,
		},
		Writer: wOut,
	}
}

func TestNewHashReader(t *testing.T) {
	Tags := []string{"$..userId"}

	type args struct {
		log     logr.Logger
		in      io.Reader
		key     string
		prefix  string
		tags    []string
		ct      masquage.CorrespondenceTable
		ct_name string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "create",
			args: args{
				log:     logr.Discard(),
				in:      strings.NewReader(""),
				key:     "0123456789abcdef",
				prefix:  "HDH",
				tags:    Tags,
				ct:      masquage.CorrespondenceTable{},
				ct_name: "ct",
			},
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewHashReader(tt.args.log, tt.args.in, tt.args.key, tt.args.prefix, tt.args.tags, tt.args.ct, tt.args.ct_name)

			switch v := got.(type) {
			case *HashReader, io.Reader:

			default:
				t.Errorf("NewHashReader() = %v", v)
			}
		})
	}
}

func TestHashReader_process(t *testing.T) {
	jsonOut := `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`
	jsonOutTwoIds := `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`
	jsonOutDirect := `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`
	jsonIn := `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`
	jsonArrayIndexSimple := `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`
	jsonArrayMultiplePatterns := `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75","HDH:698c715f1d6fdd20dc6046dd2bb1e080b94cf1a13ae92183210f072d77a18d2c","HDH:48b4685e99def91bd550b17ef4c4302377a3a322cfa59abdc2ea56dd5c92961e"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`
	jsonOneObject := `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}}`
	jsonMultilineOneObject := `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}}`

	tests := []struct {
		name    string
		fields  hashReaderTest
		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name:    "One key to mask with in-depth pattern ($..userId)",
			fields:  newHashReader([]string{"$..userId"}, "standard"),
			wantErr: false,
			want:    jsonOut,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			},
		}, {
			name:    "Two keys to mask with indepth pattern ($..userId, $..birthdate)",
			fields:  newHashReader([]string{"$..userId", "$..birthdate"}, "standard"),
			wantErr: false,
			want:    jsonOutTwoIds,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
			},
		}, {
			name:    "One key to mask, direct pattern ($.value.characteristics.birthdate)",
			fields:  newHashReader([]string{"$.value.characteristics.birthdate"}, "standard"),
			wantErr: false,
			want:    jsonOutDirect,
			wantCt: masquage.CorrespondenceTable{
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
			},
		}, {
			// In case of non existing key, the Json object should simply not change
			name:    "Non existing key ($..iddddddd)",
			fields:  newHashReader([]string{"$..idddddddd"}, "standard"),
			wantErr: false,
			want:    jsonIn,
			wantCt:  masquage.CorrespondenceTable{},
		}, {
			//
			name:    "in depth with array index",
			fields:  newHashReader([]string{"$.value.medicine[0]"}, "standard"),
			wantErr: false,
			want:    jsonArrayIndexSimple,
			wantCt: masquage.CorrespondenceTable{
				"HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75": "doliprane",
			},
		}, {
			name:    "Multiple type keys: ($.value.medicine[*], $.value.characteristics.birthdate, $..idddddddd, $.userId)",
			fields:  newHashReader([]string{"$.value.medicine[*]", "$.value.characteristics.birthdate", "$..idddddddd", "$.userId"}, "standard"),
			wantErr: false,
			want:    jsonArrayMultiplePatterns,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:48b4685e99def91bd550b17ef4c4302377a3a322cfa59abdc2ea56dd5c92961e": "benzo",
				"HDH:698c715f1d6fdd20dc6046dd2bb1e080b94cf1a13ae92183210f072d77a18d2c": "oxycodon",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
				"HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75": "doliprane",
			},
		}, {
			name:    "Single json object: ($..userId, $..birthdate)",
			fields:  newHashReader([]string{"$..userId", "$..birthdate"}, "oneObject"),
			wantErr: false,
			want:    jsonOneObject,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
			},
		}, {
			name:    "Single json object: ($..userId, $..birthdate)",
			fields:  newHashReader([]string{"$..userId", "$..birthdate"}, "multilineOneObject"),
			wantErr: false,
			want:    jsonMultilineOneObject,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := tt.fields
			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			gotString := strings.ReplaceAll(string(got), "\n", "")
			gotString = strings.ReplaceAll(gotString, "\r", "")
			gotString = strings.ReplaceAll(gotString, " ", "")
			gotString = strings.ReplaceAll(gotString, "	", "")

			if gotString != tt.want {
				t.Errorf("process() = \n%s\n -- want \n%v\n", gotString, tt.want)
			}
			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("CT: process() = \n%s\n -- want CT \n%v\n", r.Reader.ct, tt.wantCt)
			}
		})
	}
}
