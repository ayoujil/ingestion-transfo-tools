package jsonMask

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"

	"rdd-pipeline/pkg/masquage"

	"github.com/PaesslerAG/jsonpath"

	"github.com/go-logr/logr"
)

type HashReader struct {
	in              io.Reader
	Prefix          string
	Tags            []string
	Key             string
	currentline     int
	jsonWriter      *io.PipeWriter
	out             *io.PipeReader
	ct              masquage.CorrespondenceTable
	log             logr.Logger
	ctContainerName string
}

func NewHashReader(log logr.Logger, in io.Reader, key string, prefix string, tags []string, ct masquage.CorrespondenceTable, ctContainerName string) io.Reader {
	jsonReader := in
	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	log = log.WithName("Hash json")
	log = log.WithValues("prefix", prefix)
	log = log.WithValues("tags", tags)

	hashReader := &HashReader{
		in:              jsonReader,
		jsonWriter:      jsonWriter,
		out:             rOut,
		Tags:            tags,
		Prefix:          prefix,
		Key:             key,
		currentline:     0,
		ct:              ct,
		log:             log,
		ctContainerName: ctContainerName,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return hashReader
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *HashReader) process() error {
	// Read char by char as of now. Possible upgrade: read more to reduce the number of call to r.in.Read
	content := make([]byte, 1)
	var currentObjectString string = ""
	var firstBracket bool = false
	var nbOpened int = 0
	var firstItem bool = true
	var isArray bool = false
	var end = false
	for {
		_, err := r.in.Read(content)

		if err == io.EOF {
			end = true
		} else if err != nil {
			return fmt.Errorf("reading char : %w", err)
		}

		currentChar := rune(content[0])

		// if New Json object
		if currentChar == '[' && currentObjectString == "" { //Subcase: json object is surrounded by []
			isArray = true
		}
		if currentChar == '{' && currentObjectString == "" {
			nbOpened = 1
			firstBracket = true
		}

		if nbOpened > 0 {
			if currentChar == '{' && !firstBracket {
				nbOpened++
			} else if currentChar == '}' {
				nbOpened--
				firstBracket = false
			} else {
				firstBracket = false
			}
			currentObjectString += string(currentChar)
		}
		if nbOpened == 0 {
			if currentObjectString != "" {
				// End of the current json object (reached the last '}'), it is now stores in the "currentObjectString" string in memory
				payload := interface{}(nil)

				err := json.Unmarshal([]byte(currentObjectString), &payload)
				if err != nil {
					log.Fatal(err)
				}

				if !firstItem {
					currentObjectString = "," + currentObjectString
				}
				// Check JsonPath
				for _, path := range r.Tags {
					value, _ := jsonpath.Get(path, payload)
					// No check on the existance of the key, it might not exist in some json object, but still be valid
					if fmt.Sprintf("%T", value) == "string" {
						// The if/else is needed because the type of "value" can varry with this library
						valueString := fmt.Sprintf("%v", value)
						hash, err := masquage.Hash(fmt.Sprintf("%v", valueString), r.Key)

						if err != nil {
							return fmt.Errorf("hash CSV %w", err)
						}

						if r.Prefix != "" {
							hash = fmt.Sprintf("%s:%s", r.Prefix, hash)
						}

						currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", valueString), hash)
						r.ct[hash] = fmt.Sprintf("%v", valueString)

					} else if fmt.Sprintf("%T", value) == "[]interface {}" {
						for _, val := range value.([]interface{}) {

							hash, err := masquage.Hash(fmt.Sprintf("%v", val), r.Key)
							if err != nil {
								return fmt.Errorf("hash CSV %w", err)
							}

							if r.Prefix != "" {
								hash = fmt.Sprintf("%s:%s", r.Prefix, hash)
							}
							currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", val), hash)
							r.ct[hash] = fmt.Sprintf("%v", val)
						}
					} else {
						continue
					}
				}
				if firstItem {
					if isArray {
						currentObjectString = "[" + currentObjectString
					}
					firstItem = false
				}

				if _, err := io.WriteString(r.jsonWriter, currentObjectString); err != nil {
					log.Fatal(err)
				}

				currentObjectString = ""
			}
		}

		if end {
			if firstItem {
				if _, err := io.WriteString(r.jsonWriter, currentObjectString+"} "); err != nil {
					log.Fatal(err)
				}
			}
			if isArray {
				if _, err := io.WriteString(r.jsonWriter, "] "); err != nil {
					log.Fatal(err)
				}
				isArray = false
			}
			break
		}

	}

	return nil
}
