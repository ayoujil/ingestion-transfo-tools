package jsonMask

import (
	"io"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	"github.com/go-logr/logr"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(tags []string, ct masquage.CorrespondenceTable) pairingReaderTest {
	jsonReader := io.Reader(
		strings.NewReader(`[{ "id": "1.2.3.4.5.4732847239432", "userId": "HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } }
,{ "id": "4.5.7.9.6.969540965406", "userId": "HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }
]`),
	)

	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	return pairingReaderTest{
		Reader: &PairingReader{
			log:        logr.Discard(),
			in:         jsonReader,
			Tags:       tags,
			jsonWriter: jsonWriter,
			ct:         ct,
			out:        rOut,
		},
		Writer: wOut,
	}
}
func TestNewPairingReader(t *testing.T) {
	tags := []string{"userId"}

	type args struct {
		log  logr.Logger
		in   io.Reader
		tags []string
		ct   masquage.CorrespondenceTable
	}
	tests := []struct {
		name string
		args args
		want io.Reader
	}{
		{
			name: "create",
			args: args{
				log:  logr.Discard(),
				in:   strings.NewReader(""),
				tags: tags,
				ct:   masquage.CorrespondenceTable{},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewPairingReader(tt.args.log, tt.args.in, tt.args.tags, tt.args.ct)

			switch v := got.(type) {
			case *PairingReader, io.Reader:

			default:
				t.Errorf("NewPairingReader() = %v", v)
			}
		})
	}
}

func TestPairingReader_process(t *testing.T) {
	jsonOut := `[{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } },{ "id": "4.5.7.9.6.969540965406", "userId": "PATIENT_2", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`
	jsonOutNotFound := `[{ "id": "1.2.3.4.5.4732847239432", "userId": "HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } },{ "id": "4.5.7.9.6.969540965406", "userId": "HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`

	tests := []struct {
		name    string
		fields  pairingReaderTest
		wantErr bool
		want    string
	}{
		{
			name: "simple with in-depth jsonPath ($..userId)  ",
			fields: newPairingReader([]string{"$..userId"}, masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			}),
			wantErr: false,
			want:    jsonOut,
		}, {
			name: "Id not existing ($.iiiiiiiid)  ",
			fields: newPairingReader([]string{"$..iiiiiiiid"}, masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			}),
			wantErr: false,
			want:    jsonOutNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := tt.fields
			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("Hash() = %s, want %v", got, tt.want)
			}
		})
	}
}
