package jsonMask

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"

	"rdd-pipeline/pkg/masquage"

	"github.com/PaesslerAG/jsonpath"
	"github.com/go-logr/logr"
)

type PairingReader struct {
	in              io.Reader
	Tags            []string
	currentline     int
	jsonWriter      *io.PipeWriter
	out             *io.PipeReader
	ct              masquage.CorrespondenceTable
	log             logr.Logger
	ctContainerName string
}

func NewPairingReader(log logr.Logger, in io.Reader, tags []string, ct masquage.CorrespondenceTable) io.Reader {
	jsonReader := in
	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	log = log.WithName("Pairing json")
	log = log.WithValues("tags", tags)

	pairingReader := &PairingReader{
		in:          jsonReader,
		jsonWriter:  jsonWriter,
		out:         rOut,
		Tags:        tags,
		currentline: 0,
		ct:          ct,
		log:         log,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {
	// Read char by char as of now. Possible upgrade: read more to reduce the number of call to r.in.Read
	content := make([]byte, 1)
	var currentObjectString string = ""
	var firstBracket bool = false
	var nbOpened int = 0
	var firstItem bool = true

	for {
		_, err := r.in.Read(content)

		if err == io.EOF {
			if firstItem {
				if _, err := io.WriteString(r.jsonWriter, currentObjectString+"}"); err != nil {
					log.Fatal(err)
				}
				break
			}
			if _, err := io.WriteString(r.jsonWriter, "]"); err != nil {
				log.Fatal(err)
			}
			break
		} else if err != nil {
			return fmt.Errorf("reading char : %w", err)
		}

		currentChar := rune(content[0])

		// if New Json object
		if currentChar == '{' && currentObjectString == "" {
			nbOpened = 1
			firstBracket = true
		}

		if nbOpened > 0 {
			if currentChar == '{' && !firstBracket {
				nbOpened++
			} else if currentChar == '}' {
				nbOpened--
				firstBracket = false
			} else {
				firstBracket = false
			}
			currentObjectString += string(currentChar)
		}

		if nbOpened == 0 {
			// End of the current json object (reached the last '}'), it is now stores in the "currentObjectString" string in memory
			if currentObjectString != "" {

				payload := interface{}(nil)

				err := json.Unmarshal([]byte(currentObjectString), &payload)
				if err != nil {
					log.Fatal(err)
				}

				if currentObjectString != "" {
					if !firstItem {
						currentObjectString = "," + currentObjectString
					}

					for _, path := range r.Tags {
						value, _ := jsonpath.Get(path, payload)
						if fmt.Sprintf("%T", value) == "string" {
							// The if/else is needed because the type of "value" can varry with this library
							valueString := fmt.Sprintf("%v", value)
							idCT, ok := r.ct[valueString]
							if !ok {
								return fmt.Errorf("CT key %v could not be found", valueString)
							}

							currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", valueString), idCT)
						} else if fmt.Sprintf("%T", value) == "[]interface {}" {
							for _, val := range value.([]interface{}) {
								idCT, ok := r.ct[fmt.Sprintf("%v", val)]
								if !ok {
									return fmt.Errorf("CT key %v could not be found", val)
								}

								currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", val), idCT)
							}
						} else {
							continue
						}
					}

					if firstItem {
						currentObjectString = "[" + currentObjectString
						firstItem = false
					}

					if _, err := io.WriteString(r.jsonWriter, currentObjectString); err != nil {
						log.Fatal(err)
					}
					currentObjectString = ""
				}
			}
		}

	}
	return nil
}
