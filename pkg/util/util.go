package util

type VersionedConfig interface {
	GetVersion() string
	GetKind() string
	Upgrade() (VersionedConfig, error)
}
