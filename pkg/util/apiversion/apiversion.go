package apiversion

import (
	"fmt"
	"regexp"

	"github.com/blang/semver"
)

var re = regexp.MustCompile(`^rdd-pipeline/v(\d)(?:(alpha|beta)([1-9]?[0-9]))?$`)

// Parse parses a string into a semver.Version.
func Parse(v string) (semver.Version, error) {
	res := re.FindStringSubmatch(v)
	if res == nil {
		return semver.Version{}, fmt.Errorf("%s is an invalid api version", v)
	}
	if res[2] == "" || res[3] == "" {
		return semver.Parse(fmt.Sprintf("%s.0.0", res[1]))
	}
	return semver.Parse(fmt.Sprintf("%s.0.0-%s.%s", res[1], res[2], res[3]))
}
