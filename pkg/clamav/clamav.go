package clamav

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"os/exec"
	"os/user"
	"path"
	"syscall"
	"text/template"

	"github.com/go-logr/logr"
)

var errExit *exec.ExitError

type proxy struct {
	Server string
	Port   string
}

type Scanner struct {
	freshclamConfFile string
	databaseDir       string
	username          string
}

func New(log logr.Logger) (*Scanner, error) {
	tmpDir := os.TempDir()
	databaseDir := path.Join(tmpDir, "clam-database")
	freshclamConfFile, err := ioutil.TempFile(tmpDir, "freshclam")
	if err != nil {
		return nil, fmt.Errorf("create temp clam config : %w", err)
	}

	user, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("read current user info: %w", err)
	}

	var proxy proxy

	if httpProxy, ok := os.LookupEnv("HDH_HTTP_PROXY"); ok {
		u, err := url.Parse(httpProxy)
		if err != nil {
			return nil, fmt.Errorf("retrieve HDH_HTTP_PROXY env var: %w", err)
		}
		proxy.Server = u.Hostname()
		proxy.Port = u.Port()
	}

	freshclamConf, err := template.New("freshclam.conf").Parse(`
DatabaseMirror database.clamav.net
{{if .Server -}} HTTPProxyServer {{.Server}} {{- end}}
{{if .Port -}} HTTPProxyPort {{.Port}} {{- end -}}`)
	if err != nil {
		return nil, fmt.Errorf("create template freshclam conf file: %w", err)
	}

	if err := freshclamConf.Execute(freshclamConfFile, proxy); err != nil {
		return nil, fmt.Errorf("process freshclam conf file: %w", err)
	}

	if err := os.MkdirAll(databaseDir, 0o755); err != nil {
		return nil, fmt.Errorf("can't create database directory: %w", err)
	}

	return &Scanner{
		freshclamConfFile: freshclamConfFile.Name(),
		databaseDir:       databaseDir,
		username:          user.Username,
	}, nil
}

func (scanner *Scanner) Scan(file string) (int, error) {
	if exitError, err := scanner.Update(); err != nil {
		return exitError, fmt.Errorf("execute freshclam: %w", err)
	}

	/* #nosec */
	cmd := exec.Command(
		"clamscan",
		"--verbose",
		"--database="+scanner.databaseDir,
		file)

	if err := cmd.Run(); err != nil {
		if errors.As(err, &errExit) {
			return errExit.ExitCode(), fmt.Errorf("clamscan execution failed : %w", err)
		}
	}
	return cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus(), nil
}

func (scanner *Scanner) Update() (int, error) {
	/* #nosec */
	cmd := exec.Command(
		"freshclam",
		"--verbose",
		"--user="+scanner.username,
		"--foreground",
		"--datadir="+scanner.databaseDir,
		"--config-file="+scanner.freshclamConfFile)

	if err := cmd.Run(); err != nil {
		if errors.As(err, &errExit) {
			return errExit.ExitCode(), fmt.Errorf("freshclam execution failed : %w", err)
		}
	}
	return cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus(), nil
}
