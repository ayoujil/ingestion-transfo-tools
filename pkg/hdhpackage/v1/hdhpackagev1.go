package v1

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"

	blobpackage "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/klauspost/compress/zip"
	"golang.org/x/sync/errgroup"
	"gopkg.in/yaml.v2"
)

const (
	DataFileName     = "data"
	MetadataFileName = "metadata"
	Version          = "v1"
)

type HDHPackageV1 struct {
	blobpackage.Blob
	FileConfig *v1alpha1.FileConfig
	Data       io.Reader
}

func New(blobName string, log logr.Logger, buf io.ReaderAt, size int64, blobURL *azblob.BlockBlobURL, cpk *azblob.ClientProvidedKeyOptions, metadata map[string]string) (*HDHPackageV1, error) {
	r, err := zip.NewReader(buf, size)
	if err != nil {
		return nil, err
	}

	hdhfile := &HDHPackageV1{
		Blob: *blobpackage.NewBlob(blobName, blobURL, cpk, metadata),
	}

	for _, f := range r.File {
		switch f.Name {
		case MetadataFileName:
			rc, err := f.Open()
			if err != nil {
				return nil, err
			}
			bMeta, err := ioutil.ReadAll(rc)
			if err != nil {
				return nil, err
			}

			meta := new(v1alpha1.FileConfig)
			if err := yaml.Unmarshal(bMeta, meta); err != nil {
				return nil, err
			}
			hdhfile.FileConfig = meta
			rc.Close()
		case DataFileName:
			rc, err := f.Open()
			if err != nil {
				return nil, err
			}
			hdhfile.Data = rc
		default:
			log.V(2).Info("unused file in archive", "fileName", f.Name)
		}
	}

	if hdhfile.FileConfig == nil {
		hdhfile.FileConfig = &v1alpha1.FileConfig{
			HashFileConfig: v1alpha1.HashFileConfig{},
		}
	}

	return hdhfile, nil
}

func (p *HDHPackageV1) Read(b []byte) (n int, err error) {
	return p.Data.Read(b)
}

func (p *HDHPackageV1) Config() *v1alpha1.HashFileConfig {
	return &p.FileConfig.HashFileConfig
}

func (f *HDHPackageV1) zip(buf io.Writer) (int64, error) {
	w := zip.NewWriter(buf)
	defer w.Close()

	var n int64

	metadata, err := yaml.Marshal(f.FileConfig)
	if err != nil {
		return n, fmt.Errorf("can't marshal metadata: %w", err)
	}

	metaf, err := w.Create(MetadataFileName)
	if err != nil {
		return n, err
	}
	nMeta, err := metaf.Write(metadata)
	n += int64(nMeta)
	if err != nil {
		return n, err
	}

	dataf, err := w.Create(DataFileName)
	if err != nil {
		return n, err
	}

	nData, err := io.CopyBuffer(dataf, f.Data, nil)
	n += nData
	if err != nil {
		return n, err
	}

	return n, w.Flush()
}

func (p *HDHPackageV1) Upload(ctx context.Context) error {
	rOut, wOut := io.Pipe()

	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		_, err := p.zip(wOut)
		wOut.Close()
		return err
	})

	g.Go(func() error {
		defer rOut.Close()
		if err := p.Blob.UploadBlobStream(gctx, rOut); err != nil {
			return fmt.Errorf("can't upload package: %w", err)
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	return nil
}

func (f *HDHPackageV1) WriteFileAsV2(buf io.Writer) (int64, error) {
	w := zip.NewWriter(buf)
	defer w.Close()

	var n int64

	dataf, err := w.Create(DataFileName)
	if err != nil {
		return n, err
	}

	nData, err := io.Copy(dataf, f.Data)
	n += nData
	if err != nil {
		return n, err
	}

	return n, w.Flush()
}

func (f *HDHPackageV1) Version() string {
	return Version
}

func (p *HDHPackageV1) Type() string {
	return string(v1alpha1.CSV)
}

func (p *HDHPackageV1) Copy(r io.Reader, client *azblob.ContainerURL, cpk *azblob.ClientProvidedKeyOptions, blobName string) interface{} {
	return &HDHPackageV1{
		Blob:       *p.Blob.Copy(client, cpk, blobName),
		FileConfig: p.FileConfig,
		Data:       r,
	}
}
