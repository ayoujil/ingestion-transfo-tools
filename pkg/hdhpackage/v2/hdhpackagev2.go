package v2

import (
	"archive/zip"
	"context"
	"encoding/json"
	"fmt"
	"io"

	blobpackage "rdd-pipeline/pkg/hdhpackage/blob"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"golang.org/x/sync/errgroup"
)

const (
	DataFileName = "data"
	Version      = "v2"
)

type HDHPackageV2 struct {
	blobpackage.Blob
	FileConfig *v1alpha1.HashFileConfig
	Data       io.Reader
}

func NewFromFile(blobName string, data io.Reader, blobURL *azblob.BlockBlobURL, cpk *azblob.ClientProvidedKeyOptions, metadata map[string]string) (*HDHPackageV2, error) {
	config := new(v1alpha1.HashFileConfig)

	if hashConfig, ok := metadata[v1alpha1.MetadataHashConfig]; ok {
		if err := json.Unmarshal([]byte(hashConfig), config); err != nil {
			return nil, fmt.Errorf("can't parse config: %w", err)
		}
	}

	hdhfile := &HDHPackageV2{
		Blob:       *blobpackage.NewBlob(blobName, blobURL, cpk, metadata),
		FileConfig: config,
		Data:       data,
	}

	return hdhfile, nil
}

func NewFromBlobUrl(blobName string, log logr.Logger, buf io.ReaderAt, size int64, blobURL *azblob.BlockBlobURL, cpk *azblob.ClientProvidedKeyOptions, metadata map[string]string) (*HDHPackageV2, error) {
	config := new(v1alpha1.HashFileConfig)

	fmt.Printf("config : %s", metadata[v1alpha1.MetadataHashConfig])
	if hashConfig, ok := metadata[v1alpha1.MetadataHashConfig]; ok {
		if err := json.Unmarshal([]byte(hashConfig), config); err != nil {
			return nil, fmt.Errorf("can't parse config: %w", err)
		}
	}

	hdhfile := &HDHPackageV2{
		Blob:       *blobpackage.NewBlob(blobName, blobURL, cpk, metadata),
		FileConfig: config,
	}

	r, err := zip.NewReader(buf, size)
	if err != nil {
		return nil, err
	}

	for _, f := range r.File {
		switch f.Name {
		case DataFileName:
			rc, err := f.Open()
			if err != nil {
				return nil, err
			}
			hdhfile.Data = rc
		default:
			log.V(2).Info("unused file in archive", "fileName", f.Name)
		}
	}

	return hdhfile, nil
}

func (p *HDHPackageV2) Read(b []byte) (n int, err error) {
	return p.Data.Read(b)
}

func (p *HDHPackageV2) Config() *v1alpha1.HashFileConfig {
	return p.FileConfig
}

func (p *HDHPackageV2) zip(buf io.Writer) (int64, error) {
	w := zip.NewWriter(buf)
	defer w.Close()

	var n int64

	dataf, err := w.Create(DataFileName)
	if err != nil {
		return n, err
	}

	nData, err := io.CopyBuffer(dataf, p.Data, nil)
	n += nData
	if err != nil {
		return n, err
	}

	return n, w.Flush()
}

func (p *HDHPackageV2) Upload(ctx context.Context) error {
	jconf, err := json.Marshal(p.Config())
	if err != nil {
		return fmt.Errorf("fail to marshall config: %w", err)
	}

	p.Blob.Metadata()[v1alpha1.MetadataHashConfig] = string(jconf)

	rOut, wOut := io.Pipe()

	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		_, err := p.zip(wOut)
		wOut.Close()
		return err
	})

	g.Go(func() error {
		defer rOut.Close()
		if err := p.Blob.UploadBlobStream(gctx, rOut); err != nil {
			return fmt.Errorf("can't upload package: %w", err)
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}
	return nil
}

func (p *HDHPackageV2) Version() string {
	return Version
}

func (p *HDHPackageV2) Type() string {
	return p.Blob.Metadata()[v1alpha1.MetadataBlobType]
}

func (p *HDHPackageV2) Copy(r io.Reader, client *azblob.ContainerURL, cpk *azblob.ClientProvidedKeyOptions, blobName string) interface{} {
	return &HDHPackageV2{
		Blob:       *p.Blob.Copy(client, cpk, blobName),
		FileConfig: p.FileConfig,
		Data:       r,
	}
}
