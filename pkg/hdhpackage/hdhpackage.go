package hdhpackage

import (
	"context"
	"errors"
	"fmt"
	"io"

	rddblob "rdd-pipeline/pkg/blob"
	v1 "rdd-pipeline/pkg/hdhpackage/v1"
	v2 "rdd-pipeline/pkg/hdhpackage/v2"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/go-logr/logr"
	"github.com/guilhem/chunkreaderat"
)

var ErrNoVersion = errors.New("no package version found")

func NewPackageFromBlobURL(ctx context.Context, log logr.Logger, blobName string, blobURL *azblob.BlockBlobURL, cpk *azblob.ClientProvidedKeyOptions) (HDHPackage, error) {
	bReadAt := rddblob.NewBlobReaderAt(ctx, log, &blobURL.BlobURL, *cpk)

	size, err := bReadAt.SizeWithError()
	if err != nil {
		return nil, fmt.Errorf("can't get size: %w", err)
	}

	chunkreader, err := chunkreaderat.NewChunkReaderAt(bReadAt, 1024*4096)
	if err != nil {
		return nil, fmt.Errorf("can't create chunk reader: %w", err)
	}

	prop, err := blobURL.GetProperties(ctx, azblob.BlobAccessConditions{}, *cpk)
	if err != nil {
		return nil, fmt.Errorf("can't getProperties: %w", err)
	}

	meta := prop.NewMetadata()
	if _, vers := meta[v1alpha1.MetadataVersion]; !vers {
		if _, ok := meta[v1alpha1.MetadataBlobType]; !ok {
			meta[v1alpha1.MetadataVersion] = v2.Version
		}
	}

	log.V(1).Info("create new package", "v2", v2.Version, "version", meta[v1alpha1.MetadataVersion])

	var p HDHPackage

	switch meta[v1alpha1.MetadataVersion] {
	case v1.Version:
		log.V(1).Info("create new package v1", "size", size, "meta", meta)

		p, err = v1.New(blobName, log, chunkreader, size, blobURL, cpk, meta)
		if err != nil {
			return nil, fmt.Errorf("can't create HDHPackageV1: %w", err)
		}

	case v2.Version:
		fallthrough

	default:
		log.V(1).Info("create new package v2", "size", size, "meta", meta)

		meta[v1alpha1.MetadataVersion] = v2.Version

		p, err = v2.NewFromBlobUrl(blobName, log, chunkreader, size, blobURL, cpk, meta)
		if err != nil {
			return nil, fmt.Errorf("can't create HDHPackageV2: %w", err)
		}
	}

	return p, nil
}

type HDHPackage interface {
	io.Reader
	SetMetadata(context.Context) error
	Metadata() map[string]string
	Config() *v1alpha1.HashFileConfig
	Upload(context.Context) error
	Delete(context.Context) error
	Version() string
	Type() string
	BlobName() string
	AcquireLease(ctx context.Context) error
	ReleaseLease(ctx context.Context) error
	Copy(io.Reader, *azblob.ContainerURL, *azblob.ClientProvidedKeyOptions, string) interface{}
	Size(context.Context) (int64, error)
}
