package blob

import (
	"context"
	"errors"
	"fmt"
	"io"

	rddblob "rdd-pipeline/pkg/blob"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/google/uuid"
)

type Blob struct {
	metadata map[string]string
	Name     string
	cpk      *azblob.ClientProvidedKeyOptions
	blobURL  *azblob.BlockBlobURL
	leaseID  string
}

func NewBlobFromContainerClient(name string, client *azblob.ContainerURL, cpk *azblob.ClientProvidedKeyOptions, meta map[string]string) *Blob {
	blobUrl := client.NewBlockBlobURL(name)
	return NewBlob(name, &blobUrl, cpk, meta)
}

func NewBlob(name string, blobURL *azblob.BlockBlobURL, cpk *azblob.ClientProvidedKeyOptions, meta map[string]string) *Blob {
	blob := Blob{
		cpk:      cpk,
		blobURL:  blobURL,
		Name:     name,
		metadata: meta,
	}
	return &blob
}

func (b *Blob) BlobName() string {
	return b.Name
}

func (b *Blob) CopyWithPrefix(client *azblob.ContainerURL, cpk *azblob.ClientProvidedKeyOptions) *Blob {
	metadata := make(map[string]string, len(b.metadata))
	for k, v := range b.metadata {
		metadata[k] = v
	}
	blob := NewBlobFromContainerClient(b.Name, client, cpk, metadata)
	return blob
}

func (b *Blob) Copy(client *azblob.ContainerURL, cpk *azblob.ClientProvidedKeyOptions, blobName string) *Blob {
	metadata := make(map[string]string, len(b.metadata))
	for k, v := range b.metadata {
		metadata[k] = v
	}

	if blobName == "" {
		blobName = b.Name
	}

	blob := NewBlobFromContainerClient(blobName, client, cpk, metadata)
	return blob
}

func (b *Blob) AcquireLease(ctx context.Context) error {
	lease, err := b.blobURL.AcquireLease(ctx, uuid.New().String(), -1, azblob.ModifiedAccessConditions{})
	if err != nil || lease.ErrorCode() != "" {
		return fmt.Errorf("acquire lease : %w", err)
	}
	b.leaseID = lease.LeaseID()
	return nil
}

var (
	ErrNoLeaseID    = errors.New("no leaseID")
	ErrBlobNotFound = errors.New("blob not found")
)

func (b *Blob) ReleaseLease(ctx context.Context) error {
	if b.leaseID == "" {
		return ErrNoLeaseID
	}

	if _, err := b.blobURL.ReleaseLease(ctx, b.leaseID, azblob.ModifiedAccessConditions{}); err != nil {
		var storageErr azblob.StorageError
		if !errors.As(err, &storageErr) {
			return fmt.Errorf("failed to parse release error: %w", err)
		}

		if storageErr.ServiceCode() == azblob.ServiceCodeBlobNotFound {
			return ErrBlobNotFound
		}

		return fmt.Errorf("release lease error: %w", err)
	}

	b.leaseID = ""

	return nil
}

var ErrNoVersion = errors.New("no package version found")

func (b *Blob) SetMetadata(ctx context.Context) error {
	if b.leaseID != "" {
		if err := rddblob.SetMetadataWithLease(ctx, b.blobURL, b.metadata, *b.cpk, b.leaseID); err != nil {
			return err
		}
	} else {
		if err := rddblob.SetMetadata(ctx, b.blobURL, b.metadata, *b.cpk); err != nil {
			return err
		}
	}
	return nil
}

func (b *Blob) Metadata() map[string]string {
	return b.metadata
}

func (b *Blob) DownloadBlob(ctx context.Context) ([]byte, map[string]string, error) {
	return rddblob.DownloadBlob(ctx, b.blobURL, *b.cpk)
}

func (b *Blob) Size(ctx context.Context) (int64, error) {
	return rddblob.Size(ctx, b.blobURL, *b.cpk)
}

func (b *Blob) DownloadBlobStream(ctx context.Context) (io.ReadCloser, int64, map[string]string, error) {
	return rddblob.DownloadBlobStream(ctx, b.blobURL, *b.cpk)
}

func (b *Blob) UploadBlobStream(ctx context.Context, data io.Reader) error {
	if err := rddblob.UploadBlobStream(ctx, b.blobURL, data, b.metadata, *b.cpk); err != nil {
		b.Delete(ctx) // nolint
		return err
	}
	return nil
}

func (b *Blob) UploadBlob(ctx context.Context, data []byte) error {
	if err := rddblob.UploadBlob(ctx, b.blobURL, data, b.metadata, *b.cpk); err != nil {
		b.Delete(ctx) // nolint
		return err
	}
	return nil
}

func (b *Blob) Delete(ctx context.Context) error {
	return rddblob.DeleteBlobWithLease(ctx, b.blobURL, b.leaseID)
}

func (b *Blob) AddMetadata(meta map[string]string) {
	for k, v := range meta {
		b.metadata[k] = v
	}
}
