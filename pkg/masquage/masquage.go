package masquage

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash"

	gopenssl "github.com/spacemonkeygo/openssl"
)

type CorrespondenceTable map[string]string

func Hash(ID string, key string) (string, error) {
	hKey, err := hex.DecodeString(key)
	if err != nil {
		return "", err
	}
	hash := hmac.New(sha256.New, hKey)

	hash.Reset()
	if _, err := hash.Write([]byte(ID)); err != nil {
		return "", fmt.Errorf("hash write: %w", err)
	}
	hdhID := hex.EncodeToString(hash.Sum(nil))
	return hdhID, nil
}

func HashOpti(ID string, key string, hash hash.Hash) (string, error) {
	hash.Reset()
	if _, err := hash.Write([]byte(ID)); err != nil {
		return "", fmt.Errorf("hash write: %w", err)
	}
	hdhID := hex.EncodeToString(hash.Sum(nil))
	return hdhID, nil
}

func HashOpenssl(ID string, key string) (string, error) {
	hKey, err := hex.DecodeString(key)
	data := []byte(ID)
	if err != nil {
		return "", err
	}
	hash, err := gopenssl.NewHMAC(hKey, gopenssl.EVP_SHA256)
	if err != nil {
		return "", fmt.Errorf("Unable to create new HMAC: %w", err)
	}

	if _, err := hash.Write(data); err != nil {
		return "", fmt.Errorf("hash write: %w", err)
	}

	var actualHMACBytes []byte
	if actualHMACBytes, err = hash.Final(); err != nil {
		return "", fmt.Errorf("Error while finalizing HMAC: %w", err)
	}

	return hex.EncodeToString(actualHMACBytes), nil
}
